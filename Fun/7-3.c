#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(){
    int fds[2];
    pid_t pid;
    int cnt;
    char buf[100];

    if(pipe(fds) < 0) {
        perror("create pipe fail!\n");
        exit(1);
    }

    pid = fork(); // create child process

    if(pid < 0) // error
    {
        perror("can not fork!");
        exit(-1);
    }
    else if(pid == 0) // child 
    {
        close(fds[1]);
        dup2(fds[0],0);
        execlp("grep","grep",".c",NULL);
    }
    else // parent
    {
        close(fds[0]);
        dup2(fds[1],1);
        execlp("ls","ls","-l",NULL);
        wait(NULL);
    }
    
    return 0;
}