#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<string.h>

const char * fifopath = "/home/2016012963/work6/5/myfifo";

int main(int argc, char*argv[]){
    int fd;
    int cnt;
    char buf[80];
    int fifo;
    int fd_old;

    fd_old = open("/home/2016012963/work6/5/4.4.png",O_RDONLY);

    if(fd_old < 0)
    {
        perror("can not open the new file named 4.4.png");
        exit(3);
    }

    if(access(fifopath,F_OK) != 0)
    {
        fifo = mkfifo(fifopath,0777);
        if(fifo < 0)
        {
            perror("fifo create fail!");
            exit(1);
        }
    }

    fd = open(fifopath,O_WRONLY);

    if(fd < 0)
    {
        perror("open fail!");
        exit(2);
    }

    cnt = read(fd_old,buf,80);
    while(cnt > 1){
        write(fd,buf,cnt);
        cnt = read(fd_old,buf,80);
    }
    close(fd);
    exit(0);
}