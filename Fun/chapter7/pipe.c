#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(){
	int fds[2];
	int pid,cnt;
	char buf[80];

	if(pipe(fds)<0){
		perror("creat pipe fail...");
		exit(2);
	}
	
	pid = fork();

	switch(pid){
		case 0:
			close(fds[1]);
			cnt = read(fds[0],buf,80);
			while(cnt > 0){
				write(1,"write: ",7);
				write(1,buf,cnt);
				cnt = read(fds[0],buf,80);
			}
			close(fds[0]);
			exit(0);
			break;
		case 1:
			perror("fork fail...");
			exit(1);
			break;
		default:
			close(fds[0]);
			cnt = read(0,buf,80);
			while(cnt > 0){
				write(fds[1],buf,cnt);
				cnt = read(0,buf,80);
			}
			close(fds[1]);
			wait(NULL);
	}
	
	return 0;
}
