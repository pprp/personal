#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

const char *fifopath = "/tmp/myfifo";
int main(){
	int fd;
	int cnt;
	char buf[80];

	if(access(fifopath,F_OK)  == -1){
		perror("fifo file is not exist...");
		exit(1);
	}

	fd = open(fifopath,O_RDONLY);
	if(fd < 0){
		perror("open fifo fail...");
		exit(2);
	}

	cnt = read(fd,buf,80);
	while(cnt > 0){
		write(0,buf,cnt);
		cnt = read(fd,buf,80);
	}

	close(fd);

	return 0;
}
