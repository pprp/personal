#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

const char *fifopath = "/tmp/myfifo";#include <string.h>

int main(){
	int fifo;
	int fd;

	char buf[80];
	int cnt;

	if(access(fifopath,F_OK) != 0){
		fifo = mkfifo(fifopath,0660);
		if(fifo < 0){
			perror("fifo creat fail...");
			exit(1);
		}
	}

	fd = open(fifopath,O_WRONLY);
	if(fd < 0 ){
		perror("open fail...");
		exit(2);
	}
	cnt = read(0,buf,80);
	while(cnt > 1){
		write(fd,buf,cnt);
		cnt = read(0,buf,80);
	}

	close(fd);
	exit(0);
}
