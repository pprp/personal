#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>

//信号处理例程
void signal_handle(int signum){
	printf("signal(num=%d) is trap....\n",signum);
	int i;
	for(i = 0;i<5;i++){
		sleep(1);

		printf("trapping..\n");
	}
}

int main(){
	//注册信号处理程序
	signal(SIGINT,signal_handle);
	signal(3,signal_handle);

		printf("program is running....\n");
		sleep(1);
	return 0;
}
