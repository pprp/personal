#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>
void sig_handler(int num)
{
    printf("receive the signal %d.\n", num);
    raise(SIGCONT);
    signal(SIGALRM,SIG_DFL);
}

int main(){

    signal(SIGALRM, sig_handler);
    alarm(2);
    pause();
    printf("pause is over.\n");
   exit(0);
}
