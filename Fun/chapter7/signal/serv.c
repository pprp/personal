#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include<signal.h>

void handle(){
	printf("server is stopped by signal....\n");
	fflush(stdout);
	signal(SIGUSR1,SIG_DFL);
	exit(0);
}

void srv();

int main(){
	pid_t pid;
	printf("start server....\n");


	pid = fork();

	switch(pid){
		case -1:
			perror("fork fail...");
			exit(1);
			break;
		case 0:
			srv();
			break;
		default:
			printf("server started ...\n");
			break;
			exit(0);
	}
}

void srv(){
	signal(SIGUSR1,handle);
	int fd = open("serv.log",O_CREAT|O_WRONLY|O_APPEND,0644);
	if(fd < 0){
		perror("log fail...");
	}
	dup2(fd,1);
	dup2(fd,2);
	close(fd);

	setsid();
	chdir("/tmp");
	umask(0);
	close(0);

	while(1){
		printf("server is running...\n");
		fflush(stdout);
		sleep(2);
	}
}
