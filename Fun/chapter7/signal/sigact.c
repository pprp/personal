#include<stdio.h>
#include<stdlib.h>
#include<signal.h>

void handle(int signo){
	int i;
	printf("signal %d is captured....\n",signo);
	
	for(i = 0;i<5;i++){
		puts("signal is blocked (in signal processing)...");
		sleep(1);
	}
}

int main(){
	struct sigaction action;

	action.sa_handler = handle;
	action.sa_flags = SA_NODEFER;
	sigemptyset(&action.sa_mask);

	sigaction(SIGINT,&action,NULL);

		puts("Hi.....");
		sleep(5);

	return 0;
}
