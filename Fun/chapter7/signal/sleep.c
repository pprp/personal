#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>
void sig_handler(int num)
{
    printf("receive the signal %d.\n", num);
    raise(SIGCONT);
    signal(SIGALRM,SIG_DFL);
}

int main(){

    signal(SIGALRM, sig_handler);
	//alarm(5);
    int ss = sleep(6);
    
    printf("The remaining %d second.\n",ss);
   exit(0);
}
