#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>

//信号处理例程
void signal_handle(int signum){
	printf("signal(num=%d) is trap....\n",signum);
	printf("正常退出...\n");
	exit(0);
}

int main(){
	//注册信号处理程序
	signal(SIGINT,signal_handle);
	signal(3,signal_handle);

	while(1){
		printf("program is running....\n");
		sleep(2);
	}
	return 0;
}
