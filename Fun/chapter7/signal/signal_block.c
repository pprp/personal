#include<stdio.h>
#include<stdlib.h>
#include<signal.h>

void handle(int signo){
	printf("signal(%d) is block after....\n",signo);
	signal(SIGINT,SIG_DFL);
}

int main(){
	sigset_t set,oldset;
	int i;

	//注册ctrl+c处理例程
	signal(SIGINT,handle);

	puts("all signal will be blocked...\n");
	//将所有有效信号填充到信号集
	sigfillset(&set);
	//将信号集设置为阻塞
	sigprocmask(SIG_SETMASK,&set,&oldset);
	for(i = 0;i<5;i++){
		puts("signal is blocking...");
		sleep(1);
	}
	puts("Ctrl+C will be trapped....\n");
	//清空信号集
	sigemptyset(&set);
	//将ctrl+c信号添加到信号集
	sigaddset(&set,SIGINT);
	//将ctrl+c信号从阻塞信号集中删除
	sigprocmask(SIG_UNBLOCK,&set,&oldset);
	while(1){}
}
