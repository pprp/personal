#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>

int main(){
	pid_t pid;

	int i,status;

	pid = fork();

	switch(pid){
		case -1:
			perror("fork fail ...");
			exit(1);
			break;
		case 0:
			alarm(10);
			for(i = 0;i<20;i++){
				printf("running(%d)....\n",i);
				sleep(1);
			}
			break;
		default:
			wait(&status);
			if(WIFEXITED(status) != 0){
				printf("child exit normally, exit code: %d\n",WEXITSTATUS(status));
			}
			else if(WIFSIGNALED(status) != 0){
				printf("child exit because signal...\n");
			}
			else{
				printf("child exit for other reason...\n");
			}
			break;
	}

	return 0;

}
