#include "msg.h"
#include<string.h>

int main(){
	int msgid;
	
	MSG msgbuf;
	msgbuf.mtype = mtype;
	
	msgid = msgget(key,IPC_CREAT|0666);
	if(msgid == -1){
		perror("MQ open fail....");
		exit(2);
	}

	while(1){
		gets(msgbuf.msg);
		if(strcmp(msgbuf.msg,"exit") == 0){
			exit(0);
		}
		msgsnd(msgid,&msgbuf,sizeof(msgbuf.msg),0);
	}
	
	msgctl(msgid,IPC_RMID,0);//删除MQ
	return 0;
}
