#include<stdio.h>
#include<stdlib.h>
#include<sys/ipc.h>
#include<sys/msg.h>

int main(){
	int msgid;
	struct msgbuf{
		long mtype;
		char msg[100];
	} msgbuf;

	msgid = msgget(12345,IPC_CREAT|0666);
	if(msgid == -1){
		perror("create MQ fail..");
		exit(1);
	}
	
	int cnt;
	cnt = msgrcv(msgid,&msgbuf,100,0,0);

	msgctl(msgid,IPC_RMID,NULL);

	return 0;
}
