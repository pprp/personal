#include "msg.h"

int main(){
	MSG msgbuf;

	msgbuf.mtype = mtype;

	int msgid = msgget(key,IPC_CREAT|0666);
	if(msgid == -1){
		perror("open MQ fail....");
		exit(2);
	}

	while(1){
		msgrcv(msgid,&msgbuf,100,mtype,0);
		printf("%s\n",msgbuf.msg);		
	}

	return 0;
}
