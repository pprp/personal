#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(){
	int fds[2];
	int pid,cnt;
	char buf[80];

	if(pipe(fds)<0){
		perror("creat pipe fail...");
		exit(2);
	}
	
	pid = fork();

	switch(pid){
		case 0:
			close(fds[1]);
			dup2(fds[0],0);
			execlp("grep","grep","^d",NULL);
			break;
		case 1:
			perror("fork fail...");
			exit(1);
			break;
		default:
			close(fds[0]);
			dup2(fds[1],1);
			execlp("ls","ls","-l",NULL);
			wait(NULL);
	}
	
	return 0;
}
