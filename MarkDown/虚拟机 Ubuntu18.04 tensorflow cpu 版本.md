# 虚拟机 Ubuntu18.04 tensorflow cpu 版本

## 虚拟机VMware

> 配置：
>
> - 20G容量，可扩充
> - 2G内存，可扩充
> - 网络采用NAT模式
> - 平台：win10下的Ubuntu18.04

### 出现的问题

- 网络连接问题

  在安装VMware以后，需要将其服务全部打开，正常连上网的应该是右上角出现三个正方形的标志，如果没有的话，就说明有网络问题。

- 解决方法

  在本地电脑中找到服务，打开以VM开头的所有服务

  除此之外，如果你自己已经修改过网络链接问题，那么

  > 编辑 -> 虚拟网络编辑器 -> 更改设置（右下角）-> 还原默认设置

  一般来说就可以解决了，如果不能的话可以参考别的博客。

- 分辨率问题：

  安装vmware tools （一般会提醒的，或者在虚拟机选项卡中可以找到）

  解压，直接运行后缀名为pl的文件即可。

  ```shell
  ./vmware-install.pl
  ```

  

- 文件夹共享问题

  > 虚拟机   设置  选项 共享文件夹  
  >
  > 总是启用
  >
  > 添加文件夹

## 安装Anaconda

- 一般都是从清华园上下载的，地址[https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/](https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/)
- 20分钟以内会下载完成
- 安装

```bash
bash Anaconda **** .sh
```

- Anaconda 远程仓库镜像

```shell
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
conda config --set show_channel_urls yes
```

- conda 常用命令

>1. conda --version
>2. conda update conda 
>3. conda create --name mine python=3.6
>4. conda remove --name mine --all
>5. conda search --full-name python
>6. conda list
>7. conda install numpy
>8. conda remove numpy
>9. source activate mine
>10. source deactivate mine
>11. conda update numpy
>12. conda update conda
>13. conda update anaconda
>14. conda update python

- 可能遇到的问题
  - 包损坏了，重新下载最新版本，删除anaconda文件夹，将环境变量删除。
  - conda命令不存在，检查环境变量，.bashrc文件，最后一行，一般来讲，可能是anaconda版本比如anaconda2 ,anaconda3导致找不到环境。修改一下就可以正常使用了。

## 安装tensorflow

- 安装pip:

  ```shell
  sudo apt-get install python-pip python-dev 
  ```

- 安装tensorflow

  ```shell
  conda create -n tensorflow python=3.6
  
  wget https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.0.0-cp36-cp36m-linux_x86_64.whl 
  
  mv tensorflow-1.0.0-cp36-cp36m-linux_x86_64.whl tensorflow-1.0.0-py3-none-linux_x86_64.whl #重命名
  
  source activate tensorflow
  
  cd Downloads
  
  pip install --ignore-installed --upgrade tensorflow-1.0.0-py3-none-linux_x86_64.whl
  ```

- 验证安装是否成功

  ```python
  (tensorflow)$ python
  import tensorflow as tf
  hello = tf.constant('Hello, TensorFlow!')
  sess = tf.Session()
  sess.run(hello)
  ```

- 但是此时发现在spyder或者pycharm中  使用TensorFlow无法识别。

  ```
  打开你的anaconda文件夹，找到envs 打开tensorflow 将sitepack-ages里面的东西都考到 anaconda/lib/python2.7/sitepack-ages
  ```



#### 参考内容

[https://www.cnblogs.com/tiansheng/p/7281290.html](https://www.cnblogs.com/tiansheng/p/7281290.html)



## 用环境跑一下neural-style程序

```
# 将库文件下载
git clone https://github.com/anishathalye/neural-style
# 安装vgg19
wget http://www.vlfeat.org/matconvnet/models/beta16/imagenet-vgg-verydeep-19.mat
```

### 准备工作

```shell
sudo apt-get update
sudo apt-get install python-pip python-dev python-scipy git
```

### 开始

```
cd ~/workspace/neural-style-master/
python neural_style.py --content ./examples/1-content.jpg --styles ./examples/1-style.jpg --output ./examples/outtest.jpg
```

### 错误

1. no module named tensorflow

   ```source activate tensorflow```

2. ModuleNotFoundError: No module named 'scipy'

   ```conda install scipy```

3. ModuleNotFoundError: No module named 'PIL'

   ```conda install pillow```



## 结束

W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.

Optimization started...
Iteration    1/1000
Killed