# QT5 Layout

[TOC]

## 1. 分割窗口 - QSplitter

- QSplitter::setFont
- QTextEdit::setAlignment(Qt::ALignCenter)
- QSplitter::(Qt::Vertical/Qt::Horizontal,所属对象)
- QTextEdit::(QObject::tr("botton widget"),所属对象)
- QSplitter::setStretchFactor(1,1);
- QSplitter::setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding)
- QSplitter::setWindowTitle(QObject::tr("Splitter"))

```c++
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QFont font("ZYSong18030",12);
    a.setFont(font);
    //主分割窗口
    QSplitter *splitterMain =new QSplitter(Qt::Horizontal,0);
    QTextEdit *textLeft =new QTextEdit(QObject::tr("Left Widget"),splitterMain);
    textLeft->setAlignment(Qt::AlignCenter);
    //右部分割窗口
    QSplitter *splitterRight =new QSplitter(Qt::Vertical,splitterMain);
    splitterRight->setOpaqueResize(true);
    QTextEdit *textUp =new QTextEdit(QObject::tr("Top Widget"),splitterRight);
    textUp->setAlignment(Qt::AlignCenter);
    QTextEdit *textBottom =new QTextEdit(QObject::tr("Bottom Widget"),splitterRight);
    textBottom->setAlignment(Qt::AlignCenter);
    splitterMain->setStretchFactor(1,1);
    splitterMain->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    splitterMain->setWindowTitle(QObject::tr("Splitter"));
    splitterMain->show();

    //MainWindow w;
    //w.show();
    return a.exec();
}
```

## 2. 停靠窗口 - QDockWidget

- 在cpp文件中写
- QDockWidget -> QWidget(parent)
- QWidget::setCentralWidget(一个对象)
- QDockWidget::setFeatures(QDockWidget::DockWidgetMovable/DockWidgetClosable/DockWidgetFloatable)
- QDockWidget::setAllowedAreas(Qt::LeftDockWidget|Qt::RightDockWidget)
- QDockWidget::setWidget("属于停靠窗口的一个组件")

```c++
DockWindows::DockWindows(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle(tr("DockWindows"));//设置主窗口的标题栏文字
    QTextEdit *te=new QTextEdit(this);//定义一个QTextEdit对象作为主窗口
    te->setText(tr("Main Window"));
    te->setAlignment(Qt::AlignCenter);
    setCentralWidget(te);               //将此编辑框设为主窗口的中央窗体
    //停靠窗口1
    QDockWidget *dock=new QDockWidget(tr("DockWindow1"),this);
    dock->setFeatures(QDockWidget::DockWidgetMovable);            //可移动
    dock->setAllowedAreas(Qt::LeftDockWidgetArea|Qt::RightDockWidgetArea);
    QTextEdit *te1 =new QTextEdit();
    te1->setText(tr("Window1,The dock widget can be moved between docks by the user" ""));
    dock->setWidget(te1);
    addDockWidget(Qt::RightDockWidgetArea,dock);
    //停靠窗口2
    dock=new QDockWidget(tr("DockWindow2"),this);
    dock->setFeatures(QDockWidget::DockWidgetClosable|QDockWidget::DockWidgetFloatable); //可关闭、可浮动
    QTextEdit *te2 =new QTextEdit();
    te2->setText(tr("Window2,The dock widget can be detached from the main window,""and floated as an independent window, and can be closed"));
    dock->setWidget(te2);
    addDockWidget(Qt::RightDockWidgetArea,dock);
    //停靠窗口3
    dock=new QDockWidget(tr("DockWindow3"),this);
    dock->setFeatures(QDockWidget::AllDockWidgetFeatures);     //全部特性
    QTextEdit *te3 =new QTextEdit();
    te3->setText(tr("Window3,The dock widget can be closed, moved, and floated"));
    dock->setWidget(te3);
    addDockWidget(Qt::RightDockWidgetArea,dock);
}
```

## 3. 堆栈窗口 - QStackedWidget

- QListWidget::insertItem(0/1/2,tr("window1/2/3"))
- QStackWidget::addWidget(对象)
- QHBoxLayout

```c++
-stacklg.h
class StackDlg : public QDialog
{
    Q_OBJECT
public:
    StackDlg(QWidget *parent = 0);
    ~StackDlg();
private:
    QListWidget *list;
    QStackedWidget *stack;
    QLabel *label1;
    QLabel *label2;
    QLabel *label3;

};
```

```c++
-stackdlg.cpp
StackDlg::StackDlg(QWidget *parent)
    : QDialog(parent)
{
    setWindowTitle(tr("StackedWidget"));
    list =new QListWidget(this);
    list->insertItem(0,tr("Window1"));
    list->insertItem(1,tr("Window2"));
    list->insertItem(2,tr("Window3"));

    label1 =new QLabel(tr("WindowTest1"));
    label2 =new QLabel(tr("WindowTest2"));
    label3 =new QLabel(tr("WindowTest3"));

    stack =new QStackedWidget(this);
    stack->addWidget(label1);
    stack->addWidget(label2);
    stack->addWidget(label3);
    QHBoxLayout *mainLayout =new QHBoxLayout(this);
    mainLayout->setMargin(5);
    mainLayout->setSpacing(5);
    mainLayout->addWidget(list);
    mainLayout->addWidget(stack,0,Qt::AlignHCenter);
    mainLayout->setStretchFactor(list,1);
    mainLayout->setStretchFactor(stack,3);
    connect(list,SIGNAL(currentRowChanged(int)),stack,SLOT(setCurrentIndex(int)));
}

```

## 4. 基本布局 - QLayout

### addWidget

- LeftLayout->addWidget(参数)

```c++
void addWidget{
    QWidget* widget;
    int fromRow;
    int fromColumn;
    int rowSpan;
    int ColumnSpan;
    Qt::Alignment alignment=0;
}
```

### addLayout

- mainLayout->addLayout(参数)

```c++
void addLayout{
    QLayout*layout;
    int row;
    int column;
    int rowSpan;
    int ColumnSpan;
    Qt::Alignment alignment=0;
}

```