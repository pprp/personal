# QT5 开发学习

## QT 模板库

### 字符串类 - QString

- ‘+’链接
- QString::append()
- QString::sprintf()
- QString::arg()

```o
QString str = QString("%1 was born in %2.").arg("John").arg(1982);
```

- insert()
- prepend() 在开头之前插入
- replace() 指定字符串取代原字符串某些字符
- trimmed()
- simplified() 将两边的空格取消，将中间的空格换成一个空格
- startsWith() 判断以什么为开头(String str,QT::CaseSensitive)
- endsWith()
- contains()
- 比较函数
  - < > >= <=
    - 静态 localeAwareCompare(const QString&,cosnt QString&)
    - compare(const QString&,const QString&,QT::CaseSensitivity)
- toInt() toDouble() toLong(&bool,n) n 为进制数
- 字符串转化
    - 返回QByteArray类
    - toAscii()
    - toLatin1()
    - toUtf8()
    - toLocal8Bit()
    - 用法
    ``` C++
    QString str = "hello world";
    QByteArray ba = str.toAscii();
    qDebug()<<ba;
    ba.append("hi~");
    qDebug()<<ba.data();
    //QDebug()只能用在QByteArray中，不能直接用在QString
    ```
- 判断是否为空
    - QString().isNull()---true
    - QString().isEmpty()---true
    - QString("").isNull()---false
    - QString("").isEmpty()---true

---

### 容器类 - QList QLinkedList QVector

#### Qlist类

- QList\<T>用法

- append()/prepend()
- insert()
- QList对象

``` C++
#include <QDebug>

int main(int argc, char *argv[])
{
    QList<QString> list;
    {
        QString str("this is a test string");
        list << str;//pay attention to this
    }
    qDebug() << list[0] << "how Are you!";
    return 0;
}
```

#### QlinkedList类 & QVector类

1. QLinkedList是链式表，只能用迭代器
2. QVector连续内存
3. 迭代器JAVA风格
    | 容器类 | 只读迭代器 | 读写迭代器 |
    |----------------------|------------------------|-------------------------------|
    | QList\<T>,QQueue\<T> | QListIterator\<T> | QMutableIterator\<T> |
    | QLinkedList\<T> | QLinkedListIterator\<T> | QMutableLinkedListIterator\<T> |
    | QVector\<T>,QStack\<T> | QVectorIterator\<T> | QMutableVectorIterator\<T> |
4. STL 风格 迭代器
    | 容器类 | 只读迭代器 | 读写迭代器 |
    |----------------------|--------------------------------|--------------------------|
    | QList\<T>,QQueue\<T> | QList\<T>::const_iterator | QList\<T>::iterator |
    | QLinkedList\<T> | QLinkedList\<T>::const_iterator | QLinkedList\<T>::iterator |
    | QVector\<T>,QStack\<T> | QVector\<T>::const_iterator | QVector\<T>::iterator |

#### QHash 类 & QMap类

- QHash<key,T>
- QMap<key,T>

```c++
    for(auto it = mp.begin(); it!=mp.end(); it++){
        qDebug() << it.key() << it.value();
    }
```

#### QVariant

QVariant可以保存很多Qt的数据类型，包括QBrush、QColor、QCursor、QDateTime、QFont、QKeySequence、QPalette、QPen、QPixmap、QPoint、QRect、QRegion、QSize和QString，并且还有C++基本类型，如int、float等。QVariant还能保存很多集合类型，如QMap\<QString, QVariant>, QStringList和QList\<QVariant>。

---

### 算法&正则表达式

#### QT5常用算法

```C++
<QtAlgorithms> & <QtGlobal>
```

- qAbs()
- qMax()
- qSwap()
- qRound() --- 用于四舍五入

#### 正则表达式

- ^表示非

---

### 控件

#### 按钮组 - Buttons

1. Push Button
2. Tool Button
3. Radio Button
4. Check Box
5. Command and Link Button
6. Button Box

- setMinimumSize(200,120)
- setMaximumSize(200,120)
- setGeometry(62,40,73,30)
- setFont(QFont("Times",18,QFont::Bold))
- connect(quit,SIGNAL(clicked(),qApp,SLOT(quit()))

### 输入部件组 - input widgets

1. Combo Box
2. Font Combo Box
3. LineEdit
4. TextEdit
5. PlainTextEdit
    >QPlainTextEdit与QTextEdit很像，但它多用于需要与文本进行处理的地方，而QTextEdit多用于显示，可以说，QPlainTextEdit对于plain text处理能力比QTextEdit强。
6. Spin Box
7. Double Spin Box
8. Time Edit
9. Date Edit
10. Date/Time Edit
11. Dial
12. Horizontal Scroll Bar
13. Vertical Scroll Bar
14. Horizontal Slider
15. Vertical Slider

- QDateTime类
  - currentDateTime()
    - date()
    - time()
    ```c++
    QLabel * dataLabel = new QLabel();
    QDateTime * dateTime = new QDateTime(QDateTime::currentDateTime());
    dataLabel->setText(dataTime->date().toString());
    dataLabel->show();
    ```
- QTimer类
    - 计时器步骤
    ```c++
    QTimer* time_clock = new QTimer(parent);
    connect(time_clock,SIGNAL(timeout()),this,SLOT(slottimedone()));
    //open the timer
    start(int time)
    setSingleShot(true)
    stop();
    ```

### 显示控件 - Display Widgets

1. Label
2. Text Browser
3. Graphics View
4. Calendar
5. LCD Number
6. Progress Bar
7. Horizonal Line
8. Vertical Line
9. QDeclarativeView
10. QWebView

- QGraphicsView
- QTextBrowser 只读文本框，但是具有链接文本的作用
  - 几种有用的槽
    1. virtual void backward()
    2. virtual void forward()
    3. virtual void home()

### 空间间隔组 Spacers

- Horizontal Spacer
- Vertical Spacer

### 布局管理组 - Layouts

- Vertial Layout
- Horizontal Layout
- Gird Layout
- Form Layout

### 容器组 - containers

- group box
- scroll Area
- Tool Box
- Tab Widget
- Stacked Widget
- Frame
- Widget
- MdiArea
- Dock Widget
- QAxWidget

### **实例分析**

- 窗口的创建

```C++
    QWidget*window=new QWidget();
    window->resize(320,240);
    window->show();
    QPushButton*btn=new QPushButton(QObject::tr("press me"),window);
    btn->move(100,100);
    btn->show();
```

- 布局的使用

```c++
    QWidget*window=new QWidget();
    window->resize(320,240);
    window->show();

    QLabel*label=new QLabel(QObject::tr("Name"));
    QLineEdit*lineEdit=new QLineEdit();

    QHBoxLayout*layout=new QHBoxLayout();
    layout->addWidget(label);
    layout->addWidget(lineEdit);
    window->setLayout(layout);
```

### 项目视图组 - item Views

- ListView
- Tree View
- Table View
- Column View

### 项目控件组

- List Widget
- Tree Widget
- Table Widget
