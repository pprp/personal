# Javascript 正则表达式

## 创建

> var 对象名 = /表达式/[flags,’gim’]);var 对象名 = new RegExp(表达式,’gim’);

- var regExp=/^http[s]?:\/\//;
- regExp=new RegExp(“^http[s]?:\/\/”,”gi”);

## 语法

- 匹配内容的元字符

| 普通字符 | 描述                                                         |
| -------: | ------------------------------------------------------------ |
|        \ | 将下一个字符标记为一个特殊字符、或一个原义字符。例如，’n’ 匹配字符 “n”。’\n’ 匹配一个换行符。 |
|  .(点号) | 匹配除 “\n” 之外的任何单个字符。                             |
|     x\|y | 匹配 x 或 y。例如，’z\|food’ 能匹配 “z” 或 “food”。’(z\|f)ood’ 则匹配 “zood” 或 “food”。 |
|    [xyz] | 字符集合。匹配所包含的任意一个字符。例如， ‘[abc]‘ 可以匹配 “plain” 中的 ‘a’。 |
|   [^xyz] | 负值字符集合。匹配未包含的任意字符。例如， ‘[^abc]‘ 可以匹配 “plain” 中的’p'。 |
|    [a-z] | 字符范围。匹配指定范围内的任意字符。例如，’[a-z]‘ 可以匹配 ‘a’ 到 ‘z’ 范围内的任意小写字母字符。 |
|   [^a-z] | 负值字符范围。匹配任何不在指定范围内的任意字符。例如，’[^a-z]‘ 可以匹配任何不在 ‘a’ 到 ‘z’ 范围内的任意字符。 |



| 普通字符 | 描述                                                         |
| -------- | ------------------------------------------------------------ |
| \d       | 匹配一个数字字符。等价于 [0-9]。                             |
| \D       | 匹配一个非数字字符。等价于 [^0-9]。                          |
| \w       | 匹配包括下划线的任何单词字符。等价于’[A-Za-z0-9_]‘。         |
| \W       | 匹配任何非单词字符。等价于 ‘[^A-Za-z0-9_]‘。                 |
| \s       | 单个空白符号                                                 |
| \S       | 除单个空白符号之外的任意字符，与\s相反                       |
| ()       | 分组，正则表达式内括号里写的内容会被认为是子正则表达式，所匹配的结果也会被记录下来。  在括号内的子模式开头加 ?:  ，则不记录匹配结果。如：/^(?:b\|c).+/ |



| 定位符 | 描述                                                         |
| ------ | ------------------------------------------------------------ |
| ^      | 匹配输入字符串的开始位置。如果设置了 RegExp 对象的 Multiline 属性，^ 也匹配 ‘\n’ 或 ‘\r’ 之后的位置。 |
| $      | 匹配输入字符串的结束位置。如果设置了RegExp 对象的 Multiline 属性，$ 也匹配 ‘\n’ 或 ‘\r’ 之前的位置。 |
| \b     | 匹配一个单词边界，也就是指单词和空格间的位置。               |
| \B     | 匹配非单词边界。                                             |



- 频率

| 限定符 | 描述                                                         |
| ------ | ------------------------------------------------------------ |
| *      | 匹配前面的子表达式零次或多次。例如，zo* 能匹配 “z” 以及 “zoo”。 * 等价于{0,}。 |
| +      | 匹配前面的子表达式一次或多次。例如，’zo+’ 能匹配 “zo” 以及 “zoo”，但不能匹配 “z”。+ 等价于 {1,}。 |
| ?      | 匹配前面的子表达式零次或一次。例如，”do(es)?” 可以匹配 “do” 或 “does” 中的”do” 。? 等价于 {0,1}。 |
| {n}    | n 是一个非负整数。匹配确定的 n 次。例如，’o{2}’ 不能匹配 “Bob” 中的 ‘o’，但是能匹配 “food” 中的两个 o。 |
| {n,}   | n 是一个非负整数。至少匹配n 次。例如，’o{2,}’ 不能匹配 “Bob” 中的 ‘o’，但能匹配 “foooood” 中的所有 o。’o{1,}’ 等价于 ‘o+’。’o{0,}’ 则等价于 ‘o*’。 |
| {n,m}  | m 和 n 均为非负整数，其中n <= m。最少匹配 n 次且最多匹配 m 次。刘， “o{1,3}” 将匹配 “fooooood” 中的前三个 o。’o{0,1}’ 等价于 ‘o?’。请注意在逗号和两个数之间不能有空格。 |

- 匹配参数

1. g  全局匹配
2. I   忽略大小写
3. m 多行匹配

### RegExp对象方法

- test  检查指定的字符串是否存在

```javascript
  var data = "123123"; 
  var reCat = /123/gi;
  alert(reCat.test(data));  //true
```

- exec 返回查询值

```javascript
var re = new RegExp(/be/g); 
var str = "To be, or not to be:That is the question:"; 
var f; 
do { 
f = re.exec(str); 
alert(f + ":" + f.index); 
} while (f!=null); 
```

### 字符串的方法

- match方法，返回一个类似数组的对象。

```javascript
var data = "123123,213,12312,312,3,Cat,cat,dsfsdfs,";
var reCat = /cat/gi;
var arrMactches = data.match(reCat)
for (var i=0;i < arrMactches.length ; i++)
{
alert(arrMactches[i]);   //Cat  cat
}
```

- replace方法,字符串替换。

```javascript
var reg = /w+/g;
var str =  'bbs.blueidea.com';
var newStr =  str.replace(reg,'word');
alert(newStr);
```

在replace函数的替换字符里可以使用$1～$9来引用匹配的内容。

```
var reg =  /(\w+)\.(\w+)\.(\w+)/;
var str =  'bbs.blueidea.com';
var newStr =  str.replace(reg,'$1.$1.$1');
alert(newStr);
输出的结果为：bbs.bbs.bbs
```

- search方法, 返回匹配的位置，类似与indexOf函数

```javascript
function seachString(){
		    var r, re; // 声明变量
		    var s = "The rain in Spain falls mainly in the plain."; 
		    re = /falls/; // 创建正则表达式模式
		    r = s.search(re); // 查找字符串
		    alert(r); // 返回 int 结果表示出现位置
		}
```

# JQuery

jQuery是一个轻量级的JavaScript框架，不需要安装。因为jQuery实际上就是一个外部js文件，使用时直接将该js文件用<script>标记链接到自己的页面中即可，代码如下：

```<script src=" jquery.min.js" type="text/JavaScript"> </script>```

## $及其作用

- “$”用作选择器

```javascript
<script type="text/JavaScript" src="jquery-1.3.2.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
  $("p").click(function(){
  $(this).hide();
  });
}); </script>
<p>If you click on me, I will disappear.</p>


<script type="text/JavaScript" src="jquery-1.3.2.js"></script>
<script type="text/JavaScript">
   $(document).ready(function(){     //页面载入后执行
            $("h2>a").css("color","red");
		$("h2>a").css("textDecoration","none");
  });
</script>
<h2>  <a href="http://www.baidu.com">百度</a><br>
  <a href="http://www.sohu.com">搜狐</a>   </h2>
```

-  “$”用作功能函数前缀 

each()方法，能使DOM循环结构简洁，不容易出错。each()函数封装了十分强大的遍历功能，使用也很方便，它可以遍历一维数组、多维数组、DOM, JSON 等

```$.each(object, callback, args)```

该方法有三个参数:进行操作的对象object，进行操作的函数callback ，函数的参数args。

>  each处理一维数组
>
> var arr1=[“aaa”, “bbb”, “ccc”];$.each(arr1, function(i, val){ alert(i); alert(val); });结果：alert(i)将输出0，1，2alert(val)将输出aaa,bbb,ccc

> each处理二维数组 ：
>
> var arr2=[[‘a’, ‘aa’, ‘aaa’],[‘b’, ‘bb’, ‘bbb’],[‘c’, ‘cc’, ‘ccc’]];$.each(arr,function(i, item){ alert(i); alert(item);});Arr2为一个二维数组，item相当于取这二维数组中的每一个数组。Item[0]相对于取每一个一维数组里的第一个值Alert(i)为0，1，2Alert(item)将输出[‘a’, ‘aa’, ‘aaa’],[‘b’, ‘bb’, ‘bbb’],[‘c’, ‘cc’, ‘ccc’]

> each处理json数据，能循环每一个属性 ：
>
> var obj={one:1, two:2, three:3};
>
> $.each(obj, function(key,val){ alert(key); alert(val);});s

- 解决DOM加载的问题，处理onload冲突

```javascript
用法：$(document). ready(…)
$(document).ready(function(){ 
		$("#loading").css("display","none");
})             //等价于$(“#loading”).hide()  /show()
```

> $(document). ready()，通常 写作$()

- 创建DOM元素

```javascript
jQuery中“$”可以直接创建DOM元素，例如：
	var newP =$("<p>武广高速铁路即将通车！</p>"); 
等价于：var a=createElement(“p”); var b=createTextNode(“武广”)； a.appendChild(b); 
```

DOM元素创建后，还需用下面的方法将这个元素插入到在页面中。	
```  newP.appendTo("body")```

 #### JQuery选择器

主要有三大类，即CSS 3的基本选择器，CSS3的位置选择器和过滤选择器

![1528729468823](1528729468823.png)

### Jquery中常用方法

- find()方法
- find()方法可以通过查询获取新的元素集合，通过匹配选择器来筛选元素

```javascript
$("div").find("p");
 <p><span>Hello</span>, how are you?</p>
  <p>Me? I'm <span>good</span>.</p>
<script>
  $("p").find("span").css('color','red');
</script>
```

- hover方法
- 一个模仿悬停事件（鼠标移动到一个对象上面及移出这个对象）的方法

```javascript
$(document).ready(function(){
  $("p").hover(function(){
    $("p").css("background-color","yellow");
    },function(){ $("p").css("background-color","pink");
  });          });
</script> 

<p>鼠标移动到该段落。</p>
```

- toggleclass方法
- toggleclass方法用于切换元素的样式。对设置或移除被选元素的一个或多个类进行切换

```javascript
 <script type="text/javascript">
$(document).ready(function(){
  $("button").click(function(){
    $('ul li').toggleClass(function(){
      return 'listitem_' + $(this).index(); });
  });    });
</script>
<style type="text/css">
.listitem_1, .listitem_3
{   color:red;   }
.listitem_0, .listitem_2
{   color:blue;  }
</style>
<h1 id="h1">This is a heading</h1>
<ul>
<li>Apple</li>
<li>IBM</li>
<li>Microsoft</li>
<li>Google</li>
</ul>
<button class="btn1">添加或移除列表项的类</button>
```



















