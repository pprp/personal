# Mablab

## MATLAB 图形图像处理

****
theme: MATLAB
author: pprp
date: 2018/2/2
****

### 二维绘图命令

1. plot
    线性空间
    - plot(t,[x1,x2,x3]) : 在同一个坐标中显示
    - subplot : 一个屏幕分开显示两个
2. loglog
    另个对数空间
3. semilogx
    一个线性空间一个对数空间
4. semilogy
    同上
5. polar
    极坐标空间

### 显函数、符号函数、隐函数的绘图

1. fplot(fun,[xstart,xend,ystart,yend])
    ```matlab
    function y=Afun(x);
    if x < 1
        y=x+1;
    else
        y=1+1/x;
    end;
    ```
    调用：fplot('Afun',[-3,3])
- ezplot
  - 默认范围$[-2*\pi,2*\pi]$
    - ezplot(fun,lims);

### 三维图形的画法

1. 三维图形
    - plotx(x,y,z)
2. 网格图
    - mesh(x,y,z)
3. 表面图
    - surf(x,y,z)
4. 旋转曲面
    - cylinder(fun(x)) - 绕x-axis旋转一周
5. 3-D可视化
    - meshgrid()
    - isosurface(x,y,z,v)

### 标题等信息设置

- title('my title')
- xlabel('x-axis Label')
- ylabel('y-axis Label')
- text(x,y,'Text for annotatioin')
- gtext('text for annotation'),grid
- hold on 图形保持命令 将当前图形保存下来，同时在这个坐标系内绘制另外一个图形
