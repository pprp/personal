# Typora 配置说明

> 为了更好的使用markdown,解决markdown中不如Word的不便之处，对markdown编辑器Typora进行配置

[TOC]

## 贴图功能

1. 进入File
2. 进入Preference
3. 找到Images Insert
4. 勾选use relative path if posiible
5. 勾选Allow copy image to given folder
6. 进入edit
7. 进入Image Tools
8. 进入When insert Local Image
9. 进入Copy image file to floder
10. 完成

## 自定义快捷键

> File>Perferences>Advances Settings >Open Advanced Settings

- 打开配置文件conf.user.json
- 将下列代码替换到文件中

```json
/** For advanced users. */
{
  "width": null, // Integer - Window's width in pixels. Default is null (last window width)
  "height": null, // Integer - Window's height in pixels. Default is null (last window height)
  "defaultFontFamily": {
    "standard": null, //String - Defaults to "Times New Roman".
    "serif": null, // String - Defaults to "Times New Roman".
    "sansSerif": null, // String - Defaults to "Arial".
    "monospace": null // String - Defaults to "Courier New".
  },
  "autoHideMenuBar": false, //Boolean - Auto hide the menu bar unless the `Alt` key is pressed. Default is false.
  // Array - Search Service user can access from context menu after a range of text is selected. Each item is formatted as [caption, url]
  "searchService": [
    [
      "Search with Baidu",
      "https://baidu.com/search?q=%s"
    ]
  ],
  // Custom key binding, which will override the default ones.
  "keyBinding": {
    // for example: 
    "Always On Top": "Ctrl+Shift+P",
    "Code Fences": "Ctrl+Shift+F",
    "Ordered List": "Ctrl+Alt+o",
    "Unordered List": "Ctrl+Alt+u"
  },
  "autoSaveTimer": 5, // default 5 minutes
  "maxFetchCountOnFileList": 500
}
```

## 快捷键使用

- 任务列表：-[空格]空格 文字
- 标题：ctrl+数字
- 表格：ctrl+t
- 生成目录：[TOC]按回车
- 选中一整行：ctrl+l
- 选中单词：ctrl+d
- 选中相同格式的文字：ctrl+e
- 跳转到文章开头：ctrl+home
- 跳转到文章结尾：ctrl+end
- 搜索：ctrl+f
- 替换：ctrl+h
- 引用：输入>之后输入空格
- 代码块：ctrl+alt+f
- 加粗：ctrl+b
- 倾斜：ctrl+i
- 下划线：ctrl+u
- 删除线：alt+shift+5
- 插入图片：直接拖动到指定位置即可或者ctrl+shift+i
- 插入链接：ctrl+k