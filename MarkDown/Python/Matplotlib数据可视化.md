---
author:pprp
---

# Matplotlib数据可视化 

[TOC]

## 安装

- conda install matplotlib
- sudo apt-get install python-matplotlib

## 架构

1. scripting
2. Artist
3. backend


## Backend层

- FigureCanvas对象实现绘图区域
- Renderer在FigureCanvas上绘图
- Event处理用户输入

## Artist层

图中能看到的元素都是这个层的，比如标题，标签，刻度等

分为两种：

1. primitive 原始
2. composite 复合

```mermaid
graph TB
  Axes-->Figure
  Text-->Axes
  X-axis-->Axes
  Y-axis-->Axes
  Line2D-->Axes
  Y-ticks-->Y-axis
  Y-label-->Y-axis
  X-ticks-->X-axis
  X-label-->X-axis
```
## Scripting层

pyplot, 数据分析和可视化

- pylab & pyplot

  - from pylab import *
  - import matplotlib.pyplot as plt
  - import numpy as np

- pylab 在一个命名空间整合了pyplot和numpy的功能，无需单独倒入numpy

  > 建议使用pylab模块进行使用

### pyplot模块

交互式用法与MATLAB相似

#### 生成一个简单的交互式图表

```
import matplotlib.pyplot as plt
plt.plot([1,2,3,4])
plt.show()
```

> 设置图形的属性
>
> - plt.axis([fromx,tox,fromy,toy]) # 范围
> - plt.title('my first plot') # 设置标题

```python
plt.plot([1,2,3,4],[1,4,9,16],'ro')
plt.show()
```

#### matplotlib and numpy

```python
import math
import numpy as np
t=np.linspace(0,10,1000)
y1=map(math.sin,math.pi*t)
y2=map(math.sin,math.pi*t+math.pi/4)
y3=map(math.sin,math.pi*t-math.pi/4)
plt.plot(t,y1,'b*',t,y2,'g^',t,y3,'ys')
```

试了一下报错了

RuntimeError: matplotlib does not support generators as input

```
import math
import numpy as np
x=np.linspace(0,10,1000)
y1=np.sin(x)+1
y2=np.cos(x ** 2)+1
y3=np.cos(x)
plt.plot(t,y1,'b*',t,y2,'g^',t,y3,'ys')
```

从网上找的一个例子：

```python
import numpy as np
import matplotlib.pyplot as plt
plt.rcParams['font.sans-serif']=['SimHei']#用来正常显示中文标签
plt.rcParams['axes.unicode_minus']=False#用来正常显示负号

x = np.linspace(0, 10, 1000)
y = np.sin(x) + 1
z = np.cos(x ** 2) + 1

plt.figure(figsize = (8, 4))
plt.plot(x,y,label='$\sin x+1$', color = 'red', linewidth = 2)
plt.plot(x,z,'b--',label='$\cos x^2+1$')
plt.xlabel('Time(s)')
plt.ylabel('Volt')
plt.title('A Sample Example')
plt.ylim(0,2.2)
plt.xlim(0,10)
plt.legend(loc='best')
```

|      | 颜色字符 | 说明    | 颜色字符 | 说明.1       |
| ---- | ---- | ----- | ---- | ---------- |
| 0    | 'b'  | blue  | 'm'  | magenta洋红色 |
| 1    | 'g'  | green | 'y'  | 黄色         |
| 2    | 'r'  | red   | 'k'  | 黑色         |

| 0    | '-'  | 实线   |
| ---- | ---- | ---- |
| 1    | '--' | 破折线  |
| 2    | '-.' | 点划线  |
| 3    | ':'  | 虚线   |

| 标记字符 | 说明        | 标记字符 | 说明     | 标记字符 | 说明     |
| ---- | --------- | ---- | ------ | ---- | ------ |
| '.'  | 点标记       | '1'  | 下花三角标记 | 'h'  | 竖六边形标记 |
| ','  | 像素标记(极小点) | '2'  | 上花三角标记 | 'H'  | 横六边形标记 |
| 'o'  | 实心圏标记     | '3'  | 左花三角标记 | '+'  | 十字形标记  |
| 'v'  | 倒三角标记     | '4'  | 右花三角标记 | 'x'  | x标记    |
| '^'  | 上三角标记     | 's'  | 实心方形标记 | 'D'  | 菱形标记   |
| '>'  | 右三角标记     | 'p'  | 实心五角标记 | 'd'  | 瘦菱形标记  |
| '<'  | 左三角标记     | '*'  | 星形标记   | '    | '      |

pyplot并不默认支持中文显示，需要rcParams修改字体实现

> matplotlib.rcParams['font.family']='SimHei'

rcParams['font.family']

| 中文字体       | 说明   |
| ---------- | ---- |
| 'SimHei'   | 中文黑体 |
| 'Kaiti'    | 中文楷体 |
| 'LiSu'     | 中文隶书 |
| 'FangSong' | 中文仿宋 |
| 'YouYuan'  | 中文幼圆 |
| STSong     | 华文宋体 |

### 使用kwarg

关键字参数

```
plt.plot([1,2,3,3,2,6,0,2],linewidth=2.0)
```

### 处理多个Figure和Axes对象

```
t=np.arange(0,5,0.1)
y1=np.sin(2*np.pi*t)
y2=np.sin(2*np.pi*t)

plt.subplot(211)
plt.plot(t,y1,'b-.')
plt.subplot(212)
plt.plot(t,y2,'r--')
```

> subplot(numRows, numCols, plotNum)
>
> [参考](https://blog.csdn.net/sunshihua12829/article/details/52786144)

```
import numpy as np
import matplotlib.pyplot as plt
# 分成2x2，占用第一个，即第一行第一列的子图
plt.subplot(221)
# 分成2x2，占用第二个，即第一行第二列的子图
plt.subplot(222)
# 分成2x1，占用第二个，即第二行
plt.subplot(212)
```

试一试：

```
import matplotlib.pyplot as plt
import numpy as np


# plt.plot([1, 2, 3, 4], [1, 4, 9, 16], 'ro')
# plt.axis([0, 6, 0, 20])
# plt.show()

# t = np.arange(0., 5., 0.2)
# plt.plot(t, t, 'r--', t, t ** 2, 'bs', t, t ** 3, 'g^')


def f(t):
    return np.exp(-t) * np.cos(2 * np.pi * t)


t1 = np.arange(0, 5, 0.1)
t2 = np.arange(0, 5, 0.02)

plt.figure(12)
plt.subplot(221)
plt.plot(t1, f(t1), 'bo', t2, f(t2), 'r--')

plt.subplot(222)
plt.plot(t2, np.cos(2 * np.pi * t2), 'r--')

plt.subplot(212)
plt.plot([1, 2, 3, 4], [1, 4, 9, 16])
```

### 为图表添加更多元素

> 文本的添加

```
plt.title('title')
plt.xlabel('counting')
plt.ylabel('sqare values')
fontsize=20,fontname='Times New Roman'
color='gray'
# 还允许你在表格的任何位置添加文本
text(x,y,s,fontdict=None,**kwargs)
```

支持LaTeX表达式

将表达式内容放在两$符号之间，就可以用latex表达式了，通常要在表达式前加上r，表明它后面是原是文本，不能对其进行转义操作。

```python
plt.text(1.1,12,r'$y=x^2$',fontsize=20,bbox={'facecolor':'yellow','alpha':0.2})
```

> 添加网格

```
plt.grid(True)
```

> 添加图例

```
plt.legend(['Fisrt Series'])
# 默认添加到右上角
# loc 关键字可以控制位置： 0 最佳位置，9 上方水平居中，8 下方水平居中
```

### 保存图标

```
%save my_first_chart 171
# 加载
%load my_first_chart.py
# 运行
%run my_first_chart.py
```

保存为图片

```
plt.savefig('mychart.png')
```

### 处理日期值

> import datatime
>
> datatime.data(2015,3,21)
>
> 在图表中可能有点问题，显示不全

再引入import matplotlib.dates，用MonthLocator()和DayLocator()函数分别表示月份和日子，然后用DateFormatter()函数

定义好两个时间尺度，一个用于日期，一个用于月份，可以调用set_major_locator()函数和set_minor_locator()函数，为x轴设置两个不同的标签；月份刻度标签的设置需要用到set_major_formatter()函数

```python
import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

months=mdates.MonthLocator()
days=mdates.DayLocator()

timeFmt=mdates.DateFormatter('%Y-%m')
events=[datetime.date(2015,1,23),datetime.date(2015,1,28),datetime.date(2015,2,3),datetime.date(2015,2,21),datetime.date(2015,3,15),datetime.date(2015,3,24),datetime.date(2015,4,8),datetime.date(2015,4,24)]
readings=[12,22,25,20,18,15,18,14]
fig,ax=plt.subplots()
plt.plot(events,readings)
ax.xaxis.set_major_locator(months)
ax.xaxis.set_major_formatter(timeFmt)
ax.xaxis.set_minor_locator(days)
```

## 图表类型

### 线性图

```python
import matplotlib.pyplot as plt
import numpy as np
x=np.arange(-2*np.pi,2*np.pi,0.01)
y=np.sin(3*x)/x
plt.plot(x,y)
```

刻度的自定义：

```
xticks(), yticks()
plt.xticks([-2*np.pi,-np*pi,0,np.pi,2*np.pi],[r'$-2\pi$',r'$-\pi$',0,'$\pi$','$2\pi$'])
```

![image](/home/pprp/Desktop/ttt.png)

想要将坐标轴改变，需要用gca()函数

```
ax=plt.gca()
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data',0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data',0))
```

> annotate() 函数可以用来注释，添加箭头



### 直方图

```
hist() 函数
pop = np.random.randint(0,100,100)
n,bins,patches=plt.hist(pop,bins=20)
```



### 条状图

```
bar() 函数
index=np.arange(5)
values1=[5,7,3,4,6]
plt.bar(index,values1)
plt.xticks(index+0.4,['A','B','C','D','E'])
```



### 水平条状图

```
barh() 函数
index=np.arange(5)
values1=[5,7,3,4,6]
plt.barh(index,values1)
plt.xticks(index+0.4,['A','B','C','D','E'])
```



### 多序列条状图

```
index=np.arange(5)
v1=[5,7,3,4,6]
v2=[5,6,6,4,7]
v3=[5,6,5,4,6]
bw=0.3
plt.axis([0,5,0,8])
plt.bar(index,v1,bw,color='b')
plt.bar(index+bw,v2,bw,color='g')
plt.bar(index+2*bw,v3,bw,color='r')
plt.xticks(index+1.5*bw,['A','B','C','D','E'])
```

DataFrame的多序列条状图：

data是字典 'series1':[1,2,3,4]

df=pd.DataFrame(data)

df.plot(kind='bar')

### 饼图

```python
pie()函数
labels=['Nokia','Samsung','Apple','Lumia']
values=[10,30,45,15]
colors=['yellow','red','blue','green']
plt.pie(values,labels=labels,colors=colors)
plt.axis('equal')

# 突出某一块		
explode=[0.3,0,0,0]
plt.pie(values,labels=labels,colors=colors,explode=explode,startangle=180)
```



### 等值线图

```python
contour()函数

def f(x,y):
	return (1-y**5+x**5)*np.exp(-x**2-y**2)
dx=0.01
dy=0.01
x=np.arange(-2.0,2.0,dx)
y=np.arange(-2.0,2.0,dy)
X,Y=np.meshgrid(x,y)
C=plt.contour(X,Y,f(X,Y),8,colors='black')
plt.contourf(X,Y,f(X,Y),8,cmap=plt.cm.hot)
plt.clabel(C,inline=1,fontsize=10)
plt.colorbar()
```



## mplot3D

```python
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
fig=plt.figure()
ax=Axes3D(fig)
X=np.arange(-2,2,0.1)
Y=np.arange(-2,2,0.1)
X,Y=np.meshgrid(X,Y)
def f(x,y):
	return (1-y**5+x**5)*np.exp(-x**2-y**2)
ax.plot_surface(X,Y,f(X,Y),rstride=1,cstride=1)
```

> 3D散点图

```
scatter()函数
```

> 3D条状图

```
bar()函数
```

## 多面板图形

> 在一个图中显示另一个子图

```
fig=plt.figure()
ax=fig.add_axes([0.1,0.1,0.8,0.8])
inner_ax=fig.add_axes([0.6,0.6,0.25,0.25])
```

> 子图网格

```
GridSpec()函数
gs=plt.GridSpec(3,3)
fig=plt.figure(figsize=(6,6))
fig.add_subplot(gs[1,:2])
fig.add_subplot(gs[0,:2])
fig.add_subplot(gs[2,0])
fig.add_subplot(gs[:2,2])
fig.add_subplot(gs[2,1:])
```

