# sickit-learn库实现机器学习
[TOC]

## Iris数据集

```python
from sklearn import datasets
iris=datasets.load_iris()
# 数据
iris.data
# 种类
iris.target
# 种类名称
iris.target_name
```

借用matplotlib绘制散点图

iris.data 中四个值分别为：萼片的长宽，花瓣的长宽

萼片的图像分布

```python
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from sklearn import datasets

iris=datasets.load_iris()
x=iris.data[:,0]
y=iris.data[:,1]
species=iris.target

x_min,x_max=x.min() - .5, x.max() + .5
y_min,y_max=y.min() - .5, x.max() + .5

plt.figure()
plt.title('iris')
plt.scatter(x,y,c=species)
plt.xlabel('Sepal length')
plt.ylabel('Sepal width')
plt.xlim(x_min,x_max)
plt.ylim(y_min,y_max)
plt.xticks(())
plt.yticks(())
```

修改一下得到花瓣的数据图像

```python
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from sklearn import datasets

iris=datasets.load_iris()
x=iris.data[:,2]
y=iris.data[:,3]
species=iris.target

x_min,x_max=x.min() - .5, x.max() + .5
y_min,y_max=y.min() - .5, x.max() + .5

plt.figure()
plt.title('iris')
plt.scatter(x,y,c=species)
plt.xlabel('Sepal length')
plt.ylabel('Sepal width')
plt.xlim(x_min,x_max)
plt.ylim(y_min,y_max)
plt.xticks(())
plt.yticks(())
```

发现这样比较集中



## 主成分分解PCA

```python
＃　scikit-learn库中 fit_transform()用来降维的　n_components指定降到几维

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import datasets
from sklearn.decomposition import PCA

iris=datasets.load_iris()
x=iris.data[:,1]
y=iris.data[:,2]
species=iris.target
x_reduced=PCA(n_components=3).fit_transform(iris.data)

fig=plt.figure()
ax=Axes3D(fig)
ax.set_title("PCA",size=15)
ax.scatter(x_reduced[:,0],x_reduced[:,1],x_reduced[:,2],c=species)
ax.set_xlabel('first')
ax.set_ylabel('second')
ax.set_zlabel('third')

ax.w_xaxis.set_ticklabels(())
ax.w_yaxis.set_ticklabels(())
ax.w_zaxis.set_ticklabels(())
```

### Ｋ-近邻分类器

选用150中的140作为训练集，10作为测试集

```python
import numpy as np
from sklearn import datasets
np.random.seed(0)
iris=datasets.load_iris()
x=iris.data
y=iris.target
i=np.random.permutation(len(iris.data))
x_train=x[i[:-10]]
y_train=y[i[:-10]]
x_test=x[i[-10:]]
y_test=y[i[-10:]]
# 分类
from sklearn.neighbors import KNeighborsClassifier as KNC
knn=KNC()
knn.fit(x_train,y_train)
# 预测
knn.predict(x_test)
y_test
```

可以发现错误率１０％只有一个错了

> 决策边界

```python
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn import datasets
from sklearn.neighbors import KNeighborsClassifier

iris=datasets.load_iris()
x=iris.data[:,2:4]
y=iris.target

x_min=x[:,0].min() - .5 
x_max=x[:,0].max() + .5

y_min=x[:,1].min() - .5
y_max=x[:,1].max() + .5

cmap_light=ListedColormap(['#FF83FA','#EEEE00','#CAE1FF'])
h = .02

xx,yy=np.meshgrid(np.arange(x_min,x_max,h),np.arange(y_min,y_max,h))

knn = KNeighborsClassifier()
knn.fit(x,y)
Z=knn.predict(np.c_[xx.ravel(),yy.ravel()])
Z=Z.reshape(xx.shape)

plt.figure()
plt.pcolormesh(xx,yy,Z,cmap=cmap_light)

# plot the trainging points
plt.scatter(x[:,0],x[:,1],c=y)
plt.xlim(xx.min(),xx.max())
plt.ylim(yy.min(),yy.max())
```
## Diabetes数据集

```python
#　导入
from sklearn import datasets
diabetes=datasets.load_diabetes()
```

### 数据说明

> 包含了４４２个病人的生理数据和发展情况，前十个为生理数据，
>
> - 年龄
> - 性别
> - 体质指数
> - 血压
> - 六种血清化验数据

```python
>>> diabetes.data[0]
array([ 0.03807591,  0.05068012,  0.06169621,  0.02187235, -0.0442235 ,
       -0.03482076, -0.04340085, -0.00259226,  0.01990842, -0.01764613])
```

这些数据已经做过均值中心化处理，原有信息没有丢失

## 线性回归：　最小平方回归

> 线性回归：　y=a*x+c

用scikit-learn库中的线性回归模型预测模型

>  导入linear_modal 模块,LinearRegression()构造模型

```python
from sklearn import linear_model
linreg=linear_model.LinearRegression()
from sklearn import datasets
diabetes=datasets.load_diabetes()
# 分为训练集和测试
x_train=diabetes.data[:-20]
y_train=diabetes.target[:-20]
x_test=diabetes.data[-20:]
y_test=diabetes.target[-20:]
#　开始训练
linreg.fit(x_train,y_train)
#　调用预测模型上coef_属性得到１０种数据的回归系数b
linreg.coef_
# 开始预测
linreg.predict(x_test)
#　用方差来对结果进行测评:0.58
linreg.score(x_test,y_test)
```

为了对训练集有一个全面认识，对10个模型进行绘图

```python
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn import datasets
# %matplotlib inline
diabetes=datasets.load_diabetes()
x_train=diabetes.data[:-20]
y_train=diabetes.target[:-20]
x_test=diabetes.data[-20:]
y_test=diabetes.target[-20:]
plt.figure(figsize=(8,12))
for f in range(0,10):
	xi_test=x_test[:,f]
	xi_train=x_train[:,f]
	xi_test=xi_test[:,np.newaxis]
	xi_train=xi_train[:,np.newaxis]
	linreg=linear_model.LinearRegression()
	linreg.fit(xi_train,y_train)
	y=linreg.predict(xi_test)
	plt.subplot(5,2,f+1)
	plt.scatter(xi_test,y_test,color='k')
	plt.plot(xi_test,y,color='b',linewidth=2)
```

### 支持向量机

> SVM算法是二院或者判别模型，最基础任务是判断数据是属于两类中的哪一类
>
> - SVR回归问题
> - SVC向量分类

```python
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
x=np.array([[1,3],[1,2],[1,1.5],[1.5,2],[2,3],[2.5,1.5],[2,1],[3,1],[3,2],[3.5,1],[3.5,3]])
y = [0]*6+[1]*5
plt.scatter(x[:,0],x[:,1],c=y,s=50,alpha=0.9)
```

用训练集训练ＳＶＣ算法之前，用ＳＶＣ构造函数定义模型，然后调用ｆｉｔ()函数传入训练集作为参数，最后用decision_function()绘制决策边界

```python
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm

x=np.array([[1,3],[1,2],[1,1.5],[1.5,2],[2,3],[2.5,1.5],[2,1],[3,1],[3,2],[3.5,1],[3.5,3]])
y = [0]*6+[1]*5
svc=svm.SVC(kernel='linear').fit(x,y)
X,Y=np.mgrid[0:4:200j,0:4:200j]
Z=svc.decision_function(np.c_[X.ravel(),Y.ravel()])
Z=Z.reshape(X.shape)
# 画三维等高线的两个函数，contourf填充等高线之间的空隙颜色
plt.contourf(X,Y,Z>0,alpha=0.4)
plt.contour(X,Y,Z,colors=['k'],linestyles=['-'],levels=[0])
plt.scatter(x[:,0],x[:,1],c=y,s=50,alpha=0.9)
```

得到了图像

![image](/home/pprp/Desktop/Python/1.png)

怎么预测：

> svc.predict([2.0,0])

### 非线性SVC

```python
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm

x=np.array([[1,3],[1,2],[1,1.5],[1.5,2],[2,3],[2.5,1.5],[2,1],[3,1],[3,2],[3.5,1],[3.5,3]])
y = [0]*6+[1]*5
svc=svm.SVC(kernel='poly',C=1,degree=3).fit(x,y)
X,Y=np.mgrid[0:4:200j,0:4:200j]
Z=svc.decision_function(np.c_[X.ravel(),Y.ravel()])
Z=Z.reshape(X.shape)
# 画三维等高线的两个函数，contourf填充等高线之间的空隙颜色
plt.contourf(X,Y,Z>0,alpha=0.4)
plt.contour(X,Y,Z,colors=['k','k','k'],linestyles=['--','-','--'],levels=[-1,0,1])
plt.scatter(svc.support_vectors_[:,0],svc.support_vectors_[:,1],s=120,facecolors='none')
plt.scatter(x[:,0],x[:,1],c=y,s=50,alpha=0.9)
```

![image](/home/pprp/Desktop/Python/2.png)

### 支持向量回归SVR

使用Diabetes数据集

```python
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn import datasets

diabetes=datasets.load_diabetes()
# 分为训练集和测试
x_train=diabetes.data[:-20]
y_train=diabetes.target[:-20]
x_test=diabetes.data[-20:]
y_test=diabetes.target[-20:]

x0_test=x_test[:,2]
x0_test=x_train[:,2]
x0_test=x_test[:,np.newaxis]
x0_train=x_test[:,np.newaxis]

x0_test.sort(axis=0)
x0_test=x0_test*100
x0_train=x0_train*100

svr=svm.SVR(kernel='linear',C=1000)
svr2=svm.SVR(kernel='poly',C=1000,degree=2)
svr3=svm.SVR(kernel='poly',C=1000,degree=3)

svr.fit(x0_train,y_train)
svr2.fit(x0_train,y_train)
svr3.fit(x0_train,y_train)

y=svr.predict(x0_test)
y2=svr2.predict(x0_test)
y3=svr3.predict(x0_test)

plt.scatter(x0_test,y,color='k')
plt.plot(x0_test,y,color='b')
plt.plot(x0_test,y2,color='r')
plt.plot(x0_test,y3,color='g')
```

