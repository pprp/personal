# deepin 中运行需要的命令

## 锐捷链接命令

* cd ruijie_linux/

* sudo ./rjsupplicant.sh

## 链接WiFi

1. iwconfig 
找到有无线的名称 wlp2s0
2. sudo ip link set wlp2s0 up
激活
3. sudo iw dev wlp2s0 scan | less
看扫描出来的信息 找到SSID eg:NWAFU
4. sudo iw dev wlp2s0 connect [SSID]
将SSID填入其中
5. sudo iw dev wlp2s0 connect [SSID] key 0:[WEP 密钥]

## 强力卸载软件
sudo apt-get purge + nameofapp