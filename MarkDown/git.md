---
theme: How to use Github
author: pprp
date: 2018/2/1
---
## How to use Github

Firstly you should match you SSH with your github

### Git usage
---
#### Create SSH
  ssh-keygen
- SSH Path
  ~/.ssh/
- pwd
    is '\n'
- How to Install git
    - sudo apt-get install git -y
- Config the user Name
    - git config --global user.name "***"
    - git config --global user.email "***@##.com"
---
#### Clone github
Use SSH and copy to clipboard for example(git@github.com:pprp/ACM.git)

- Clone
    - git clone git@github.com:pprp/ACM.git
- Move this file to Local Index Hub
    - mv ../new.txt ./new.txt
- Add your File to Local Index hub
    - git add "your file's name"
    - eg: git add new.txt
- Delete a File From github
    - git rm -f "your file's name"
    - git rm -f new.txt
- Redo and clone
    - git reset --hard HEAD
- Commit your changes
    - git commit -m 'your comments'
    - git commit -m 'small changes about (***)'
- Push your changes to Github
    - git push -u origin master
- Fetch some new changes
    - git fetch origin
- Pull your Github
    - git pull