## Array对象

> 小tips：
>
> - ```javascript
>   function $(id) {
>   	return document.getElementById(id);
>   }
>   ```

### 初始化

>var course = new Array ("C++程序设计","HTML开发基础","数据库原理","计算机网络");
>
>var course = ["C++程序设计","HTML开发基础","数据库原理","计算机网络"];

### 属性和方法

> - length：数组对象长度
> - join(separator)：把数组各个项用某个字符(串)连接起来，但并不修改原来的数组，默认用逗号分隔
> - pop()：删除并返回数组的最后一个元素
> - push(newelement1,.,newelementX)：可向数组的末尾添加一个或多个元素，并返回新的长度
> - shift()：用于把数组的第一个元素从其中删除，并返回第一个元素的值
> - unshift(newelement1,newelement2,....,newelementX)： 向数组的开头添加一个或更多元素，并返回新的长度
> - 其它方法:sort()、reverse()、toString()

### Array案例

```javascript
<script type="text/javascript">
    var course1 = new Array ("C++程序设计","HTML开发基础","数据库原理","计算机网络");
    var course2 = ["C++程序设计","HTML开发基础","数据库原理","计算机网络"];
    document.write("数组中的元素：<br>");
    //访问数组中的元素
    for (var i=0;i<=course1.length-1;i++ )
    {
        document.write(i+"-"+course1[i]+"&nbsp;&nbsp;");
    }
    document.write("<br><br>");
    //join方法的使用
    document.write(course2.join("-")+"<br>");//"-"分隔
    document.write(course2.join("+")+"<br>");//"+"分隔
    document.write(course2.join()+"<br>");  //默认
    //pop,push方法的使用
    document.write("<br>删除数组最后元素是"+course2.pop());  
    var s=course2.push("大学物理","高等数学");
    document.write("<br>课程数组2的长="+s);
    var course1 = new Array ("C++程序设计","HTML开发基础","数据库原理","计算机网络");
    //shift,unshift方法的使用
    var ss=course1.shift();
    document.write("<br>删除数组第一个元素是："+ss);
    //在数组开始处插入新元素
    var s=course1.unshift("大学语文");//在IE中显示undefined	
    alert(course1.unshift("asfasf"));
    document.write("<br>数组元素分别："+course1+"<br>数组的长度="+s); //在IE中用course1.length代替
</script>

```



## Date对象

###　生成日期对象 

```javascript
var MyDate = new Date(); 
var MyDate = new Date(milliseconds);
var MyDate = new Date(string);
var MyDate = new Date(year,month,day,hours,minutes,seconds,milliseconds);
应用举例
var birthday = new Date(“December 17, 1991 03:24:00”); birthday = new Date(1991,11,17); 
```

### 方法名：

| 方法名               | 说明                       |
| ----------------- | ------------------------ |
| getDate()         | 从Date对象返回一个月中的某一天(1～31)。 |
| getDay()          | 从Date对象返回一周中的某一天(0～6)。   |
| getMonth()        | 从Date对象返回月份(0～11)。(一般+1) |
| getFullYear()     | 从Date对象以四位数字返回年份。        |
| getHours()        | 返回Date对象的小时(0～23)。       |
| getMinutes()      | 返回Date对象的分钟(0～ 59)。      |
| getSeconds()      | 返回Date对象的秒数(0～ 59)。      |
| getMilliseconds() | 返回Date对象的毫秒(0～999)。      |
| getTime()         | 返回至今的毫秒数。                |

### 日期的调整与转换

```javascript
today.toString():把Date对象转换为字符串
today.toLocaleString():转换为本地时间串
today.toDateString();//日期部分转为字符串
today.toTimeString();//时间部分转为字符串
```

### 调整日期对象的日期和时间

```javascript
var today = new Date();
today.setDate(today.getDate()+5);  //将日期调整到5天以后，如果碰到跨年月，自动调整
today.setFullYear(2007,10,1);  //调整today对象到2007-10-1，月和日期参数可以省略
```

#### 实例

```html

<html >
  <head>
    <title>日期对象举例</title>
  </head>
  <body >
    <script type="text/javascript">
      var now = new Date();
      var y = now.getFullYear();
      var m = now.getMonth()+1;
      var d = now.getDate();
      var h = now.getHours();
      var mi = now.getMinutes();
      var s = now.getSeconds();
      if(m<10){m=“0”+m}  //处理成两位表示
      if(d<10){d="0"+d}
      if(h<10){h="0"+h}
      if(mi<10){mi="0"+mi;}
      if(s<10){s="0"+s;}
      var str = y+"年"+m+"月"+d+"日 "+h+":"+mi+":"+s;
      document.write(str);
</script>
</body>
</html>

```

## Math

> Math 对象提供多种算术常量和函数，执行普通的算术任务。可以直接通过“Math”名来使用它提供的属性和方法

- Math.PI
- Math.random()
- Math.sqrt()
- Math.max(x1,x2,[x3...])
- Math.min()
- Math.pow(x,y)
- Math.log(x)
- Math.ceil() # 上舍入
- Math.floor()#下摄入
- Math.exp()

#### 案例

```html
<html>
  <head><title>随机整数发生器举例</title>
  <script  type="text/javascript">
    function randomTest() {
      var m=parseFloat(document.Form1.minN.value);
      var n=parseFloat(document.Form1.maxN.value); 
      if(m>=n){                     
        alert("上限与下限相等,请返回重新输入!"); 
      }else{        
         var num=new Array();
         for(var i=0;i<10;i++) 
         { //产生 0-1 之间随机数，并通过系数变换到 m-n 之间
           num[i]=Math.round((Math.random()*(n-m)+m));                      
         } 
        document.getElementById("p1").innerHTML="随机数产生:"+num.join();
      }
    }
  </script>
</head>
<body>
  <h3 id="h34">随机整数发生器</h3>
  <form name="Form1">
   下限: <input type="text" name="minN" size="20" value=1><br> 
   上限: <input type="text" name="maxN" size="20" value=10><br/> 
   <input type="button" value="数学运算" onclick="randomTest();">
  </form>
  <p id="p1">随机产生数据：</p>
</body>
</html>
```

## Number

- ```
  按指定进制将数值转换为字符串    
             toString(radix)
             var x=10;
             var s=x.toString(2)  ; //s=1010
             var s1=x.toString(8);   //s1=12

  ```

- ```
  将Nmber四舍五入为指定小数点的数字
       toFixed(n) ：  固定小数点的位数
       如：    var x=10.24523;
                 var y=x.toFixed(2);  //y=10.25
  ```

## String

- 两种不同的定义字符串对象的方式

- ```javascript
  var  s1 = "Welcome to you!";var  s2 = new String("Welcome to you!");
  ```

- str.length: 获取字符串的长度 

- charAt()：获取字符串中指定位置的字符

- index_of(searchvalue,fromindex)：字串查找

- lastIndex_Of(searchvalue,fromindex)：字串查找

- split():字符串的分割

- 字符串的显示风格 

- ```
  big() ：变大些； 
  small()：变小些；
  fontsize()：字体大小；
  fontcolor()：字体颜色；
  bold() ：变粗些;
  italics()：斜体；
  sub()：下标；
  up()：下标
  ```

- 大小写转换  

- ```
   toLowerCase()：把字符串转换为小写
   toUpperCase()：把字符串转换为大写
   ```
  ```



## Boolean

- 参数为下列情况时返回true  
  -  1、true、”true”、”false”、”dfaf a”
- 参数为下列情况时返回flase    
  -    0、null、false、NaN、””、空

## DOM

> - document对象是客户端JavaScript最为常用的对象之一，在浏览器对象模型中，它位于window对象的下一层级
> - document对象包含一些简单的属性，提供了有关浏览器中显示文档的相关信息
> - document对象还包含一些引用数组的属性，这些属性可以代表文档中的表单、图象、链接、锚以及applet
> - document对象还定义了一系列的方法，通过这些方法，可以使JavaScript在解析文档时动态地将HTML文本添加到文档中

#### 根据 DOM，HTML 文档中的每个成分都是一个节点。

具体的规定如下：

- 整个文档是一个文档节点； 
- 每个 HTML 标签是一个元素节点；
- 包含在 HTML 元素中的文本是文本节点；
- 每一个 HTML 属性是一个属性节点；
- 注释属于注释节点。

​```html
<html> //文档节点
  <head>
    
  </head>
	<body> //元素节点
    	<p align="center"> //属性节点 getAttribute setAttribute
          content // 文本节点
      </p>  
  </body>

</html>
  ```

### 节点访问

- 通过getElementById( )方法访问节点（得到的是唯一的一个控件）
- 通过getElementsByName ( )方法访问节点（得到一个数组）
- 通过getElementsByTagName ( )方法访问节点（标记名字<p>不一定是一个，所以得到的依然是一个数组）
- 通过form元素访问节点

### 创建和修改节点

![](1.png)



#### 实例1

```html
<html>
<body>

<script type="text/javascript">

  function createP(){
    var op = document.createElement("p");
    var otext=document.createTextNode("hello world!!!!!!!!!!!!");
    op.appendChild(otext);
    document.forms[0].appendChild(op);
  }

</script>
<form name="form1">
<input type="button" value="创建节点" onClick="createP()"/>
</form>

</body>
</html>
```





#### 实例2：运用document对象删除、插入和替换网页中的节点 

```html
<html>
    <head>
        <title>删除、插入、替换节点举例</title>
        <script language="javascript" type="text/javascript">
        function operateNode()
        {
            //将页面上的<p>元素删除
            var p = document.getElementsByTagName("p")[0];
            document.form1.removeChild(p);
            //将页面中的<h2>元素更换为<h5>元素并重新设置文本节点内容
            var h5 =document.createElement("h5");
            var otext = document.createTextNode("web前端开发技术！");
            h5.appendChild(otext);
            var h2 = document.getElementsByTagName("h2")[0];
            document.form1.replaceChild(h5,h2);
            //在hdb元素前插入一个<p>元素
            var op =document.createElement("p");
            var otext = document.createTextNode("中国的是世界的！");
            op.appendChild(otext);
            var hdb = document.getElementsByTagName("hdb")[0];
            document.form1.insertBefore(op,hdb);
        }
        </script>
    </head>
    <body>
        <form name="form1">
            <h2>javaScript程序设计</h2>
            <p>hello world!</p>
            <hdb>世界的也是中国的！</hdb><br>
            <input type="button" value="点击修改节点" onClick="operateNode()">
        </form>
    </body>
</html>
```



### 文档碎片

```javascript
//文档碎片:类似一个临时的文档，要所有要加的dom元素先放在这里，达到不要每次操作dom元素
提高页面效率


    var d1 = new Date();
    //创建十个段落,常规的方式
    for(var i = 0 ; i < 1000; i ++) {
        var p = document.createElement("p");
        var oTxt = document.createTextNode("段落" + i);
        p.appendChild (oTxt);
        document.body.appendChild(p);
    }
    var d2 = new Date();
    document.write("第一次创建需要的时间:"+(d2.getTime()-d1.getTime()));


    //使用了createDocumentFragment()的程序
    var d3 = new Date();
    var pFragment = document.createDocumentFragment();
    for(var i = 0 ; i < 1000; i ++) {
        var p = document.createElement("p");
        var oTxt = document.createTextNode("段落" + i);
        p.appendChild(oTxt);
        pFragment.appendChild(p);
    }
    document.body.appendChild(pFragment);
    var d4 = new Date();
    document.write("第2次创建需要的时间:"+(d4.getTime()-d3.getTime()));
```

- innerText和innerHTML
  - > 在DOM中有两个很重要的属性，这两个属性分别是innerText和innerHTML，通过这两个属性，可以更方便的进行文档操作。

  - <p><strong>test</strong></p>
  - innerText : test
  - innertHTML: <strong>test</strong>


```html
<!--  edu_14_2_4.html -->
<html>
    <head>
        <title>innerText、innerHTML举例</title>
        <script type="text/javascript">
        function textGet()
        {
            var oDiv = document.getElementById("oDiv");
            var msg = "通过innerText属性获得：";
            msg+=oDiv.innerText;
            msg+="\n通过innerHTML属性获得： "
            msg+=oDiv.innerHTML;
            alert(msg);
        }
        </script>
    </head>
    <body onload="textGet()">
        <div id="oDiv" >
            <strong>web前端开发技术，不错！</strong>
        </div>
    </body>
</html>
```

#### document对象获取及设置节点属性方法的应用 

```html
<!--  edu_14-17.html -->
<html>
    <head>
        <title>获得、设置节点属性</title>
        <script type="text/javascript">
            function changeColor()
            {
                 var table = document.getElementById("myTable");
                 var color = table.getAttribute("bgcolor");
                 table.setAttribute("bgColor","red");
                 alert("颜色发生改变了！原来的颜色为"+color);
            }
        </script>
    </head>
    <body>
        <input type="button" value="更改颜色" onclick="changeColor()"><br>
        <table border="1" bgColor="#000fff" style="width:300px" id="myTable">
            <tr>
                <td>第一行，第一列;</td>
                <td>第一行，第二列;</td>
                <td>第一行，第三列;</td>
                <td>第一行，第四列;</td>
            </tr>
            <tr>
                <td>第二行，第一列;</td>
                <td>第二行，第二列;</td>
                <td>第二行，第三列;</td>
                <td>第二行，第四列;</td>
            </tr>
            <tr>
                <td>第三行，第一列;</td>
                <td>第三行，第二列;</td>
                <td>第三行，第三列;</td>
                <td>第三行，第四列;</td>
            </tr>
            <tr>
                <td>第四行，第一列;</td>
                <td>第四行，第二列;</td>
                <td>第四行，第三列;</td>
                <td>第四行，第四列;</td>
            </tr>
        </table>
    </body>
</html>

```

## BOM

客户端浏览器这些预定义的对象统称为浏览器对象，它们按照某种层次组织起来的模型统称为浏览器对象模型（BOM）

![](3.png)

window对象位于浏览器对象模型的顶层，是document、frame、location等其他对象的父类

![](4.png)

#### 实例

```html
<html>

<head>
    <title>对话框举例</title>
    <script type="text/javascript">
        function dialogTest() {
            window.alert("欢迎访问本页面！！"); //此处直接alert()也可以
            var answer = confirm("1+1=2吗?");
            if (answer == true)
                alert("算术运算 1+1=2 成立!");
            else
                alert("算术运算 1+1=2 不成立!");
            var answer1 = prompt("算术运算题目  : 1+1 = ?", "请输入值");
            if (answer1 == 2)
                alert("恭喜您,你的答案正确! ");
            else if (answer1 == null)
                alert("对不起,您还没作答! ");
            else
                alert("对不起,您的答案错误! ");
        }

    </script>
</head>

<body onload="dialogTest()">
</body>

</html>
```



#### 定时器

```html
<!--  edu_14_3_2.html -->
<html>

<head>
    <title>状态栏移动举例</title>
    <script language="JavaScript" type="text/javascript">
        var TimerID;
        var dir = 1;
        var str_num = 0;
        //用于动态显示的目标字符串
        var str = "欢迎来到javascript世界!";
        //设定动态显示的状态栏信息
        function startStatus() {
            var str_space = "";
            str_num = str_num + 1 * dir;
            if (str_num > 50 || str_num < 0) {
                dir = -1 * dir;
            }
            for (var i = 0; i < str_num; i++) {
                str_space += " ";
            }
            window.status = str_space + str;
        }
        //状态栏滚动开始
        function MyStart() {
            TimerID = setInterval("startStatus();", 100);
        }
        //状态栏滚动结束，并更新状态栏
        function MyStop() {
            clearInterval(TimerID);
            window.status = "状态栏移动结束!";
        }

    </script>
</head>

<body>
    <br>
    <center>
        <p>单击对应的按钮，实现动态状态栏的滚动与停止!</p>
        <form name="MyForm">
            <input type="button" value="开始状态栏滚动" onclick="MyStart()"><br>
            <input type="button" value="停止状态栏滚动" onclick="MyStop()"> <br>
        </form>
    </center>
</body>

</html>
```

## Navigator对象

#### 属性

![](5.png)

#### 方法

![](6.png)



## screen 对象

- screen对象用于获取用户屏幕设置的相关信息，主要包括显示尺寸和可用颜色的数量信息。 

  ![](7.png)

## history对象

![](8.png)

在实际开发中，如下使用history方法：

history.back()   //与单击浏览器后退按钮执行的操作一样

history.go(-2)   //与单击两次浏览器后退按钮执行的操作一样

history.forward() //等价于点击浏览器前进按钮或调用history.go(1)。



## Location 对象

location对象用来表示浏览器窗口中加载的当前文档的URL，该对象的属性说明了URL中的各个部分 。location对象的属性及方法

![](9.png)

#### 方法

![](10.png)