# javascript

## 消息对话框

```
1.警告框
    alert (message) 
2.确认框
   confirm (message) 
3.提示框
   prompt (text, defaultText)
```

### 关键字

| 表9-1 JavaScript的关键字 |        |       |            |         |
| ------------------- | ------ | ----- | ---------- | ------- |
| break               | case   | catch | continue   | default |
| delete              | do     | else  | finally    | for     |
| function            | if     | in    | instanceof | new     |
| return              | switch | this  | throw      | try     |
| typeof              | var    | void  | while      | with    |

### 保留字

| 表9-2 JavaScript的保留字 |          |              |        |            |
| ------------------- | -------- | ------------ | ------ | ---------- |
| abstract            | boolean  | byte         | char   | class      |
| const               | debugger | double       | enum   | export     |
| extends             | final    | float        | goto   | implements |
| import              | int      | interface    | long   | native     |
| package             | private  | protected    | public | short      |
| static              | super    | synchronized | throws | transient  |
| volatile            |          |              |        |            |

### 转义字符

| 转义字符 | 代表含义 | 转义字符 | 代表含义  |
| ---- | ---- | ---- | ----- |
| \b   | 退格符  | \t   | 水平制表符 |
| \f   | 换页符  | \'   | 单引号   |
| \n   | 换行符  | \"   | 双引号   |
| \r   | 回车符  | \\   | 反斜线   |

### 常用系统函数

- 返回字符串表达式中的值：eval（字符串表达式） 返回值：表达式的值或“undefined”。
- 返回字符的编码：escape(字符串)
- 返回字符串ASCII码:unescape (string)    unescape 函数返回的字符串是 ISO-Latin-1 字符集的字符。参数string包含形如“%xx”的字符的字符串，此处xx为两位十六进制数值。
- 返回实数:parseFloat(string);
- 返回不同进制的数:parseInt(numbestring , radix);   字符串以“0x”开始-16进制；以“0“开始--8进制；其他--10进制。
- 判断是否为数值 :isNaN(testValue);         NaN:not a Number  （注意大小写）



## 基本用法

document.getElementById('zoom').style.fontsize=size+'px';
document.getElementById('c').style.background=ColorName;

# javascript 事件分析

## 事件编程

- 事件源
  - window
  - form
  - mouse
  - key
- 事件
  - html事件
  - 突变事件
  - 双击事件
- 事件句柄
  - onclick
  - ondblclick

### 事件类型
• 鼠标单击
• 键盘事件
• html
• 突变事件
### 事件句柄
事件发生要进行的操作，onload , 事件句柄，

- 窗口事件
- 表单元素事件
- 鼠标事件
- 键盘事件

#### 事件句柄一览表

| 事件名称      | 事件句柄        | 事件                   |
| --------- | ----------- | -------------------- |
| load      | onLoad      | 当文档载入时执行JS代码         |
| unload    | onUnload    | 当文档卸载时执行JS代码         |
| change    | onChange    | 当元素改变时执行JS代码         |
| submit    | onSubmit    | 当表单被提交时执行JS代码        |
| reset     | onReset     | 当表单被重置时执行JS代码        |
| select    | onSelect    | 当元素被选取时执行JS代码        |
| blur      | onBlur      | 当元素失去焦点时执行JS代码       |
| focus     | onFocus     | 当元素获得焦点时执行JS代码       |
| click     | onClick     | 当鼠标被单击时执行JS代码        |
| dblclick  | onDblclick  | 当鼠标被双击时执行JS代码        |
| mousedown | onMouseDown | 当鼠标按钮被按下时执行JS代码      |
| mousemove | onMouseMove | 当鼠标指针移动时执行JS代码       |
| mouseout  | onMouseOut  | 当鼠标指针移出某元素时执行JS代码    |
| mouseover | onMouseOver | 当鼠标指针悬停于某元素之上时执行JS代码 |
| mouseup   | onMouseUp   | 当鼠标按钮被松开时执行JS代码      |
| keydown   | onKeyDown   | 当键盘被按下时执行JS代码        |
| keypress  | onKeyPress  | 当键盘被按下后又松开时执行JS代码    |
| keyup     | onKeyUp     | 当键盘被松开时执行JS代码        |

### 事件处理

静态指定

> <input type="button" value="通过函数输出信息" onclick="testInfo('单击按钮，调用函数输出信息')">

动态制定

> document.myform.mybutton.onclick();

特定对象特定事件

```
<script type=“text/javascript”  for=“对象” event=“事件”>
//事件处理程序代码
</script>
```
### 事件处理返回值

基本语法：事件句柄=“return 函数名（参数）;" 

## 表单事件句柄
| 事件句柄     | 事件           |
| -------- | ------------ |
| onchange | 当元素改变时执行脚本   |
| onsubmit | 当表单被提交时执行脚本  |
| onreset  | 当表单被重置时执行脚本  |
| onselect | 当元素被选取时执行脚本  |
| onblur   | 当元素失去焦点时执行脚本 |
| onfocus  | 当元素获得焦点时执行脚本 |



### 鼠标事件

| 事件句柄        | 事件                 |
| ----------- | ------------------ |
| onclick     | 当鼠标被单击时执行脚本        |
| ondblclick  | 当鼠标被双击时执行脚本        |
| onmousedown | 当鼠标按钮被按下时执行脚本      |
| onmousemove | 当鼠标指针移动时执行脚本       |
| onmouseout  | 当鼠标指针移出某元素时执行脚本    |
| onmouseover | 当鼠标指针悬停于某元素之上时执行脚本 |
| onmouseup   | 当鼠标按钮被松开时执行脚本      |

### 键盘事件

| 事件句柄       | 事件              |
| ---------- | --------------- |
| onkeydown  | 当键盘被按下时执行脚本     |
| onkeypress | 当键盘被按下后又松开时执行脚本 |
| onkeyup    | 当键盘被松开时执行脚本     |