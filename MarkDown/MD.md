# 标题

```diff
tags:
- Markdown
- 语言
categories:
- 技术
```

***

  theme: How to Use Markdown
  author: pprp
  date: 2.1

***

[TOC]

## 引用

> 区块引言
> [mermaid官方教程](https://mermaidjs.github.io/flowchart.html)
> [编辑器下载](https://github.com/knsv/mermaid)
> [Mermaid 在线编辑](http://knsv.github.io/mermaid/live_editor/)

## **文字** 加粗

## *wen* 斜体

## **asdf**

## *asdf*

## __asdf__

## _asdf_

在\$$数学环境中可以编写LaTeX文本

$$
\begin{aligned}
A &=(x-y)(x+y)\\
  &=x*x+x*y-y*x-y*y\\
  &=x^2-y^2
\end{aligned}
$$
<!-- asdf -->

## To do List

- [x] done
- [X] doing
- [x] asdf

## token

d2eb050ab20b3dc44df855a328bad968295d784b

## 流程图

```flow
st=>start: 开始
e=>end: 结束
op=>operation: 操作
sub=>subroutine: 子程序
cond=>condition: 判断
io=>inputoutput: 输出

st(right)->op->cond
cond(yes)->io(right)->e
cond(no)->sub(right)->e
```

```flow
st=>start: 开始吧
e=>end: 结束吧
op=>operation: 操作
cond=>condition: 判断

st->op->cond
cond(yes)->e
cond(no)->sub(right)->op
```

## 用mermaid流程图

1. TB（ top bottom）表示从上到下
2. BT（bottom top）表示从下到上
3. RL（right left）表示从右到左
4. LR（left right）表示从左到右
5. TD与TB一样表示从上到下

```mermaid
graph TD
    A[chrismas] --> B(Go shopping)
    B-->c{Let me think}
    c-->|one| D[laptop]
    c-->|two| E[iphone]
    c-->|three| F[car]
```

```mermaid
graph TD

A[Start] --> B[Your Operation]
B --> C{Yes or No?}
C --> |yes| D[end]
C --> |no| B
```

### 形状

```mermaid
graph LR
    A-->B[bname]
    A-->D
    C(cname)-->D((dname))
    D-->E
    E>ename]-->F{fname}
```

### 连线

```mermaid
graph TB
  A1-->B1
  A2---B2
  A3--tt---B3
  A4--tt-->B4
  A5-.-B5
  A6-.->B6
  A7-.ttt.-B7
  A8-.ttt.->B8
  A9===B9
  A10==>B10
  A11==ttt===B11
  A12==ttt==>B12
```

### 另外用法

```mermaid
graph TD
    client1-->|fff|SVN(SVN server)
    client2-->|eee|SVN((SVN server))
    client3-->|fff|SVN((SVN server))
    client4-->|eee|SVN((SVN server))
    client5(...)-->SVN((SVN server))
    SVN---|store the data|sharedrive
    
```

### 分部分

用subgraph --- end

```mermaid
graph TD
    subgraph Title
    client1-->|read / write|SVN((SVN server))
    client2-->|read only|SVN
    client3-->|read / write|SVN
    client4-->|read only|SVN
    client5(...)-->SVN
    SVN---|store the data|sharedrive
    end

```

## 序列图

### test1

```sequence

title: 序列图sequence Title

participant A
participant B
participant C

note left of A: A left
note over B: B over
note right of C: C right

A->A:A to A
A->B:实现箭头
A-->C:虚线箭头
B->>C:虚箭头
C->>A:虚箭头

```

### test2

```sequence
  Alice->Bob:hello Bob, how are you?
  Note right of Bob:Bob thinks
  Bob-->Alice: I am good, thanks
```

## 脚注 - can't work

脚注[^keyword]样例
这个是一个脚注$^1$


## 甘特图

```mermaid
gantt
dateFormat YYY-MM-DD
title 产品计划

section 初期阶段
明确需求: 2016-03-01,10d
section 中级阶段
明确需求: 2016-03-11,15d
section 后期阶段
进行测试: 2016-03-20, 9d

```

```mermaid
gantt
       dateFormat  YYYY-MM-DD
       title Adding GANTT diagram functionality to mermaid

       section A section
       Completed task            :done,    des1, 2014-01-06,2014-01-08
       Active task               :active,  des2, 2014-01-09, 3d
       Future task               :         des3, after des2, 5d
       Future task2              :         des4, after des3, 5d

       section Critical tasks
       task1 :crit, done, 2014-01-06,24h
       task2 :crit, done, after des1, 2d
       task3 :crit, active, 3d
       task4 :crit, 5d
       task5 :crit, 2d
       task6 :1d

       section Documentation
       Describe gantt syntax               :active, a1, after des1, 3d
       Add gantt diagram to demo page      :after a1  , 20h
       Add another diagram to demo page    :doc1, after a1  , 48h

       section Last section
       Describe gantt syntax               :after doc1, 3d
       Add gantt diagram to demo page      :20h
       Add another diagram to demo page    :48h
```

```mermaid
graph TB
    sq[Square shape] --> ci((Circle shape))

    subgraph A subgraph
        od>Odd shape]-- Two line<br/>edge comment --> ro
        di{Diamond with <br/> line break} -.-> ro(Rounded<br>square<br>shape)
        di==>ro2(Rounded square shape)
    end

    %% Notice that no text in shape are added here instead that is appended further down
    e --> od3>Really long text with linebreak<br>in an Odd shape]

    %% Comments after double percent signs
    e((Inner / circle<br>and some odd <br>special characters)) --> f(,.?!+-*ز)

    cyr[Cyrillic]-->cyr2((Circle shape Начало));

     classDef green fill:#9f6,stroke:#333,stroke-width:2px;
     classDef orange fill:#f96,stroke:#333,stroke-width:4px;
     class sq,e green
     class di orange
```