#include <iostream>

using namespace std;

void frontorder(int a,int *tree,int n)
{
    if(tree[a]!=0&&a <= n)
    {

        cout << tree[a] << " ";
        frontorder(a*2,tree,n);
        frontorder(a*2+1,tree,n);

    }
}

void midorder(int a,int *tree,int n)
{
    if(tree[a]!=0&&a<=n)
    {
        midorder(a*2,tree,n);
        cout << tree[a] << " ";
        midorder(a*2+1,tree,n);
    }
}

void laterorder(int a,int *tree,int n)
{
    if(tree[a]!=0&&a<=n)
    {
        laterorder(a*2,tree,n);
        laterorder(a*2+1,tree,n);
        cout << tree[a] << " ";
    }
}
int main()
{

    int n;
    cin >> n;
    int tree[n+1];
    tree[0]=0;
    for(int i=1; i<n+1; i++)
    {
        cin>>tree[i];

    }

    int a=1;
    frontorder(a,tree,n);
    cout << endl;
    a=1;
    midorder(a,tree,n);
    cout << endl;
    a=1;
    laterorder(a,tree,n);

    return 0;
}
