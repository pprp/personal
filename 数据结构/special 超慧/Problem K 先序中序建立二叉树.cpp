#include <iostream>
#include <queue>
#include <cstdio>

using namespace std;
const int maxn = 2000;

struct node
{
    node*lc;
    node*rc;
    int data;
    node()
    {
        lc = NULL;
        rc = NULL;
        data = 0;
    }
};

node*createTree(int* pre,int* in,int n)
{
    if(n == 0)
    {
        return NULL;
    }
    node * root = NULL;

    int lpre[maxn] = {0},rpre[maxn] = {0};
    int lin[maxn] = {0},rin[maxn] = {0};

    root = new node;
    root->data = pre[1];
    int lenght;
    int renght;
    for(int i=1; i<=n; i++)
    {
        if(in[i] == pre[1])
        {
            lenght = i-1;
            renght = n-i;
        }
    }
    int j = 0;
    for(int i=2; i<=n; i++)
    {

        if(i<lenght+2)
            lpre[i-1] = pre[i];
        else
        {
            j++;
            rpre[j] = pre[i];
        }
    }
    j = 0;
    for(int i=1; i<=n; i++)
    {
        if(in[i] != pre[1])
        {
            if(i<=lenght)
                lin[i] = in[i];
            else
            {
                j++;
                rin[j] = in[i];

            }
        }
    }
    root->lc = createTree(lpre,lin,lenght);
    root->rc = createTree(rpre,rin,renght);
    return root;
}

void leverorder(node* root)
{
    queue <node*>Q;
    node* p = root;
    Q.push(p);
    while(!Q.empty())
    {
        p = Q.front();
        printf("%d ",p->data);
        Q.pop();

        if(p->lc != NULL) Q.push(p->lc);
        if(p->rc != NULL) Q.push(p->rc);
    }
}

void postorder(node* root)
{

    if(root != NULL)
    {
        if(root->lc != NULL) postorder(root->lc);
        if(root->rc != NULL) postorder(root->rc);
        printf("%d ",root->data);
    }

}

int main()
{
    int n;
    scanf("%d",&n);
    int pre[maxn];
    int in[maxn];
    int x;
    for(int i=1; i<=n; i++)
    {
        scanf("%d",&pre[i]);
    }
    for(int i=1; i<=n; i++)
    {
        scanf("%d",&in[i]);
    }
    node* tree = createTree(pre,in,n);
    leverorder(tree);
    printf("\n");
    postorder(tree);

    return 0;
}
