/*已知二叉树采用二叉链表存储，其结点结构定义如下：

template<class T>

struct BinTreeNode{

 T data；

 BinTreeNode<T> *lchild，*rchild；

}；

编写计算二叉树中节点data值等于给定x值的结点个数算法，p指向二叉树的根节点,BinaryTree为二叉树类。函数原型为：

int BinaryTree<T>::CountNode(BinTreeNode<T> *p，T x);

*/


#include <iostream>

using namespace std;

int sum = 0;

template <class T>
struct BinTreeNode
{
    T data;
    BinTreeNode<T> *lchild,*rchild;
    BinTreeNode()
    {
        lchild = NULL;
        rchild = NULL;
    }
    BinTreeNode(T x,BinTreeNode<T>* l = NULL,BinTreeNode<T>*r = NULL)
    {
        data = x;
        lchild = l;
        rchild = r;
    }

};
template<class T>

class BinaryTree
{
public:
    BinTreeNode<T>* root;
    BinaryTree() {}

    int CreateTree(BinTreeNode<T>* &root);
    int CountNode(BinTreeNode<T> *p,T x);


};

template<class T>

int BinaryTree<T>::CreateTree( BinTreeNode<T>* & root)
{

    T item;

    cin >> item;

    if(item != 0)
    {
        root = new BinTreeNode<T>(item);
        if(root == NULL)
        {
            cerr << "存储分配错！" << endl;

        }
        CreateTree(root->lchild);
        CreateTree(root->rchild);
    }
    else root = NULL;

}

template<class T>

int BinaryTree<T>::CountNode(BinTreeNode<T>* p,T x)
{
    if(p != NULL)
    {
        if(p->data == x)
        {
            sum++;
        }
        CountNode(p->lchild,x);
        CountNode(p->rchild,x);
    }
    return sum;
}


int main()
{
    //int n;
    //cin >> n;
    BinaryTree <int> Tree;

    //int loyo = 0;
    Tree.CreateTree(Tree.root);

    int x;
    cin >> x;

    cout << Tree.CountNode(Tree.root,x) <<endl;

    return 0;
}
