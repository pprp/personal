#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED



#endif // LIST_H_INCLUDED
//带有附加头结点，将附加头结点算入链表的长度
using namespace std;
template <class T>
struct LinkNode
{
    T data;
    LinkNode<T>* next;
    LinkNode(LinkNode<T>* prr=NULL)
    {
        next=prr;
    }
    LinkNode(T & item,LinkNode<T>* prr=NULL)
    {
        data=item;
        next=prr;
    }


};
template<class T>
class List
{
public:
    List()
    {
        first = new LinkNode<T>;
    }
    List(const T& x)
    {
        first = new LinkNode<T>(x);
    }
    List(List<T> & L);
    ~List()
    {
        makeEmpty();
    }
    void makeEmpty();
    int length();
    LinkNode<T>*getHead();
    LinkNode<T>*Search(T x);
    LinkNode<T>*locate(int i);
    bool getData(int i,T& x)const;
    void setdata(int i,T& x);
    bool Insert(int i,T& x);
    bool Remove(int i,T& x);
    bool IsEmpty();
    bool IsFull();
    void Sort();
    void input(T endtag);
    void output();

protected:
    LinkNode<T>* first;
};

template<class T>
List<T>::List(List<T>& L)//复制构造函数
{
    T value;
    LinkNode<T>* up=L.getHead();
    LinkNode<T>* down=first=new LinkNode<T>;
    while(up->next!=NULL)
    {
        value=up->next->data;
        down->next=new LinkNode<T>(value);
        up=up->next;
        down=down->next;
    }
    down->next=NULL;
}

template<class T>//把表清空
void List<T>::makeEmpty()
{
    LinkNode<T>*p;
    while(first->next!=NULL)
    {
        p=first->next;
        first->next=p->next;
        delete p;
    }

}

template<class T>//链表长度

int List<T>::length()
{
    int k=0;
    LinkNode<T>*p=first;
    while(p!=NULL)
    {
        p=p->next;
        k++;
    }
    return k;
}
template<class T>

LinkNode<T>*List<T>::getHead()
{
    return first;
}
template<class T>

LinkNode<T>*List<T>::Search(T x)
{
    LinkNode<T>*current=first;
    while(current->next!=NULL)
    {
        if(current->data==x)
        {
            return current;
            break;
        }
        current->current->next;
    }
}
template<class T>

LinkNode<T>*List<T>::locate(int i)
{
    if(i<=0)return NULL;
    int k=0;
    LinkNode<T>*current=first;
    while(current->next!=NULL&&k<i)
    {
        current=current->next;
        k++;
    }
    return current;
}
template<class T>

bool List<T>::getData(int i,T& x)const
{
    LinkNode<T>*current=locate(i);
    if(i<=0)
    {
        return false;
    }
    else if(current==NULL||current->next==NULL)
    {
        return false;
    }
    else
    {
        x=current->data;
        return true;
    }
}
template<class T>

void List<T>::setdata(int i,T& x)
{
    if(i<=0) return false;
    LinkNode<T>*current=locate(i);
    current->data=x;
}
template<class T>

bool List<T>::Insert(int i,T& x)
{
    LinkNode<T>*current=locate(i);
    if(current==NULL) return false;
    LinkNode<T>*newnode=new LinkNode<T>(x);
    newnode->next=current->next;
    current->next=newnode;
    return true;
}
template<class T>

bool List<T>::Remove(int i,T& x)
{


    LinkNode<T>*current=locate(i-1);
    LinkNode<T>*del;
    current->next=del->next;
    x=del->data;
    delete del;
    return true;

}
template<class T>

bool List<T>::IsEmpty()
{
    if(first->next==NULL)
        return true;
    else
        return false;
}
template<class T>

bool List<T>::IsFull()
{
    return false;
}

template<class T>

void List<T>::input(T endtag)
{

    LinkNode<T>*newnode,*last;
    T x;
    makeEmpty();
    cout<<"请输入任意个元素"<<endl;
    cin >> x;
    last=first;
    while(x!=endtag)
    {
        newnode=new LinkNode<T>(x);
        last->next=newnode;
        last=newnode;
        cin >> x;

    }
}
template<class T>

void List<T>::output()
{
    LinkNode<T>*current=first->next;
    while(current!=NULL)
    {
        cout << current->data <<" ";
        current=current->next;
    }

}

