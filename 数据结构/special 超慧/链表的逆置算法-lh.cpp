//#include <iostream>
////编写算法，实现单链表的原地逆置操作（原地逆置即不需要新开辟空间）。
////单链表带有头结点，指针first指向头结点,List类以及定义节点的LinkNode类定义同课本。
////不需要编写能运行的程序，写出算法即可，函数的声明已给出，只需要写出函数体。
//using namespace std;
//template <class T>
//
//struct LinkNode{
//    T data;
//    LinkNode<T>* next;
//    LinkNode(LinkNode<T>*ptr=NULL)
//    {
//        next=ptr;
//    }
//    LinkNode(T & x,LinkNode<T>*ptr=NULL)
//    {
//        data=x;
//        next=ptr;
//    }
//
//};
//
//class List{
//protected:
//    LinkNode<T>*first;
//public:
//    List(){first=new LinkNode<T>;}
//    List(T& x){first=new LinkNode<T>(x);}
//    List(List<T> & L);
//    ~List();
//    void inverse();
//
//
//};
//int main()
//{
//
//}

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>

using namespace std;

template <class T>
class LinkNode
{
public:
    T data;
    LinkNode * next;
    LinkNode(T dt = 0):data(dt),next(NULL) {}
};

template <class T>
class List
{
protected:
    LinkNode<T> * first;
public:
    List()
    {
        first = new LinkNode<T>;
    }
    List(const T & x)
    {
        first = new LinkNode<T>(x);
    }
    List(List<T>& L);//ok
    ~List()//ok
    {
        makeEmpty();
    }
    void makeEmpty();//ok
    void Input();//ok
    void Output();//ok
    void inverse();
};

template <class T>
void List<T>::inverse()
{
    LinkNode<T>* item=NULL;//声明一个空结点
    LinkNode<T>* befo;//声明一个结点用来表示要移动的结点
    if(first->next==NULL)
    {
        return ;
    }
    while(first->next!=NULL)
    {
        befo=first->next;//有附加头结点，不用移动，所以从下一个结点开始移动
        first->next=befo->next;
        befo->next=item;//将要移动的结点指向空结点处
        item=befo;
    }
   // first = new LinkNode<T>;
    first->next = item;//改变头结点的指向
}
template <class T>
List<T> :: List(List<T>& L)
{
    T val;
    LinkNode<T> * bf = L.getHead();
    LinkNode<T> * nw = first = new LinkNode<T>;
    while(bf->next != NULL)
    {
        val = bf->next->data;
        nw->next = new LinkNode<T>(val);
        nw = nw->next;
        bf = bf->next;
    }
    nw->next = NULL;
}
template <class T>
void List<T> :: makeEmpty()
{
    LinkNode<T> * p;
    while(first->next != NULL)
    {
        p = first->next;
        first->next = p->next;
        delete p;
    }
    return ;
}
template <class T>
void List<T>::Input()
{
    LinkNode<T> * q = first;
    int sz;
    cout << "input the size:"<<endl;
    cin >> sz;
    cout <<"please enter the element:" << endl;
    while(sz--)
    {
        T val;
        cin >> val;
        LinkNode<T>* newnode = new LinkNode<T>(val);
        q->next = newnode;
        q = q->next;
    }
}
template<class T>
void List<T>::Output()
{
    LinkNode<T> * p = first->next;
    while(p != NULL)
    {
        cout << p->data <<" ";
        p = p->next;
    }
    cout << endl;
    return ;
}

int main()
{
    List<int> ll;
    ll.Input();
    ll.Output();
    ll.inverse();
    ll.Output();
    return 0;
}
