#include <iostream>
#include <cstdio>
//在单链表存储结构上实现基本操作：初始化、创建、插入、删除、查找、遍历、逆置、合并运算
using namespace std;

struct LinkNode
{

    int data;
    LinkNode*next;
    LinkNode(LinkNode*prr=NULL)
    {
        next=prr;
    }
    LinkNode(int & item,LinkNode*prr=NULL)
    {
        data=item;
        next=prr;
    }
};
class List
{
protected:
    LinkNode*first;
public:
    List()
    {
        first=new LinkNode;
    }
    List(int x)
    {
        first=new LinkNode(x);
    }
    //List(List&L);
    ~List();
    LinkNode*Locate(int i);
    void Create(int n);
    bool Insert(int i,int x);
    int Remove(int i);
    void Search(int i);
    void Scane();
    bool Reverse();
    void Merge(List & L);

};
List::~List()
{
    LinkNode*del=first;
    while(first!=NULL)
    {
        first = first->next;
        delete del;
        del=first;
    }
}
LinkNode*List::Locate(int i)
{
    LinkNode*p=first;
    int k=0;
    while(p!=NULL&&k<i)
    {
        p=p->next;
        k++;
    }
    return p;
}
void List::Create(int n)
{
    LinkNode*last=first;

    for(int i=0; i<n; i++)
    {
        int x;
        cin>>x;
        LinkNode* newnode=new LinkNode(x);
        last->next=newnode;
        last=newnode;
    }

}
bool List::Insert(int x,int i)
{
    if(i<=0) return false;
    LinkNode*current=Locate(i-1);
    LinkNode*newnode=new LinkNode(x);
    newnode->next=current->next;
    current->next=newnode;
    return true;

}
int List::Remove(int i)
{
    if(i<=0) return false;
    LinkNode*current=Locate(i-1);

    LinkNode*del=current->next;
    current->next=del->next;
    //x=del->data;
    delete del;
    //return x;
}
void List::Search(int i)
{
    int k=0;
    LinkNode*current=first;
    while(current!=NULL)
    {
        if(current->data==i)
        {
            cout<<k<<endl;
            return ;
        }
        else
        {
            current=current->next;
            k++;
        }
    }
    cout<< "Not found"<<endl;
}
void List::Scane()
{

    LinkNode*current=first->next;
    while(current!=NULL)
    {
        cout<< current->data <<" ";
        current=current->next;
    }
    cout << endl;
}
bool List::Reverse()
{
    LinkNode* item=NULL;//声明一个空结点
    LinkNode* befo;//声明一个结点用来表示要移动的结点
    if(first->next==NULL)
    {
        return false;
    }
    while(first->next!=NULL)
    {
        befo=first->next;//有附加头结点，不用移动，所以从下一个结点开始移动
        first->next=befo->next;
        befo->next=item;//将要移动的结点指向空结点处
        item=befo;
    }

    first->next = item;
    return true;
}
void List::Merge(List & L)
{
    LinkNode * p = first->next, *q = L.first->next;
    while((p != NULL) && (q != NULL))
    {
        if(p->data > q->data)
        {
            cout << q->data << " ";
            q = q->next;
        }
        else
        {
            cout << p->data << " ";
            p = p->next;
        }
    }
    while(p != NULL)
    {
        cout << p->data << " ";
        p = p->next;
    }
    while(q != NULL)
    {
        cout << q->data << " ";
        q = q->next;
    }
    cout << endl;
}

int main()
{
//    freopen("in.txt","r",stdin);
    List ll;
    int n;
    cin>>n;
    ll.Create(n);
    //ll.Scane();

    int mu,plac;
    cin>>mu>>plac;
    ll.Insert(mu,plac);
    ll.Scane();

    int re;
    cin>>re;
    ll.Remove(re);
    ll.Scane();

    int se;
    cin>>se;
    ll.Search(se);
    //ll.Scane();

    ll.Reverse();
    ll.Scane();

    List L;
    int s2;
    cin>>s2;
    L.Create(s2);
    ll.Merge(L);

    return 0;
}
