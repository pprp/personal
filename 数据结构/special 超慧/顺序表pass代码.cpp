#include <iostream>
#include <cstdio>
//在顺序存储结构实现基本操作：初始化、创建、插入、
//删除、查找、遍历、逆置、合并运算。
using namespace std;
class SeqList
{
protected:
    int *data;

    int last;
public:
    SeqList();
    //SeqList(SeqList& L);
    ~SeqList()
    {
        delete []data;
    }
    void create(int n);
    void Insert(int x,int i);
    bool Remove(int i);
    void Search(int n);
    void Scan();
    void Reverse();
    void Merge(SeqList & S);

};

SeqList::SeqList()
{
    last=-1;
    data=new int[1010];
    if(data==NULL)
        cout<<"存储分配错误"<<endl;
}
//SeqList::SeqList(SeqList& L);

void SeqList::create(int n)
{

    for(int i=0; i<n; i++)
    {
        cin>>data[i];
        last++;
    }
}
void SeqList::Insert(int x,int i)
{
    for(int j=last; j>=i-1; j--)
    {
        data[j+1]= data[j];
    }
    data[i-1]=x;
    last++;
}
bool SeqList::Remove(int i)
{
    for(int j=i; j<=last; j++)
    {
        data[j-1]=data[j];
    }
    last--;
    return true;
}
void SeqList::Search(int n)
{
    for(int i=0; i<=last; i++)
    {
        if(data[i]==n)
        {
            cout <<i+1<<endl;
            return ;
        }
    }
    cout << "Not found" << endl;

}
void SeqList::Scan()
{
    for(int i=0; i<=last; i++)
        cout<<data[i]<<" ";
    cout << endl;
}
void SeqList::Reverse()
{
    for(int i=0,j=last; i<=j; i++,j--)
    {
        int temp=data[i];
        data[i]=data[j];
        data[j]=temp;
    }
}
void SeqList::Merge(SeqList& S)
{
    int *sum;
    sum = new int[last+S.last+2];
    int temp,i = 0,j=0,h=0;

    while(j<=last&&h<=S.last)
    {
        if(data[j]<S.data[h])
        {
            temp=data[j];
            cout<<temp<<" ";
            j++;
        }
        else
        {
            temp=S.data[h];
            cout<<temp<<" ";
            h++;
        }
        sum[i]=temp;
        i++;
    }
    if(j<=last)
    {
        for(int g=j; g<last; g++)
        {
            sum[i]=data[g];
            cout<<sum[i]<<" ";
            i++;
        }
    }
    if(h<=S.last)
    {
        for(int g=h; g<=S.last; g++)
        {
            sum[i]=S.data[g];
            cout<<sum[i]<<" ";
            i++;
        }
    }

}



int main()
{
    freopen("in.txt","r",stdin);
    SeqList SS;
    int n;
    cin>>n;
    SS.create(n);
    int x1,m1;
    cin>>x1>>m1;
    SS.Insert(x1,m1);
    SS.Scan();

    int m2;
    cin>>m2;
    SS.Remove(m2);
    SS.Scan();
    cin>>m2;
    SS.Search(m2);

    SS.Reverse();
    SS.Scan();

    SeqList SSS;
    cin>>m2;
    SSS.create(m2);
    SSS.Merge(SS);

    return 0;
}
