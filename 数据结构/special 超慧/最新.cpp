#include <iostream>
#include <stdio.h>
using namespace std;

struct node
{
    char name[20];
    char num[20];
    char sex[10];
    int age;
    char cla[20];
    char hel[20];

    node *next;

    friend istream& operator >>(istream & is,node & L)
    {
        is >> L.name >> L.num >> L.sex >> L.age >> L.cla >> L.hel;
        return is;
    }
    friend ostream& operator <<(ostream & os,node & L)
    {
        os << L.name << L.num << L.sex << L.age << L.cla << L.hel<<endl;
        return os;
    }

};

class Stack
{
public:
    Stack();
    ~Stack();
    void push(node & x);
    void pop();
    node getfront(node & x);
    bool Isempty();
private:
    node* top;
};
Stack::Stack()
{
    top=NULL;
}
Stack::~Stack()
{
    while(top!=NULL)
    {
        top=top->next;
        delete top;
    }
}

void Stack::push(node & x)
{
    node *newnode=new node(x);
    newnode->next = top;
    top = newnode;
}
void Stack::pop()
{
    node *p=top;
    top=top->next;
    delete p;
}
node Stack::getfront(node & x)
{
    return *top;
}
bool Stack::Isempty()
{
    if (top==NULL)
    {
        return true;
    }

    return false;
}

class List
{
public:
    node* first;
    List( )
    {
        first=NULL;
    }
    ~List()
    {
        while(first!=NULL)
        {
            node *p=first->next;
            delete first;
            first=p;

        }
    }
    void input(node & x)
    {
        if(first==NULL)
        {
            node *newnode=new node(x);
            first=newnode;
        }
        else
        {
            node *newnode=new node(x);
            node *current=first;
            while(current->next!=NULL)
            {
                current=current->next;
            }
            newnode=current->next;
            current=newnode;
        }
    }
};

int main()
{
    freopen("in.txt","r",stdin);
    int n;
    cin>>n;
    List LL;
    for(int i=0; i<n; i++)
    {
        node x;
        cin>>x;
        LL.input(x);
    }
    Stack SS;
    for(int i=0; i<n; i++)
    {
        SS.push(*LL.first);
        LL.first=LL.first->next;
    }
    SS.pop();

    return 0;
}
