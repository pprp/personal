#include <iostream>
#include <cstring>
#include <cstdio>
#include <queue>
#include <stack>

using namespace std;
queue<int> qu;
stack<int> st;
const int maxn = 1000;
int mp[maxn][maxn];
bool vis[maxn];
int n;
///递归的写法
//void dfs(int st)
//{
//    printf("%d ",st);
//    vis[st] = 1;
//    for(int i = 0 ; i < n; i++)
//        if(mp[st][i] != 0 && vis[i] == 0)
//            dfs(i);
//    return ;
//}
///非递归，用栈实现
///注意，一开始取栈顶以后不会弹出，开始vis标记操作，判断然后进栈
void dfs(int stt)
{
    int tmp;
    st.push(stt);
    vis[stt] = 1;
    cout << stt << " ";
    while(!st.empty())
    {
        int i;
        tmp = st.top();
        for(i = 0 ; i < n ; i++)
        {
            if(mp[tmp][i] != 0 && vis[i] == 0)
            {
                cout << i << " ";
                vis[i] = 1;
                st.push(i);
                break;
            }
        }
        if(i == n)
            st.pop();
    }
}
void wfs(int i)
{
    vis[i] = 1;
    qu.push(i);
    while(!qu.empty())
    {
        int st = qu.front();
        cout << st << " ";
        for(int i = 0 ; i < n ; i++)
        {
            if(mp[st][i] != 0 && vis[i] == 0)
            {
                vis[i] = 1;
                qu.push(i);
            }
        }
        qu.pop();
    }
}
int main()
{
    freopen("in.txt","r",stdin);
    scanf("%d",&n);
    memset(mp,0,sizeof mp);
    memset(vis,0,sizeof vis);
    for(int i = 0; i < n ; i++)
        for(int j = 0 ; j < n ; j++)
            scanf("%d",&mp[i][j]);
    cout << "DFS" << endl;
    for(int i = 0 ; i < n; i++)
    {
        dfs(i);
        memset(vis,0,sizeof vis);
        while(!st.empty())
        {
            st.pop();
        }
        cout << endl;
    }
    cout << "WFS" << endl;
    for(int i = 0 ; i < n ; i++)
    {
        wfs(i);
        memset(vis,0,sizeof vis);
        while(!qu.empty())
            qu.pop();
        cout << endl;
    }
    return 0;
}
