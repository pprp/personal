#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <iomanip>

using namespace std;

class stu
{
protected:
    int num;
    char name[100];
    double sc1,sc2,sc3;
public:
    stu * next;
    double avg;
    int rk;
    friend istream& operator>>(istream& in, stu& ss)
    {
        in >> ss.num >> ss.name >> ss.sc1 >> ss.sc2 >> ss.sc3;
        ss.avg = (ss.sc1 + ss.sc2 + ss.sc3)/3.0;
        ss.rk = -1;
        ss.next = NULL;
        return in;
    }
    friend ostream& operator<<(ostream& out,stu &ss)
    {
        out << setiosflags(ios::fixed) << setprecision(2);
        out << ss.num<<" " << ss.name<<" " << ss.sc1<<" " << ss.sc2
            <<" " << ss.sc3<<" "<<ss.avg<<" ";
        out<< ss.rk<<" " << endl;
        return out;
    }
};
bool cmp(const stu & a, const stu & b)
{
    return a.avg > b.avg;
}

//class List
//{
//public:
//    stu * first;
//    List()
//    {
//        first = NULL;
//    }
//    void input(int sz)
//    {
//        stu * su = new stu;
//        cin >> *su;
//        first = su;
//        stu * p = first;
//        for(int i = 0 ; i < sz-1; i++)
//        {
//            su = new stu;
//            cin >> *su;
//            p->next = su;
//            p = p->next;
//        }
//    }
//    void output()
//    {
//        stu * p = first;
//        while(p != NULL)
//        {
//            cout << *p;
//            p = p->next;
//        }
//    }
//};

int main()
{
//    freopen("in.txt","r",stdin);
    int sz;
    cin >> sz;
    vector<stu> vt;
    stu* nn = new stu;
    for(int i = 0 ; i < sz; i++)
    {
        cin >> *nn;
        vt.push_back(*nn);
    }
    sort(vt.begin(),vt.end(),cmp);
    for(int i = 1; i <= sz; i++)
    {
        vt[i-1].rk = i;
    }
    for(int i = 0; i < sz; i++)
    {
        cout << vt[i];
    }
//    List la;
//    la.input(sz);
//    la.output();

    return 0;
}
