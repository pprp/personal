#include <iostream>
#include <cstring>
#include <cstdio>

using namespace std;
int n;
int a[1000];


void Pprint(int root)
{
    if(root <= n)
    {
        if(a[root] != 0)
            cout << a[root] << " ";
        Pprint(root*2);
        Pprint(root*2+1);
    }
}

void Mprint(int root)
{
    if(root <= n)
    {
        Mprint(root*2);
        if(a[root] != 0)
            cout << a[root] << " ";
        Mprint(root*2+1);
    }
}

void Bprint(int root)
{
    if(root <= n)
    {
        Bprint(root*2);
        Bprint(root*2+1);
        if(a[root] != 0)
            cout << a[root] << " ";
    }
}


int main()
{
//    freopen("in.txt","r",stdin);

    memset(a,0,sizeof(a));
    cin >> n;
    for(int i = 1 ; i <= n; i++)
        cin >> a[i];
    Pprint(1);
    cout << endl;
    Mprint(1);
    cout << endl;
    Bprint(1);
    cout << endl;
    return 0;
}
