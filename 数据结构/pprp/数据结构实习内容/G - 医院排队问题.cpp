#include <iostream>
#include <queue>
#include <cstdio>

using namespace std;

queue<int> qu;

int main()
{
    freopen("in.txt","r",stdin);
    char ch;
    int num;
    while(cin >> ch)
    {
        if(ch == 'A' || ch == 'a')
        {
            cin >> num;
            qu.push(num);
        }
        else if(ch == 'n' || ch == 'N')
        {
            if(!qu.empty())
            {
                int tmp = qu.front();
                cout << "病历号为" << tmp << "的病人就诊" << endl;
                qu.pop();
            }
            else
            {
                cout << "无病人就诊" << endl;
            }
        }
        else if(ch == 'S' || ch == 's')
        {
            cout << "今天不再接收病人排队，下列排队的病人依次就诊：";
            while(!qu.empty())
            {
                cout << qu.front()<<" ";
                qu.pop();
            }
        }
        else
        {
            cout << "输入命令不合法！" << endl;
        }
    }

    return 0;
}
