#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>

using namespace std;

class SeqList
{
protected:
    int *data;
    int last;
    int maxSize;
public:
    SeqList(int sz = 10000):data(new int[sz]),last(-1),maxSize(sz) {}
    ~SeqList()
    {
        delete[]data;
    }
    void input(int sz)
    {
        for(int i = 0 ; i < sz; i++)
        {
            cin >> data[i];
        }
        last = sz-1;
    }
    void output(bool tag)const
    {
        for(int i = 0 ; i < last ; i++)
        {
            cout <<data[i] <<' ';
        }
        cout << data[last] << ' ';
        if(tag == 1)
            cout << endl;
    }
    bool Insert(int i, int & x)
    {
        for(int j = last; j >= i; j--)
            data[j+1] = data[j];
        data[i] = x;
        last++;
        return true;
    }
    bool Delete(int i)
    {
        i--;
        for(int j = i ; j < last; j++)
        {
            data[j] = data[j+1];
        }
        last--;
        return true;
    }
    int Find(int &x)
    {
        for(int i = 0 ; i <= last; i++)
        {
            if(data[i] == x)
                return i;
        }
        return -1666;
    }
    void inverse()
    {
        for(int i = 0 ; i <= last/2; i++)
        {
            int tmp = data[i];
            data[i] = data[last-i];
            data[last-i] = tmp;
        }
    }
    void Merge(SeqList & L)
    {
        for(int i = 1 ; i <= last+1; i++)
        {
            L.data[L.last+i] = data[i-1];
        }
        int len = L.last + last + 2;
        L.last = len-1;
        sort(L.data,L.data+L.last);
    }
    void Traver()
    {
        for(int i = 0; i<last; i++)
            cout<<data[i]<<' ' ;
        cout<<data[last]<<' '<<endl;
        return ;
    }
};

int main()
{
//    freopen("1.in","r",stdin);
    SeqList la;
    int sz;
    cin >> sz;
    la.input(sz);
    int i, x;
    cin >> x >> i;
    la.Insert(i-1,x);
    la.output(1);
    cin >> x;
    la.Delete(x);
    la.output(1);
    cin >> x;
    int index = la.Find(x);
    if(index != -1666)
        cout << index+1 << endl;
    else
        cout << "Not found" << endl;
    la.inverse();
    la.output(1);
    cin >> sz;
    SeqList lala;
    lala.input(sz);
    la.Merge(lala);
    lala.output(0);

    return 0;
}
