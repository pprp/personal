#include <iostream>
#include <cstdio>
#include <cstring>
#include <queue>

using namespace std;
const int maxn = 1000;
int main()
{
    char ch[maxn];
    int rec[maxn];
    memset(rec,0,sizeof(rec));
    scanf("%s",ch);
    int len = strlen(ch);
    int level = 1;
    int maxLevel = -10000;
    for(int i = 0 ; i < len ; i++)
    {
        if(ch[i] == '(')
        {
            level++;
            continue;
        }
        else if(ch[i] == ')')
        {
            level--;
            continue;
        }
        else if(ch[i] == ',')
        {
            continue;
        }

        rec[i] = level;
        maxLevel = max(maxLevel,level);
    }
//    cout << maxLevel << endl;
    for(int i = 1 ; i <= maxLevel ; i++)
    {
        for(int j = 0 ; j < len; j++)
        {
            if(rec[j] == i)
            cout << ch[j];
        }
    }
    cout << endl;

    return 0;
}
