#include <iostream>
#include <cstdio>

using namespace std;

class Node
{
public:
    int a;
    Node * next;
    Node(int val = 0)
    {
        a = val;
        next = NULL;
    }
};

//bu dai tou jie dian
class List
{
protected:
    Node * first;
public:
    List()
    {
        first = NULL;
    }
    ~List()
    {
        if(first != NULL)
        {
            Node * p;
            p = first;
            while(p->next != NULL)
            {
                first = p->next;
                delete p;
                p = first;
            }
        }
        first = NULL;
    }
    //test:ok
    void Input(int sz)
    {
        Node * p;
        int val;
        cin >> val;
        Node * newnode = new Node(val);
        first = newnode;
        p = first;
        for(int i = 0 ; i < sz-1; i++)
        {
            cin >> val;
            p->next = new Node(val);
            p = p->next;
        }
    }
    //test:ok
    void Output()
    {
        Node * p = first;
        while(p != NULL)
        {
            cout << p->a << " ";
            p = p->next;
        }
        cout << endl;
    }
    //test:ok
    void Insert(int x, int i)
    {
        int pos = 1;
        Node * p = first;
        while(p->next != NULL)
        {
            pos++;
            p = p->next;
            if(pos == i-1)
                break;
        }
        Node * newnode = new Node(x);
        newnode->next = p->next;
        p->next = newnode;
    }
    //test:ok
    void Delete(int i)
    {
        int pos = 1;
        Node * p = first;
        while(p->next != NULL)
        {
            pos++;
            p = p->next;
            if(pos == i-1)
                break;
        }
        Node * q = p->next;
        p->next = q->next;
        delete q;
        q = NULL;
    }
    //test:ok
    void Find(int x)
    {
        int pos = 1;
        Node *p = first;
        while(p->next != NULL)
        {
            if(p->a == x)
            {
                cout << pos << endl;
                return;
            }
            p = p->next;
            pos++;
        }
        cout << "Not found" << endl;
    }
    //test:ok
    void Inverse()
    {
        Node * tmp = NULL;
        Node * p = first;
        while(p != NULL)
        {
            first = p->next;
            p->next = tmp;
            tmp = p;
            p = first;
        }
        first = tmp;
        return ;
    }
    //test:
    void Merge(List & L)
    {
        Node * lp = L.first;
        Node * p = first;
        while(lp != NULL && p != NULL)
        {
            if(lp->a > p->a)
            {
                cout << p->a << " ";
                p = p->next;
            }
            else
            {
                cout << lp->a << " ";
                lp = lp->next;

            }
        }

        while(lp != NULL)
        {
            cout << lp->a <<" ";
            lp = lp->next;
        }

        while(p != NULL)
        {
            cout << p->a <<" ";
            p = p->next;
        }
        cout << endl;
    }

};

int main()
{
//    freopen("in.txt","r",stdin);
    int i, x, sz;
    cin >> sz;
    List L;
    L.Input(sz);
    cin >> x >> i;
    L.Insert(x,i);
    L.Output();
    cin >> i;
    L.Delete(i);
    L.Output();
    cin >> x;
    L.Find(x);
    L.Inverse();
    L.Output();

    List LL;
    cin >> sz;
    LL.Input(sz);

    LL.Merge(L);

    return 0;
}
