#include<cstdio>
#include<cstring>
#include<vector>
#include <iostream>
#include<queue>

using namespace std;
struct node
{
    int ii, jj;
};
int n, m;
const int maxn = 1000;
int Map[maxn][maxn], vis[maxn][maxn];//迷宫，标记数组
node Pre[maxn][maxn];//记录路径
queue<node> q;
vector<node> a;//记录结果路径用的
//int x[9] = {1,1,1,0,-1,-1,-1,0};
//int y[9] = {1,0,-1,-1,-1,0,1,1};
int x[] = {1,1,0,-1,-1,-1,0,1};
int y[] = {0,-1,-1,-1,0,1,1,1};
int judge(int xx, int yy)
{
    if(xx >= 1 && yy >= 1 && xx <= n && yy <= m &&
            !vis[xx][yy] && Map[xx][yy] == 0)
        return 1;
    else
        return 0;
}
void Bfs(int u, int v)
{
    int i;
    memset(vis, 0, sizeof(vis));
    while(!q.empty()) q.pop();//初始化队列
    q.push((node)
    {
        u, v
    });//入队列最开始0,0点
    vis[u][v] = 1;//标记一下已经走过&&记录路径
    node t;
    int xx, yy;
    while(!q.empty())
    {
        t = q.front();
        q.pop();
        if(t.ii == n && t.jj == m)//到达终点
        {
            a.push_back((node)
            {
                t.ii, t.jj
            });//记录路径
            while(t.ii || t.jj)//直到起点退出循环
            {
                xx = t.ii, yy = t.jj;
                a.push_back((node)
                {
                    Pre[xx][yy].ii, Pre[xx][yy].jj
                });//存入上一点
                t.ii = Pre[xx][yy].ii;
                t.jj = Pre[xx][yy].jj;
            }
            for(i = 0; i < a.size()-1; i++)//反向输出
            {
                printf("(%d, %d) ", a[i].ii, a[i].jj);
            }
            return ;

        }
        for(i = 0; i < 8; i++)//上下左右的走
        {
            xx = t.ii + x[i];
            yy = t.jj + y[i];
            if(judge(xx, yy))//判断下一步可以走
            {
                vis[xx][yy] = vis[t.ii][t.jj] + 1;//路径++
                Pre[xx][yy] = (node)
                {
                    t.ii, t.jj
                };//记录好那一点到的这一点
                q.push((node)
                {
                    xx, yy
                });//入队列
            }
        }
    }
}
int main()
{
    freopen("in.txt","r",stdin);
    cin >> n >> m;
    int i, j;
    for(i = 1; i <= n; i++)
        for(j = 1; j <= m; j++)
            scanf("%d", &Map[i][j]);

    Bfs(1, 1);//最开始0，0点
    a.clear();
    return 0;
}
