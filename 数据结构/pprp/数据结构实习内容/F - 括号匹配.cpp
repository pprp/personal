#include <iostream>
#include <stack>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main()
{
    freopen("in.txt","r",stdin);
    stack<char> kh;
    char tmp;

    char ch = getchar();
    kh.push(ch);

    while(1)
    {
        ch = getchar();
        if(ch == '#')
            break;
        if(!kh.empty())
            tmp = kh.top();
        kh.push(ch);
        if(ch+tmp == 248 || ch+tmp == 184 || ch+tmp == 81)
        {
            kh.pop();
            kh.pop();
        }
    }

    if(kh.empty())
        cout << "ƥ��" << endl;
    else
        cout << "��ƥ��" << endl;
    return 0;
}
