#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
const int maxn = 1000;
int indegree[maxn];
int mp[maxn][maxn];
int nn;
bool vis[maxn];

bool topuSort()
{
    for(int i = 0 ; i < nn ; i++)
    {
        int tag = -1;
        //找到入度为0的点
        for(int i = 0 ; i < nn; i++)
            if(!vis[i] && !indegree[i])
            {
//                vis[i] = 1;
                tag = i;
                break;
            }
        //删除这个点
       indegree[tag] = -1;
        vis[tag] = 1;
        for(int i = 0 ; i < nn ; i++){
            if(mp[tag][i] && !vis[i])
                indegree[i]--;
        if(tag == -1)
            return true;
        }
    }
    return false;
}
int main()
{
//    freopen("in.txt","r",stdin);
    memset(indegree,0,sizeof indegree);
    memset(vis,0,sizeof vis);
    for(int i = 0; i < maxn; i++)
        for(int j = 0; j < maxn ; j++)
            mp[i][j] = 0;
    scanf("%d",&nn);
    for(int i = 0; i < nn ; i++)
        for(int j = 0 ; j <nn ; j++)
        {
            cin >> mp[i][j];
            if(mp[i][j] == 1)
                indegree[j]++;
        }
    if(topuSort())
        cout << "YES" << endl;
    else
        cout << "NO" << endl;
    return 0;
}
