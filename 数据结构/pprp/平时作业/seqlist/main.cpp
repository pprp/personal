#include <iostream>
#include "linearList.h"

using namespace std;

int main()
{
    SeqList<int> st;
    int sz;
    cout << "enter size of the array." << endl;
    cin >> sz;

    st.input(sz);
    cout << "the array is:" << endl;
    st.output();

    return 0;
}
