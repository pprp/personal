#ifndef LINEARLIST_H_INCLUDED
#define LINEARLIST_H_INCLUDED
#include <iostream>
#include <algorithm>

template <class T>
class LinearList
{
public:
    virtual bool Insert(int i, T & x) = 0;
    virtual void Sort() = 0;
    virtual void input(int sz) = 0;
    virtual void output() const = 0;
    virtual bool Delete(T & x) = 0;
    virtual void Resize(int n) = 0;
};

template <class T>
class SeqList : public LinearList<T>
{
protected:
    T *data;
    int last;
    int maxSize;
public:
    SeqList(int sz = 1000):data(new T[sz]),last(-1),maxSize(sz) {}
    ~SeqList()
    {
        delete[]data;
    }
    void input(int sz);
    void output()const;
    bool Insert(int i, T & x);
    void Sort();
    bool Delete(T & x);
    void Resize(int n);
};

using namespace std;

template <class T>
void SeqList<T> :: input(int sz)
{
    if(sz < 0 || sz > maxSize-1)
        return ;
    if(last == maxSize)
        return ;
    for(int i = 0 ; i < sz; i++)
    {
        cin >> data[i];
    }
    last = sz-1;
}

template <class T>
void SeqList<T>::output()const
{
    for(int i = 0 ; i < last ; i++)
    {
        cout << data[i] << " ";
    }
    cout << endl;
}

template <class T>
bool SeqList<T> :: Insert(int i, T & x)
{
    if(i < 0 || i > maxSize)
        return false;
    if(last == maxSize-1)
        return false;
    for(int j = last; j >= i; j--)
        data[j+1] = data[j];
    data[i] = x;
    last++;
    return true;
}

template <class T>
void SeqList<T> :: Sort()
{
    sort(data,data+last);
    return ;
}

template <class T>
bool SeqList<T> :: Delete(T & x)
{
    int i, j;
    for(i = 0; i < last; i++)
    {
        if(data[i] == x)
            break;
    }
    if(i == last)
        return false;
    for(j = i ; j < last - 1; j++)
    {
        data[i] = data[i+1];
    }
    last--;
    return true;
}

template <class T>
void SeqList<T> :: Resize(int n)
{
    if(n <= 0)
    {
        cout << "invalid number." << endl;
        return ;
    }

    if(n != maxSize)
    {
        T * newarray = new T[n];
        if(newarray == NULL)
        {
            cout << "false." << endl;
            return ;
        }
        int sz = last + 1;
        T * stptr = data;
        T * edptr = newarray;
        while(sz--)
        {
            *stptr = *edptr;
            stptr++, edptr++;
        }
        delete []data;
        data = newarray;
        maxSize = n;
    }
}


#endif // LINEARLIST_H_INCLUDED
