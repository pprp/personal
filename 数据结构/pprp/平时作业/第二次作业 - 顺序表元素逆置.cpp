/*
@theme: 第二次数据结构作业 - 顺序表的逆置
@writer:pprp
@begin:19:35
@end:20:52
@declare:
@data:2017/9/7
*/

#include <iostream>
#include <cstdio>
#include <cstdlib>

using namespace std;

const int defaultMaxSize = 100;

template <class T>
class LinearList
{
public:
    LinearList(){}
    ~LinearList(){}
    virtual void Reverse() = 0;
    virtual void input(int sz) = 0;
    virtual void output()const = 0;
};

template <class T>
class SegList : public LinearList<T>
{
private:
    T * data;
    int maxSize;
    int last;
public:
    SegList(int sz = defaultMaxSize)
    {
        if(sz > 0)
        {
            maxSize = sz;
            data = new T[maxSize];
            if(data == NULL)
                cout << "there is no room." << endl;
            last = -1;
        }

    }
    ~SegList()
    {
        maxSize = 0;
        delete []data;
        last = -1;
    }
    void Reverse();
    void input(int sz);
    void output()const;
};

template <class T>
void SegList<T> :: Reverse()
{
    //两个指针分别指向该顺序表的头和尾
    int stptr = 0;
    int edptr = last;
    for( ; stptr <= edptr; stptr++,edptr--)
    {
        swap(data[stptr],data[edptr]);
    }
    return ;
}

template <class T>
void SegList<T>:: input(int sz)
{
    if(last == maxSize-1)
        cout << " the array is full." << endl, exit(1);
    if(sz < 0 || sz > maxSize)
        cout << " the size of the array is too big/small." << endl, exit(1);
    last = sz - 1;

    cout << "please input the numbers of the array." << endl;
    for(int i = 0 ; i <= last; i++)
    {
        cin >> data[i];
    }
    return ;
}

template <class T>
void SegList<T>::output() const
{
    cout << "the array is as follow:" << endl;
    for(int i = 0 ; i <= last ; i++)
    {
        cout << data[i] << " ";
    }
    cout << endl;
}


int main()
{
    SegList<int> obj;
    cout << "please input the size of array." << endl;
    int sz = 0;
    cin >> sz;
    obj.input(sz);
    obj.output();

    cout << "reverse the array:" << endl;
    obj.Reverse();
    obj.output();

    return 0;
}
