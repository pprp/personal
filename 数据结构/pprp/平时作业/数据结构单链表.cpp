/*
@theme:带有附加头结点的单链表模板类
@writer:pprp
@begin:19:32
@end:19:
@error:
@date:2017/9/13
*/

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>

using namespace std;

template <class T>
class LinkNode
{
    T data;
    LinkNode * next;
    LinkNode(T& dt = 0):data(dt),next(NULL) {}
};

template <class T>
class LinearList
{
public:
    LinearList() {}
    ~LinearList() {}
    virtual int Size();
    virtual bool Insert(int i, T & x);
    virtual bool Remove(int i, T & x);

};

template <class T>
class List : public LinearList<T>
{
protected:
    LinkNode<T> * first;
public:
    List()
    {
        first = new LinkNode<T>;
    }
    List(const T & x)
    {
        first = new LinkNode<T>(x);
    }
    //拷贝构造函数
    List(List<T>& L);//ok
    ~List()//ok
    {
        makeEmpty();
    }
    void makeEmpty();//ok
    int Length()const;//ok
    LinkNode<T>* getHead()const//ok
    {
        return first;
    }
    LinkNode<T>* Search(T x);//ok
    LinkNode<T>* Locate(int i);//ok
    bool getData(int i, T & x)const;//ok
    void setData(int i, T & x);//ok
    bool Insert(int i, T & x);//ok
    bool Remove(int i, T & x);//ok
    bool IsEmpty()const//ok
    {
        return first->next == NULL ? true: false;
    }
    void Sort();//
    void Input();//ok
    void Output();//ok
    List<T> & operator=(List<T> & L);
};

//test:
template <class T>
List<T> :: List(List<T>& L)
{
    T val;
    LinkNode<T> * bf = L.getHead();
    LinkNode<T> * nw = first = new LinkNode<T>;
    while(bf->next != NULL)
    {
        val = bf->next->data;
        nw->next = new LinkNode<T>(val);
        nw = nw->next;
        bf = bf->next;
    }
    nw->next = NULL;
}
//test:
//attention:要保留头结点
template <class T>
void List<T> :: makeEmpty()
{
    LinkNode<T> * p;
    while(first->next != NULL)
    {
        p = first->next;
        first->next = p->next;
        delete p;
    }
    return ;
}

//test:
template <class T>
int List<T> :: Length()const
{
    int sz = 0;
    LinkNode<T>* p = first;
    while(p->next != NULL)
    {
        p = p->next;
        sz++;
    }
    return sz;
}
//test:
template <class T>
LinkNode<T>* List<T>::Search(T x)
{
    LinkNode<T> *p = first;
    while(p->next != NULL)
    {
        if(p->data == x)
            return p;
        p = p->next;
    }
    return NULL;
}
//test:
template <class T>
LinkNode<T>* List <T> :: Locate(int i)
{
    int cnt = 0;
    if(i < 0)return NULL;
    LinkNode<T>* p = first;
    while(p != NULL && cnt < i)
    {
        p = p->next ;
        cnt++;
    }
    return p;
}
//test:
template <class T>
bool List<T>::getData(int i, T & x)const
{
    if(i < 0)return false;
    LinkNode<T> * p = Locate(i);
    if(p == NULL)
        return false;
    x = p->data;
    return true;
}
//test:
template <class T>
void List<T>::setData(int i, T & x)
{
    if(i < 0) return false;
    LinkNode<T>* p =Locate(i);
    p->data = x;
}
//test:
template<class T>
bool List<T>::Insert(int i, T & x)
{
    if(i < 0) return false;
    LinkNode<T>* p = Locate(i-1);
    LinkNode<T>* newnode = new LinkNode<T>;
    newnode->data = x;
    newnode->next = p->next;
    p->next = newnode;
    return true;
}
//test:
template <class T>
bool List<T>::Remove(int i, T & x)
{
    if(i < 0) return false;
    LinkNode<T>* p = Locate(i-1);
    LinkNode<T>* del = p->next;
    p->next = del->next;
    x = del->data;
    delete del;
    return true;
}
//test:
template <class T>
void List<T>::Sort()
{

}
//test:
template <class T>
void List<T>::Input()
{
    LinkNode<T> * q = first->next;
    int sz;
    cout << "input the size:"<<endl;
    cin >> sz;
    while(sz--)
    {
        T val;
        cout <<"please enter the element:" << endl;
        cin >> val;
        LinkNode<T>* newnode = new LinkNode<T>;
        newnode->data = val;
        q->next = newnode;
        q = q->next;
    }
}
//test:
template<class T>
void List<T>::Output()
{
    LinkNode<T> * p = first->next;
    while(p != NULL)
    {
        cout << p->data << endl;
        p = p->next;
    }
    return ;
}
//test:
template <class T>
List<T> & List<T> :: operator=(List<T> & L)
{
    T val;
    LinkNode<T> * srcptr = L.getHead();
    LinkNode<T> * desptr = first = new LinkNode<T>;
    while(srcptr->next != NULL)
    {
        val = srcptr->next->data;
        desptr->next = new LinkNode<T>(val);
        desptr = desptr->next;
        srcptr = srcptr->next;
    }
}
int main()
{

    return 0;
}
