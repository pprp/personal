
#include <iostream>
#include <cstring>
#include <cstdio>

using namespace std;
const int defaultsz = 1000;

template<class T>
class DStack
{
private:
    T * d;
    int maxSize;
    int top1,top2;
public:
    int getmxsz()
    {
        return maxSize;
    }
    DStack(int sz = defaultsz):maxSize(sz),top1(-1),top2(maxSize)
    {
        d = new int[maxSize];
        for(int i = 0 ; i < maxSize; i++)
            d[i] = 0;
    }
    ~DStack()
    {
        delete[] d;
    }
    void Push(T & x,bool p)
    {
        if(IsFull())
        {
            cout << "+_+" << endl;
            return;
        }
        if(p == 0)
        {
            d[++top1] = x;
        }
        else
        {
            d[--top2] = x;
        }
        return ;
    }
    bool Pop(bool p,T & x)
    {
        if(p == 0)
        {
            if(IsEmpty(0))
                return false;
            x = d[top1];
            d[top1] = 0;
            top1--;
        }
        else
        {
            if(IsEmpty(1))
                return false;
            x = d[top2];
            d[top2] = 0;
            top2++;
        }
        return true;
    }
    bool GetTop(T & x,bool p)
    {
        if(p == 0)
        {
            if(IsEmpty(0))
                return false;
            x = d[top1];
        }
        else
        {
            if(IsEmpty(1))
                return false;
            x = d[top2];
        }
        return true;
    }
    bool IsEmpty(bool p)
    {
        if(p == 0)
        {
            if(top1 == -1)
                return true;
            else
                return false;
        }
        else
        {
            if(top2 == maxSize-1)
                return true;
            else
                return false;
        }
    }
    bool IsFull()
    {
        if(top1 == top2-1)
            return true;
        return false;
    }
    void judge()
    {
        for(int i = 0 ; i < maxSize; i++)
        {
            cout << d[i] << " ";
        }
        cout << endl;
    }
};

int main()
{
    freopen("in.txt","r",stdin);
    int sz;
    bool p;
    int x;
    cout << "input sz:";
    cin >> sz;

    DStack<int> ds(sz);

    for(int i = 0; i < ds.getmxsz() ; i++)
    {
        cin >> p >> x;
        ds.Push(x,p);
    }
    ds.judge();

    cout <<"gettop:" << endl;
    ds.GetTop(x,0);
    cout << x << endl;
    ds.GetTop(x,1);
    cout << x << endl;

    cin >> p;
    ds.Pop(p,x);
    cout << "x:" << x << endl;
    ds.judge();
    cin >> p;
    ds.Pop(p,x);
    cout << "x:" << x << endl;
    ds.judge();
    return 0;
}
