/*
测试数据：前序建立内容和要查找的结果
1 2 1  0 0 1 0 0 1 6 0 0 7 0 0
1
答案为4
*/
#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;
const int maxn = 1000;

template<class T>
struct BinTreeNode
{
    T data;
    BinTreeNode<T>* lchild, * rchild;
    BinTreeNode():lchild(NULL),rchild(NULL){}//构造函数1
    BinTreeNode(int dd):data(dd),lchild(NULL),rchild(NULL){}//构造函数2
};

template<class T>
class BinaryTree
{
public:
    int CountNode(BinTreeNode<T> *p,T x);
    void CreateTree(BinTreeNode<T>* & root)//创建一个二叉树
    {
        T tmp;
        cin >> tmp;
        if(tmp == 0)
            root = NULL;
        else
        {
            root = new BinTreeNode<T>(tmp);
            if(root == NULL)
            {
                return;
            }
            CreateTree(root->lchild);
            CreateTree(root->rchild);
        }
    }
};
template<class T>
int BinaryTree<T>::CountNode(BinTreeNode<T> *p,T x)//对二叉树进行计数
{
    if(p != NULL)//当前节点不为空
    {
        if(p->data == x)
            //如果该节点的值等于x，那么当前的计数应该是左右子树的计数和+1
            return CountNode(p->lchild,x) + CountNode(p->rchild,x) + 1;
        else
            //如果该节点的值不等于x，那么当前的计数应该是左右子树的计数和
            return CountNode(p->lchild,x) + CountNode(p->rchild,x);
    }
    //当前节点为空那么返回0
    return 0;
}

int main()
{
    BinTreeNode<int> * root = NULL;
    BinaryTree<int> obj;
    obj.CreateTree(root);
    int x;
    cin >> x;
    cout << obj.CountNode(root,x) << endl;
    return 0;
}
