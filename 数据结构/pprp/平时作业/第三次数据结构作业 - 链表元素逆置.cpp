#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>

using namespace std;

template <class T>
class LinkNode
{
public:
    T data;
    LinkNode * next;
    LinkNode(T dt = 0):data(dt),next(NULL) {}
};

template <class T>
class List
{
protected:
    LinkNode<T> * first;
public:
    List()
    {
        first = new LinkNode<T>;
    }
    List(const T & x)
    {
        first = new LinkNode<T>(x);
    }
    List(List<T>& L);//ok
    ~List()//ok
    {
        makeEmpty();
    }
    void makeEmpty();//ok
    void Input();//ok
    void Output();//ok
    void inverse();
};

template <class T>
void List<T>::inverse()
{
    if(first == NULL||first->next == NULL)
        return ;
    LinkNode<T>*head, *p;

    head = NULL;
    first = first->next;
    p = first;
    while(p != NULL)
    {
        p = p->next;
        first->next = head;
        head = first;
        first = p;
    }
    first = new LinkNode<T>;
    first->next = head;
}
template <class T>
List<T> :: List(List<T>& L)
{
    T val;
    LinkNode<T> * bf = L.getHead();
    LinkNode<T> * nw = first = new LinkNode<T>;
    while(bf->next != NULL)
    {
        val = bf->next->data;
        nw->next = new LinkNode<T>(val);
        nw = nw->next;
        bf = bf->next;
    }
    nw->next = NULL;
}
template <class T>
void List<T> :: makeEmpty()
{
    LinkNode<T> * p;
    while(first->next != NULL)
    {
        p = first->next;
        first->next = p->next;
        delete p;
    }
    return ;
}
template <class T>
void List<T>::Input()
{
    LinkNode<T> * q = first;
    int sz;
    cout << "input the size:"<<endl;
    cin >> sz;
    cout <<"please enter the element:" << endl;
    while(sz--)
    {
        T val;
        cin >> val;
        LinkNode<T>* newnode = new LinkNode<T>(val);
        q->next = newnode;
        q = q->next;
    }
}
template<class T>
void List<T>::Output()
{
    LinkNode<T> * p = first->next;
    while(p != NULL)
    {
        cout << p->data <<" ";
        p = p->next;
    }
    cout << endl;
    return ;
}

int main()
{
    freopen("in.txt","r",stdin);
    List<int> ll;
    ll.Input();
    ll.Output();
    ll.inverse();
    ll.Output();
    return 0;
}
