/*
@theme:百鸡百钱问题
@writer:pprp
@declare:对 x 进行枚举,
@date:2017/9/4
*/
#include <bits/stdc++.h>

using namespace std;

int main()
{
    int x[10], y[10], z[10];

    memset(x,0,sizeof(x));
    memset(y,0,sizeof(y));
    memset(z,0,sizeof(z));

    int cnt = 0;

    for(int i = 0 ; i <= 20 ; i++)
    {
        int a = i;
        int c = (300+3*a)/4;
        int b = (100-7*a)/4;
        int d = (300+3*a)%4;
        int e = (100-7*a)%4;
        if(a+b+c == 100 && a >= 0 && a <= 100
                && b >= 0 && b <= 100
                && c >= 0 && c <= 100
                && d == 0 && e == 0)
        {
            x[cnt] = a;
            y[cnt] = b;
            z[cnt] = c;
            cnt++;
        }
    }
    for(int i = 0 ; i < cnt ; i++)
    {
        cout << "ans " << i << ":" << endl;
        cout << x[i] << " " << y[i] << " " << z[i] << endl;
    }
    return 0;
}
