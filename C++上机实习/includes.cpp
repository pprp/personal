#include <bits/stdc++.h>

using namespace std;

class Point
{
public:
    double x;
    double y;

    Point():x(0),y(0) {}
    Point(double x,double y)
    {
        this->x = x;
        this->y = y;
    }
};

class Line
{
private:

    Point a;
    Point b;
public:
    Line();
    Line(const Point&a1,const Point&b1):a(a1),b(b1) {}
    double compute()
    {
        return sqrt((a.x-b.x)*(a.x-b.x) +
                    (a.y-b.y)*(a.y-b.y));
    }
};

class Triangle
{
private:
    Point a;
    Point b;
    Point c;
    Line L1;
    Line L2;
    Line L3;
// Line *L1 = new Line(a,b);
// Line *L2 = new Line(a,c);
// Line *L3 = new Line(b,c);
public:
    Triangle();
    Triangle(const Point&a1,const Point&a2,
             const Point&a3):a(a1),b(a2),c(a3),L1(a,b),L2(a,c),L3(b,c) {}
// Line L1(a,b);
// Line L2(a,c);
// Line L3(b,c);
    double zhouchang()
    {
        return L1.compute()+L2.compute()+L3.compute();
    }
    double mianji()
    {
        double a = L1.compute();
        double b = L2.compute();
        double c = L3.compute();

        double p = (a+b+c)/2.0;

        return sqrt(p*(p-a)*(p-b)*(p-c));
    }
};

int main()
{
    double x1,y1,x2,y2,x3,y3;
    cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;

    Point a(x1,y1);
    Point b(x2,y2);
    Point c(x3,y3);

    Triangle TRI(a,b,c);

    cout << TRI.zhouchang() << " " << TRI.mianji() << endl;

    return 0;
}
