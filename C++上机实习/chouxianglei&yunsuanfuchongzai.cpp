#include <bits/stdc++.h>

using namespace std;
class Shape
{
public:
    virtual double Area() = 0;
    virtual void Show() = 0;
    virtual ~Shape() {}
    friend bool operator ==(Shape & sh1,Shape & sh2)
    {
        return (abs(sh1.Area()-sh2.Area())<=1.0e-8);
    }
    friend bool operator >(Shape & sh1,Shape & sh2)
    {
        return (sh1.Area() > sh2.Area());
    }
    friend bool operator <(Shape & sh1,Shape & sh2)
    {
        return (sh1.Area() < sh2.Area());
    }
};
class Rectangle:public Shape
{
protected:
    double rectWidth;
    double rectHeight;
public:
    Rectangle(double w,double h)
    {
        rectHeight = h;
        rectWidth = w;
    }
    void Show()
    {
        cout << "W: "<<rectWidth;
        cout << "; H:"<<rectHeight;
        cout << "; Area: "<<Area()<<endl;
    }
    double Area()
    {
        return rectHeight*rectWidth;
    }
};
class Ellipse:public Shape
{
protected:
    double rectWidth;
    double rectHeight;
public:
    static const double PI = 3.1415926;
    Ellipse(double w,double h)
    {
        rectHeight = h;
        rectWidth = w;
    }
    void Show()
    {
        cout << "W: "<<rectWidth;
        cout << "; H:"<<rectHeight;
        cout << "; Area: "<<Area()<<endl;
    }
    double Area()
    {
        return PI*rectHeight*rectWidth/4.0;
    }
};
int main()
{
    int num;
    cin >> num;
    Shape ** sh = new Shape*[num];
    for(int i = 0 ; i < num ; i++)
    {
        double a,b;
        char shType;
        cin >> shType>>a>>b;
//由于作用域的问题，rectangle对象在这个地方执行完毕以后会被销毁，导致出现问题；

        switch(shType)
        {
        case 'R':
        {
            sh[i]=new Rectangle(a,b);
            break;
        }
        case 'E':
        {
            sh[i]=new Ellipse(a,b);
            break;
        }
        }
    }
    for(int i = 0 ; i < num; i++)
    {
        sh[i]->Show();
    }
    for(int i = 0 ; i < num - 1; i++)
    {
        for(int j = i+1; j < num ; j++)
        {
            if(*sh[i] == *sh[j])
            {
                cout << "Area of Shape["<<i<<"] is equal to Shape["<<j<<"]"<<endl;
            }
        }
    }

    int k;
    Shape* shTemp;
    for(int i=0; i<num-1; i++)
    {
        k=i;
        for(int j=i+1; j<num; j++)
        {
            if(*sh[j]>*sh[k])
            {
                k=j;
            }
        }
        shTemp=sh[i];
        sh[i]=sh[k];
        sh[k]=shTemp;
    }
    for(int i = 0 ; i < num; i++)
    {
        sh[i]->Show();
    }
    return 0;
}
