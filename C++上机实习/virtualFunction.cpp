#include <bits/stdc++.h>

using namespace std;

class Person
{
protected:
    string szName;
public:
    Person():szName("")
    {
    }
    Person(string name):szName(name)
    {
    }
    virtual ~Person() {}

    virtual void Print()
    {
        cout << "Person "<< szName<<endl;
    }
    virtual void setName(string name)
    {
        szName = name;
    }
    virtual void setNumber(int) {}
    virtual void setResearch(string) {}
    virtual void setYear(int) {}
};

class Student : public Person
{
protected:
    int iNumber;
public:
    Student():iNumber(0) {}
    Student(string name,int num):Person(name),iNumber(num) {}
    void Print()
    {
        cout << "Student " << szName <<" "<<iNumber<<endl;
    }
    void setNumber(int num)
    {
        iNumber = num;
    }
};

class Teacher : public Person
{
protected:
    int iYear;
public:
    Teacher():iYear(0) {}
    Teacher(string name,int year):Person(name),iYear(year) {}
    void Print()
    {
        cout << "Teacher " << szName << " " << iYear;
    }
    void setYear(int year)
    {
        iYear = year;
    }
};

class Graduate : public Student
{
protected:
    string szResearch;
public:
    Graduate():szResearch("") {}
    Graduate(string name,string research,int num):
        Student(name,num),szResearch(research) {}
    void Print()
    {
        cout << "Graduate " << szName<<" "<<iNumber<<" "<<szResearch << endl;
    }
    void setResearch(string res)
    {
        szResearch = res;
    }
};

int main()
{
    int num;

    cin >> num;

    Person *m_person[num];

    Person per;
    Student stu;
    Teacher tea;
    Graduate gra;

    for(int i = 0 ; i < num ; i++)
    {
        string tmp;

        cin >> tmp;

        if(tmp == "Person")
        {
            string tmp1;
            cin >> tmp1;
            m_person[i] = &per;
            m_person[i]->setName(tmp1);
        }
        else if(tmp == "Student")
        {
            string tmp1;
            int num;
            cin >> tmp1 >> num;
            m_person[i] = &stu;
            m_person[i]->setName(tmp1);
            m_person[i]->setNumber(num);
        }
        else if(tmp == "Graduate")
        {
            string name;
            string name2;
            int num;
            cin >> name >> num >> name2;
            m_person[i] = &gra;
            m_person[i]->setName(name);
            m_person[i]->setNumber(num);
            m_person[i]->setResearch(name2);
        }
        else if(tmp == "Teacher")
        {
            string tmp1;
            int tmp3;
            cin >> tmp1 >> tmp3;
            m_person[i] = &tea;
            m_person[i]->setName(tmp1);
            m_person[i]->setYear(tmp3);
        }
    }

    string test;
    while(cin >> test)
    {
        if(test == "exit")
        {
            return 0;
        }
        else
        {
            int idx = test[0]-'0';
            m_person[idx]->Print();
        }
    }
    return 0;
}
