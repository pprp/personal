#include <bits/stdc++.h>

using namespace std;

class StudentRecord
{
public:
    string stuName;
    int stuNo;
// StudentRecord():stuName("NULL"),stuNo(0) {}
// StudentRecord(string str,int num):stuName(str),stuNo(num) {}
    void print()
    {
        cout << "Name: "<< stuName;
        cout <<", Number: " << stuNo<<endl;
    }
};

class StudentNode
{
public:
    StudentRecord data;
    StudentNode* next;

// StudentNode():next(NULL) {}
    StudentNode(StudentRecord data,StudentNode *next=NULL):
        data(data),next(next) {}
};

class LinkedList
{
public:
    StudentNode* head;
    LinkedList()
    {
        head = NULL;
    }
    LinkedList(StudentNode & node):head(NULL) {}

    void headInsert(StudentNode& node);
    void headDelete();
    void tailInsert(StudentNode node);
};

class LinkedStack : public LinkedList
{
public:
// LinkedStack()
// {}
// LinkedStack(StudentNode &node):LinkedList(node)
// {}
    void Push(StudentRecord record)
    {
        StudentNode tmp(record,NULL);
        headInsert(tmp);
    }
    bool Pop(StudentRecord &record)
    {
        if(head == NULL)
            return false;
        else
        {
            StudentNode tmp = *(head);
            record = tmp.data;
            headDelete();
            return true;
        }
    }
};

class LinkedQueue : public LinkedList
{
public:
// LinkedQueue() {}
// LinkedQueue(StudentNode & node):LinkedList(node) {}

    void EnQueue(StudentRecord& record);
    bool DeQueue(StudentRecord &record);
};

int main()
{
    LinkedQueue Queue;
    LinkedStack Stack;
    string order;
    while(cin >> order)
    {
        if(order == "Push")
        {
            StudentRecord tmp;
            cin >> tmp.stuName >> tmp.stuNo;
            Stack.Push(tmp);
        }
        else if(order == "EnQueue")
        {
            StudentRecord tmp;
            cin >> tmp.stuName >> tmp.stuNo;
            Queue.EnQueue(tmp);
        }
        else if(order == "Pop")
        {
            StudentRecord tmp;
            if(!Stack.Pop(tmp))
                cout<<"Stack is empty!"<<endl;
            else
            {
                tmp.print();
            }

        }
        else if(order == "DeQueue")
        {
            StudentRecord tmp;
            if(!Queue.DeQueue(tmp))
                cout << "Queue is empty!" << endl;
            else
            {
                tmp.print();
            }

        }
        else if(order == "Exit")
        {
            return 0;
        }
        else
        {
            cout <<"Input error!"<< endl;
        }
    }
    return 0;
}
void LinkedQueue::EnQueue(StudentRecord& record)
{
    StudentNode tmp(record,NULL);
    tailInsert(tmp);
}

bool LinkedQueue::DeQueue(StudentRecord &record)
{
    if(head == NULL)return false;
    else
    {
        StudentNode tmp = *head;
        record = tmp.data;
        headDelete();
        return true;
    }

}
void LinkedList::headInsert(StudentNode& tmp)
{
    if(head==NULL)
    {
        head = new StudentNode(tmp.data,NULL);
    }
    else
    {
        StudentNode* p;
        p = new StudentNode(tmp.data,head);
        head = p;
    }
}
void LinkedList::headDelete()
{
    StudentNode* temp = head;
    if(temp->next == NULL)
    {
        head = NULL;
        delete temp;
    }
    else
    {
        head = temp->next;
        delete temp;
    }
}
void LinkedList::tailInsert(StudentNode tmp)
{
    if(head == NULL)
    {
        head = new StudentNode(tmp.data,NULL);
    }
    else
    {
        StudentNode* p = head;
        while(p->next)
        {
            p = p->next;
        }
        p->next = new StudentNode(tmp.data,NULL);
    }
}
