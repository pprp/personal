#include <bits/stdc++.h>

using namespace std;

class Person
{

public:
    string Name;
    Person():Name("NULL"),Gender("NULL"),Age(0)
    {
        cout << "Person" << Name << "constructed" << endl;
    }
    Person(string name,string gender,int age):Name(name),Gender(gender),Age(age)
    {
        cout << "Person" << Name << "constructed" << endl;
    }
    virtual~Person()
    {
        cout << "Person" <<Name <<"destructed" <<endl;
    }
    string getGender()
    {
        return Gender;
    }
    int getAge()
    {
        return Age;
    }
protected:
    string Gender;
    int Age;
};

class StudentRecord : virtual public Person
{
public:
    long long Number;
    string ClassName;
    static int TotalCount; //pay attention!!
    StudentRecord():Number(0),ClassName("NULL"),Score(0)
    {
        cout << "Student" <<Name<<"constructed"<<endl;
        TotalCount++;
    }
    StudentRecord(string name,string gender,int age,
                  long long num,string str,int sco):Person(name,gender,age)
    {

        Number = num;
        ClassName = str;
        Score = sco;
        cout << "Student" << Name << "constructed" << endl;
        TotalCount++;
    }
    ~StudentRecord()
    {
        cout << "Student" <<Name<<"destructed"<<endl;

    }
    static int getTotalCount()
    {
        return TotalCount;
    }
protected:
    int Score;
};
class TeacherRecord : virtual public Person
{
public:
    string CollegeName;
    string DepartmentName;
    TeacherRecord():CollegeName("NULL"),DepartmentName("NULL"),Year(0)
    {
        cout << "teacher" << Name << "constructed"<<endl;
    }
    TeacherRecord(string name,string gender,int age,
                  string str1,string str2,int year):Person(name,gender,age)
    {
        CollegeName = str1;
        DepartmentName = str2;
        Year = year;
        cout << "teacher" << Name << "constructed"<<endl;
    }
    ~TeacherRecord()
    {
        CollegeName.resize(0);
        DepartmentName.resize(0);
        cout << "teacher" << Name << "destructed"<<endl;

    }
protected:
    int Year;
};
class TeachingAssistant : public StudentRecord, public TeacherRecord
{
public:
    string LectureName;
    TeachingAssistant():LectureName("NULL")
    {
        cout << "teachingassistant" << Name << "constructed"<< endl;
    }
    TeachingAssistant(string name,string gender,int age,
                      long long num,string str,int sco,
                      string str1,string str2,int year,
                      string str3):
        Person(name,gender,age),
        StudentRecord(name,gender,age,num,str,sco),
        TeacherRecord(name,gender,age,str1,str2,year)
    {
        LectureName = str3;
        cout << "teachingassistant" << Name << "constructed"<< endl;
    }
    ~TeachingAssistant()
    {
        LectureName.resize(0);
        cout << "teachingassistant" << Name << "destructed"<< endl;

    }
    void Show()
    {
        cout <<"Name:"<< Name
             <<" Gender:"<<getGender()<<
             " Age:"<<getAge()<<" Number:"
             <<Number<<" ClassName:"<<ClassName
             <<" TotalCount:"<<getTotalCount()<<
             " Score:"<< Score<<" CollegeName:"<<
             CollegeName<<" DepartmentName:"<<DepartmentName
             <<" Year:"<<Year<<
             " LectureName:"<<LectureName<<endl;
    }
    void SetName(string name)
    {
        Name = name;
    }
};

int StudentRecord::TotalCount = 0;

int main()
{

    TeachingAssistant TA("郑七","男",22,2010123,"软20101",89,"信息","软件",1,"数据结构");

    TA.Show();



    TA.SetName("郑八");

    TA.Show();

    return 0;
}
