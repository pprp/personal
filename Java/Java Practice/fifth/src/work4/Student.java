package work4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Student implements Serializable {
	private int id;
	private String name;
	private int age;

	public static void main(String[] args) throws IOException,
			ClassNotFoundException {
		Student stu1 = new Student(1001, "Lee", 21);
		Student stu2 = new Student(1003, "zhang", 22);
		Student stu3 = new Student(1004, "liang", 18);
		Student stu4 = new Student(1002, "dong", 19);
		File ff = new File("student.txt");

		FileOutputStream fileOut = new FileOutputStream(ff);
		ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
		objectOut.writeObject(stu1);
		objectOut.writeObject(stu2);
		objectOut.writeObject(stu3);
		objectOut.writeObject(stu4);
		fileOut.close();
		objectOut.close();

		FileInputStream fileIn = new FileInputStream("student.txt");
		ObjectInputStream objectIn = new ObjectInputStream(fileIn);
		for (int i = 0; i < 4; i++) {
			Student tmp = (Student) objectIn.readObject();
			System.out.println(tmp);
		}
		fileIn.close();
		objectIn.close();
	}
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Student(int id, String name, int age) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
}
