package work3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Copy {
	public Copy() throws IOException {
		File name;
		name = new File(".\\files");

		File namecopy;
		namecopy = new File(".\\filescopy");
		if (name.isDirectory())
			namecopy.mkdir();
		copy(name,namecopy);
	}
	//remember it
	public void copy(File src,File dest) throws IOException {
		if(src.isDirectory()){
			if(!dest.exists()){
				dest.mkdir();
			}
			String files[] = src.list();
			for(String file:files){
				File srcFile = new File(src,file);
				File destFile = new File(dest,file);
				copy(srcFile,destFile);
			}
		}else if(src.isFile()){
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dest);
			
			byte [] buffer = new byte[1024];
			
			int len;
					
			while((len = in.read(buffer))>0){
				out.write(buffer,0,len);
				System.out.println(buffer);
			}
			in.close();
			out.close();
		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {
		Copy cp = new Copy();
	}

}
