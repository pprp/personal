package work1;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class FocusTest {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		MyFocus mf = new MyFocus();
	}
}
//requestFocus 需要放在setVisable之后
@SuppressWarnings("serial")
class MyFocus extends JFrame {
	JTextField text;
	JButton button;

	public MyFocus() {
		setLayout(new FlowLayout());
		text = new JTextField(20);
		text.setForeground(Color.gray);
		text.setFont(new Font("宋体", Font.PLAIN, 20));
		button = new JButton("确定");
		text.setEditable(true);
		add(text);
		add(button);
		button.requestFocus();
		
		text.addMouseListener(new MouseListener() {//注册
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				text.setText("");
			}
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
			}
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
			}
		});
		text.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				if(text.getText().isEmpty())
					button.requestFocus();
				String ss = text.getText();
				text.setText(ss);
			}
			@Override
			public void focusLost(FocusEvent e) {
					text.setText("密码/邮箱/手机号");
			}
		});
		text.setEditable(true);
		setTitle("FocusEvent测试");
		setSize(400, 300);
		setLocation(400, 200);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
}