package work2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;

@SuppressWarnings("serial")
public class ICalendar extends JFrame implements ActionListener {
	JPanel pCenter;
	JPanel pCenterContent;
	JPanel pSouth;
	JPanel pNorth;

	JButton nextMonth, previousMonth;
	Calendar now;
	JLabel[][] mp = new JLabel[6][7];
	JLabel message;

	JPanel weekPanel;
	JButton week[] = new JButton[7];

	public ICalendar() {
		now = Calendar.getInstance();
		init();
		setTitle("我的第一个日历");
		setBounds(100, 100, 488, 308);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		nextMonth.addActionListener(this);
		previousMonth.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == nextMonth) { // increase
			if (now.get(Calendar.MONTH) == 11)
				now.roll(Calendar.YEAR, true);
			now.roll(Calendar.MONTH, true);
		} else if (e.getSource() == previousMonth) {// decrease
			if (now.get(Calendar.MONTH) == 0)
				now.roll(Calendar.YEAR, false);
			now.roll(Calendar.MONTH, false);
		}
		pSouthPerform();
		// 对mp进行处理
		clearCalendarPane();
		setCalendarPane();
	}

	public void init() {
		// 初始化所有部件
		pCenter = new JPanel(new BorderLayout());
		weekPanel = new JPanel(new GridLayout(1, 7));
		pCenterContent = new JPanel(new GridLayout(6, 7));
		pCenterContent.setBackground(Color.pink);
		pCenterContent.setBorder(new BevelBorder(BevelBorder.LOWERED));
		pSouth = new JPanel();
		pSouth.setBackground(Color.yellow);
		pNorth = new JPanel();
		pNorth.setBackground(Color.pink);
		message = new JLabel();
		nextMonth = new JButton("下月");
		previousMonth = new JButton("上月");

		add();
		add(pCenter, BorderLayout.CENTER);
		add(pSouth, BorderLayout.SOUTH);
		add(pNorth, BorderLayout.NORTH);
	}

	public void add() {
		weekPerform();
		// 对pCenter进行处理
		pCenter.add(weekPanel, BorderLayout.NORTH);
		pCenter.add(pCenterContent, BorderLayout.CENTER);
		// 对pNorth进行处理
		pNorth.setLayout(new FlowLayout());
		pNorth.add(nextMonth);
		pNorth.add(previousMonth);
		// 对pSouth进行处理
		pSouth.setLayout(new FlowLayout());
		message.setFont(new Font("宋体", Font.BOLD, 20));
		String tmp = getMessage(now);
		message.setText(tmp);
		pSouth.add(message);
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				mp[i][j] = new JLabel("", JLabel.CENTER);
				pCenterContent.add(mp[i][j]);
			}
		}
		setCalendarPane();
	}

	public void weekPerform() {
		String[] str = { "日", "一", "二", "三", "四", "五", "六", "七" };
		// 对星期进行处理
		for (int i = 0; i < 7; i++) {
			week[i] = new JButton(str[i]);
			weekPanel.add(week[i]);
		}
	}

	public void pCenterPerform() {
		// 对pCenter进行处理
		pCenter.add(weekPanel, BorderLayout.NORTH);
		pCenter.add(pCenterContent, BorderLayout.CENTER);
	}

	public void pSouthPerform() {
		// 对pNorth进行处理
		pNorth.setLayout(new FlowLayout());
		pNorth.add(nextMonth);
		pNorth.add(previousMonth);
		// 对pSouth进行处理
		pSouth.setLayout(new FlowLayout());
		message.setFont(new Font("宋体", Font.BOLD, 20));
		String tmp = getMessage(now);
		message.setText(tmp);
		pSouth.add(message);
	}

	public void setCalendarPane() {
		int from = getTheFirstDay();
		int len = getDaysOfMonth();
		System.out.println(from + "++++" + len);
		// cnt days
		int cnt = 1;
		// the first line
		for (int i = 0; i < from; i++) {
			mp[0][i].setText("");
			mp[0][i].setFont(new Font("宋体", Font.BOLD, 15));
		}
		for (int i = from; i < 7; i++) {
			mp[0][i].setText(String.valueOf(cnt));
			mp[0][i].setFont(new Font("宋体", Font.BOLD, 15));
			cnt++;
		}
		for (int i = 1; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				mp[i][j].setText(String.valueOf(cnt));
				mp[i][j].setFont(new Font("宋体", Font.BOLD, 15));
				cnt++;
				if (cnt > len)
					break;
			}
			if (cnt > len)
				break;
		}
	}

	private int getTheFirstDay() {
		Calendar tmp = Calendar.getInstance();
		tmp.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
				now.get(Calendar.DATE));
		tmp.set(Calendar.DAY_OF_MONTH, 1);
		return tmp.get(Calendar.DAY_OF_WEEK) - 1;
	}

	private int getDaysOfMonth() {
		// 计算这个月有多少天
		Calendar tmp = Calendar.getInstance();
		tmp.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH),
				now.get(Calendar.DATE));
		tmp.set(Calendar.DATE, 1);
		tmp.roll(Calendar.DATE, false);
		return tmp.get(Calendar.DATE);
	}

	public String getMessage(Calendar cal) {
		String s = "日历:" + cal.get(Calendar.YEAR) + " 年 "
				+ ((cal.get(Calendar.MONTH)) + 1) + " 月 "
				+ cal.get(Calendar.DATE) + "日";
		return s;
	}

	public void clearCalendarPane() {
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				mp[i][j].setFont(new Font("宋体", Font.BOLD, 15));
				mp[i][j].setText("");
			}
		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		ICalendar myc = new ICalendar();
	}
}
