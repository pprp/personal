package work3;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class Calculater extends JFrame implements ActionListener {	
	private String[] str = { "1", "2", "3", "+", "4", "5", "6", "-", "7", "8",
			"9", "*", "c", "0", "=", "/" };
	JPanel cal = new JPanel(new GridLayout(4, 4));
	JButton[] btn = new JButton[16];
	JTextField textShow = new JTextField();

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch(Exception e){
			e.printStackTrace();
		}
		Calculater mc = new Calculater();
	}
	
	public Calculater() {
		init();
		setTitle("�ҵļ�����");
		setBounds(100, 100, 450, 300);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void init() {
		textShow.setColumns(100);
		textShow.setHorizontalAlignment(JTextField.RIGHT);
		for (int i = 0; i < 16; i++) {
			btn[i] = new JButton(str[i]);
			btn[i].setFont(new Font("����", Font.ITALIC, 20));
			btn[i].addActionListener(this);
			cal.add(btn[i]);
		}
		add(textShow, BorderLayout.NORTH);
		add(cal, BorderLayout.CENTER);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn[0]) {
			addAction(0);
		} else if (e.getSource() == btn[1]) {
			addAction(1);
		} else if (e.getSource() == btn[2]) {
			addAction(2);
		} else if (e.getSource() == btn[3]) {
			addAction(3);
		} else if (e.getSource() == btn[4]) {
			addAction(4);
		} else if (e.getSource() == btn[5]) {
			addAction(5);
		} else if (e.getSource() == btn[6]) {
			addAction(6);
		} else if (e.getSource() == btn[7]) {
			addAction(7);
		} else if (e.getSource() == btn[8]) {
			addAction(8);
		} else if (e.getSource() == btn[9]) {
			addAction(9);
		} else if (e.getSource() == btn[10]) {
			addAction(10);
		} else if (e.getSource() == btn[11]) {
			addAction(11);
		} else if (e.getSource() == btn[12]) {
			addAction(12);
		} else if (e.getSource() == btn[13]) {
			addAction(13);
		} else if (e.getSource() == btn[14]) {
			addAction(14);
		} else if (e.getSource() == btn[15]) {
			addAction(15);
		}
	}

	void addAction(int i) {
		String tmp = null;
		switch (i) {
		case 0:
		case 1:
		case 2:
			tmp = textShow.getText();
			tmp += (i+1);
			textShow.setText(tmp);
			break;
		case 3:
			tmp = textShow.getText();
			tmp += ("+");
			textShow.setText(tmp);
			break;
		case 4:
		case 5:
		case 6:
			tmp = textShow.getText();
			tmp += (i);
			textShow.setText(tmp);
			break;
		case 7:
			tmp = textShow.getText();
			tmp += ("-");
			textShow.setText(tmp);
			break;
		case 8:
		case 9:
		case 10:
			tmp = textShow.getText();
			tmp += (i-1);
			textShow.setText(tmp);
			break;
		case 11:
			tmp = textShow.getText();
			tmp += ("*");
			textShow.setText(tmp);
			break;
		case 12:
			textShow.setText("");
			break;
		case 13:
			tmp = textShow.getText();
			tmp += ("0");
			textShow.setText(tmp);
			break;
		case 14:
			String str3 = textShow.getText();
			if(str3.indexOf('+',0) != -1){
				int a = Integer.parseInt(str3.substring(0,str3.indexOf('+',0)));
				int b = Integer.parseInt(str3.substring(str3.indexOf('+',0)+1,str3.length()));
				System.out.println(a+b);
				textShow.setText(String.valueOf(a+b));
			}else if(str3.indexOf('-', 0)!=-1){
				int a = Integer.parseInt(str3.substring(0,str3.indexOf('-',0)));
				int b = Integer.parseInt(str3.substring(str3.indexOf('-',0)+1,str3.length()));
				System.out.println(a-b);
				textShow.setText(String.valueOf(a-b));
			}else if(str3.indexOf('*', 0)!=-1){
				int a = Integer.parseInt(str3.substring(0,str3.indexOf('*',0)));
				int b = Integer.parseInt(str3.substring(str3.indexOf('*',0)+1,str3.length()));
				System.out.println(a*b);
				textShow.setText(String.valueOf(a*b));
			}else if(str3.indexOf('/', 0)!=-1){
				int a = Integer.parseInt(str3.substring(0,str3.indexOf('/',0)));
				int b = Integer.parseInt(str3.substring(str3.indexOf('/',0)+1,str3.length()));
				System.out.println((double)a/b);
				textShow.setText(String.valueOf((double)a/b));
			}
			else {
				System.out.println("wrong");
			}
			
			System.out.println("index:" + str3.indexOf('+',0));
			System.out.println(str3.substring(0,str3.indexOf('+',0)));
			System.out.println(str3.substring(str3.indexOf('+',0)+1,str3.length()));
			int a = Integer.parseInt(str3.substring(0,str3.indexOf('+',0)));
			int b = Integer.parseInt(str3.substring(str3.indexOf('+',0)+1,str3.length()));
			System.out.println(a+b);
			textShow.setText(String.valueOf(a+b));
			break;
		case 15:
			tmp = textShow.getText();
			tmp += ("/");
			textShow.setText(tmp);
			break;
		default:
		}
	}	
}