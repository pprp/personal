package work5;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

@SuppressWarnings("serial")
public class GameFrame extends JFrame {
	private JButton exit;
	private JButton restart;
	private JPanel low;
	private JButton adminFail;
	ChessBoard cb;

	public GameFrame() {
		exit = new JButton("退出");
		restart = new JButton("开始");
		adminFail = new JButton("认输");
		
		cb = new ChessBoard();
		low = new JPanel(new FlowLayout());
		low.add(exit);
		low.add(restart);
		low.add(adminFail);

		setTitle("粉红五子棋");
		setBounds(50,50,600,600);
		add(cb, BorderLayout.CENTER);
		add(low, BorderLayout.SOUTH);
		setVisible(true);

		MyListener liser = new MyListener();
		exit.addActionListener(liser);
		restart.addActionListener(liser);
		adminFail.addActionListener(liser);
	}
	private class MyListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (e.getSource() == restart) {
				cb.restartGame();
			} else if (e.getSource() == exit) {
				System.exit(0);
			} else if (e.getSource() == adminFail){
				JOptionPane.showMessageDialog(GameFrame.this, "你认输了");
				cb.restartGame();
			}
		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		GameFrame win = new GameFrame();

	}
}
