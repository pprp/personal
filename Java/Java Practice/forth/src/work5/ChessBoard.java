package work5;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

@SuppressWarnings("serial")
public class ChessBoard extends JPanel implements MouseListener {
	public static int MARGIN = 30;
	public static int GRID_SPAN = 35;
	public static int ROWS = 15, COLS = 15;
	Point[] chessList = new Point[(ROWS + 1) * (COLS + 1)];
	boolean isColor = true;// black first is true
	boolean gameOver = false;// default is not over
	int chessCount;
	int ind_x, ind_y;

	public ChessBoard() {
		setBackground(Color.PINK);
		addMouseListener(this);
		addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {}
			@Override
			public void mouseMoved(MouseEvent e) {}
		});
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i <= ROWS; i++) {
			g.drawLine(MARGIN, MARGIN + i * GRID_SPAN, MARGIN + COLS
					* GRID_SPAN, MARGIN + i * GRID_SPAN);
		}
		for (int i = 0; i <= COLS; i++) {
			g.drawLine(MARGIN + i * GRID_SPAN, MARGIN, MARGIN + i * GRID_SPAN,
					MARGIN + ROWS * GRID_SPAN);
		}
		// draw point
		for (int i = 0; i < chessCount; i++) {
			int xx = chessList[i].getX() * GRID_SPAN + MARGIN;
			int yy = chessList[i].getY() * GRID_SPAN + MARGIN;
			g.setColor(chessList[i].getColor());
			g.fillOval(xx - Point.D / 2, yy - Point.D / 2, Point.D, Point.D);
			if (i == chessCount - 1) {
				g.setColor(Color.GREEN);
				g.drawRect(xx - Point.D / 2, yy - Point.D / 2, Point.D, Point.D);
			}
		}
	}
	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mousePressed(MouseEvent e) {
		if (gameOver)return;
		String colorName = isColor ? "执黑棋者" : "执白棋者";
		ind_x = (e.getX() - MARGIN + GRID_SPAN / 2) / GRID_SPAN;
		ind_y = (e.getY() - MARGIN + GRID_SPAN / 2) / GRID_SPAN;
		if (ind_x < 0 || ind_x > ROWS || ind_y < 0 || ind_y > COLS) {
			return;
		}
		if (findChess(ind_x, ind_y)) {
			return;
		}
		Point pt = new Point(ind_x, ind_y, isColor ? Color.black : Color.white);
		chessList[chessCount++] = pt;
		repaint();
		if (isWin()) {
			String out = String.format("%s 胜", colorName);
			JOptionPane.showMessageDialog(this, out);
			gameOver = true;
		} else if (chessCount == (COLS + 1) * (ROWS + 1)) {
			String out = "平局";
			JOptionPane.showMessageDialog(this, out);
			gameOver = true;
		}
		isColor = !isColor;
	}
	@Override
	public void mouseReleased(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}
	private boolean findChess(int x, int y) {
		for (Point pt : chessList) {
			if (pt != null && pt.getX() == x && pt.getY() == y)
				return true;
		}
		return false;
	}
	private boolean isWin() {
		int continuecnt = 1;
		for (int x = ind_x - 1; x >= 0; x--) {
			Color c = isColor ? Color.black : Color.white;
			if (getChess(x, ind_y, c) != null)
				continuecnt++;
			else
				break;
		}
		for (int x = ind_x + 1; x <= ROWS; x++) {
			Color c = isColor ? Color.black : Color.WHITE;
			if (getChess(x, ind_y, c) != null)
				continuecnt++;
			else
				break;
		}
		if (continuecnt >= 5) {// 判断记录数大于等于五，即表示此方获胜
			return true;
		} else
			continuecnt = 1;
		for (int y = ind_y - 1; y >= 0; y--) {// 纵向向上寻找
			Color c = isColor ? Color.black : Color.white;
			if (getChess(ind_x, y, c) != null) {
				continuecnt++;
			} else
				break;
		}
		for (int y = ind_y + 1; y <= ROWS; y++) {// 纵向向下寻找
			Color c = isColor ? Color.black : Color.white;
			if (getChess(ind_x, y, c) != null) {
				continuecnt++;
			} else
				break;
		}
		if (continuecnt >= 5) {// 判断记录数大于等于五，即表示此方获胜
			return true;
		} else
			continuecnt = 1;
		//
		for (int x = ind_x + 1, y = ind_y - 1; y >= 0 && x <= COLS; x++, y--) {// 右下寻找
			Color c = isColor ? Color.black : Color.white;
			if (getChess(x, y, c) != null) {
				continuecnt++;
			} else
				break;
		}
		for (int x = ind_x - 1, y = ind_y + 1; y <= ROWS && x >= 0; x--, y++) {// 左上寻找
			Color c = isColor ? Color.black : Color.white;
			if (getChess(x, y, c) != null) {
				continuecnt++;
			} else
				break;
		}
		if (continuecnt >= 5) {// 判断记录数大于等于五，即表示此方获胜
			return true;
		} else
			continuecnt = 1;
		//
		for (int x = ind_x - 1, y = ind_y - 1; y >= 0 && x >= 0; x--, y--) {// 左下寻找
			Color c = isColor ? Color.black : Color.white;
			if (getChess(x, y, c) != null) {
				continuecnt++;
			} else
				break;
		}
		for (int x = ind_x + 1, y = ind_y + 1; y <= ROWS && x <= COLS; x++, y++) {// 右上寻找
			Color c = isColor ? Color.black : Color.white;
			if (getChess(x, y, c) != null) {
				continuecnt++;
			} else
				break;
		}
		if (continuecnt >= 5) {// 判断记录数大于等于五，即表示此方获胜
			return true;
		} else
			continuecnt = 1;
		return false;

	}
	private Point getChess(int ind_x, int ind_y, Color cr) {
		for (Point c : chessList) {
			if (c != null && c.getX() == ind_x && c.getY() == ind_y
					&& c.getColor() == cr)
				return c;
		}
		return null;
	}
	public void restartGame() {
		for (int i = 0; i < chessList.length; i++)
			chessList[i] = null;
		isColor = true;
		gameOver = false;
		chessCount = 0;
		repaint();
	}
	// Dimension:矩形
	public Dimension getPreferredSize() {
		return new Dimension(MARGIN * 2 + GRID_SPAN * COLS, MARGIN * 2
				+ GRID_SPAN * ROWS);
	}
}
