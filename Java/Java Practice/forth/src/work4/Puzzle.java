package work4;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.*;

@SuppressWarnings("serial")
public class Puzzle extends JFrame implements MouseListener {
	// 菜单栏
	JMenuBar mb;
	JMenu mn;
	JMenu submn;
	JMenuItem mi1, mi2;
	JRadioButton rb1, rb2, rb3, rb4;
	ButtonGroup group;
	// 内容
	int rec;
	JPanel left, right;
	JLabel up, down;
	JButton[] ImageBtn;
	Image ii;

	public Puzzle() {
		setupMenu();
		setupPanel();
		setTitle("拼图");
		setBounds(100, 100, 700, 460);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@SuppressWarnings("unused")
	private void setupPanel() {
		left = new JPanel();
		getContentPane().add(left);
		left.setLayout(new GridLayout(3, 3));

		ImageIcon[][] tmpII = new ImageIcon[3][3];
		tmpII = splitIcon(3, 3);

		BufferedImage bImage = null;
		BufferedImage blank = null;

		ImageBtn = new JButton[3 * 3];
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				ImageBtn[i * 3 + j] = new JButton();
				left.add(ImageBtn[i * 3 + j]);
			}
		}
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				ImageBtn[i * 3 + j].setIcon(tmpII[i][j]);
			}
		}

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				ImageBtn[i * 3 + j].addMouseListener(this);
			}
		}
	}

	private void restart() {
		outOfOrder();
		Random random = new Random();
		rec = random.nextInt(9);
		System.out.println("rec:" + rec);
		ImageBtn[rec].setIcon(null);
	}

	// 图片的分割函数
	public ImageIcon[][] splitIcon(int rows, int cols) {
		int subWidth, subHeight;
		ImageIcon[][] icons = new ImageIcon[rows][cols];
		BufferedImage bi = null; // 原始图片变量
		BufferedImage image = null; // 切割以后的图片变量
		try {
			bi = ImageIO.read(new File(".\\pics\\girl.jpg"));
			subWidth = bi.getWidth() / cols;
			subHeight = bi.getHeight() / rows;
			System.out.println(subWidth + " " + subHeight + " " + cols + " "
					+ rows);
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					image = bi.getSubimage(j * subWidth, i * subHeight,
							subWidth, subHeight);
					icons[i][j] = new ImageIcon(image.getScaledInstance(
							subWidth / 2, subHeight / 2, Image.SCALE_DEFAULT));
				}
			}
		} catch (Exception e) {
			System.out.println("error");
			e.printStackTrace();
		}
		return icons;
	}

	private void setupMenu() {
		mb = new JMenuBar();
		mn = new JMenu("菜单");
		submn = new JMenu("难度");
		mi1 = new JMenuItem("开始");
		mi2 = new JMenuItem("选择图片");
		mi1.addMouseListener(this);
		mi2.addMouseListener(this);
		rb1 = new JRadioButton("简单(3*3)");
		rb2 = new JRadioButton("一般(4*4)");
		rb3 = new JRadioButton("困难(5*5)");
		rb4 = new JRadioButton("非常困难(6*6)");
		rb1.addMouseListener(this);
		rb2.addMouseListener(this);
		rb3.addMouseListener(this);
		rb4.addMouseListener(this);

		group = new ButtonGroup();
		group.add(rb1);
		group.add(rb2);
		group.add(rb3);
		group.add(rb4);

		// menu
		add(mn);
		submn.add(rb1);
		submn.add(rb2);
		submn.add(rb3);
		submn.add(rb4);
		mn.add(submn);
		mn.add(mi1);
		mn.add(mi2);
		mb.add(mn);
		setJMenuBar(mb);
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Puzzle pz = new Puzzle();
	}

	public void outOfOrder() {
		Random random = new Random();
		for (int i = 0; i < ImageBtn.length; i++) {
			int idx1 = random.nextInt(ImageBtn.length);
			int idx2 = random.nextInt(ImageBtn.length);
			JButton tmp = new JButton();
			tmp.setIcon(ImageBtn[idx1].getIcon());
			ImageBtn[idx1].setIcon(ImageBtn[idx2].getIcon());
			ImageBtn[idx2].setIcon(tmp.getIcon());
		}
	}

	private void exChange(JButton jb1, JButton jb2) {
		JButton tmp = new JButton();
		tmp.setIcon(jb1.getIcon());
		jb1.setIcon(jb2.getIcon());
		jb2.setIcon(tmp.getIcon());
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int i;
		for (i = 0; i < ImageBtn.length; i++) {
			if (e.getSource().equals(ImageBtn[i]))
				break;
		}
		System.out.println("this is :" + i);
		System.out.println("change" + i + "," + rec);
		if (i == ImageBtn.length)
			return;
		if (i == 0) {
			if (rec == 1 || rec == 3) {
				exChange(ImageBtn[i], ImageBtn[rec]);
				rec = i;
			}
		} else if (i == 1) {
			if (rec == 0 || rec == 2 || rec == 4) {
				exChange(ImageBtn[i], ImageBtn[rec]);
				rec = i;
			}
		} else if (i == 2) {
			if (rec == 1 || rec == 5) {
				exChange(ImageBtn[i], ImageBtn[rec]);
				rec = i;
			}
		} else if (i == 3) {
			if (rec == 0 || rec == 4 || rec == 6) {
				exChange(ImageBtn[i], ImageBtn[rec]);
				rec = i;
			}
		} else if (i == 4) {
			if (rec == 1 || rec == 3 || rec == 5 || rec == 7) {
				exChange(ImageBtn[i], ImageBtn[rec]);
				rec = i;
			}
		} else if (i == 5) {
			if (rec == 2 || rec == 4 || rec == 8) {
				exChange(ImageBtn[i], ImageBtn[rec]);
				rec = i;
			}
		} else if (i == 6) {
			if (rec == 7 || rec == 3) {
				exChange(ImageBtn[i], ImageBtn[rec]);
				rec = i;
			}
		} else if (i == 7) {
			if (rec == 4 || rec == 6 || rec == 8) {
				exChange(ImageBtn[i], ImageBtn[rec]);
				rec = i;
			}
		} else if (i == 8) {
			if (rec == 5 || rec == 7) {
				exChange(ImageBtn[i], ImageBtn[rec]);
				rec = i;
			}
		}
		System.out.println("now rec:" + rec);
	}
	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getSource() == mi2) {
			JOptionPane.showMessageDialog(Puzzle.this, "对不起，你只能选择这个萌萌哒妹子");
			System.out.println("2333");
			return;
		}
		if (e.getSource() == mi1) {
			this.restart();
			JOptionPane.showMessageDialog(Puzzle.this, "您已经开始游戏了!\n友情提示：有可能按键不太灵敏，请有耐心");
		}
		if (e.getSource() == rb1) {
			JOptionPane.showMessageDialog(Puzzle.this, "您已经选择了简单!");
		}
		if (e.getSource() == rb2) {
			JOptionPane.showMessageDialog(Puzzle.this, "简单你都不会还想玩一般？已为您选择了简单");
		}
		if (e.getSource() == rb3) {
			JOptionPane.showMessageDialog(Puzzle.this, "简单你都不会还想玩困难？已为您选择了简单");
		}
		if (e.getSource() == rb4) {
			JOptionPane
					.showMessageDialog(Puzzle.this, "简单你都不会还想玩非常困难？已为您选择了简单");
		}
	}
	@Override
	public void mouseReleased(MouseEvent e) {
	}
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	@Override
	public void mouseExited(MouseEvent e) {
	}

}
