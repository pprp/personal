package work3;

public class GoodShelf {
	int Shelf;
	int maxShelf = 100;
	Product[] pd = new Product[100];

	public int getNumber() {
		return Shelf;
	}

	public GoodShelf(int Shelf) {
		this.Shelf = Shelf;
	}

	public synchronized void place(int num) {
		while (Shelf + num > maxShelf) {
			try {
				System.out.println("提示：货架已满！");
				this.notifyAll();
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		pd[Shelf] = new Product(Shelf);
		if (Shelf > maxShelf)
			return;				
		System.out.println(Thread.currentThread().getName() + "上架了" + pd[Shelf] + "的商品 ");
		Shelf += num;
		System.out.println(this);
		this.notifyAll();
	}

	public synchronized void remove(int extract) {
		while (Shelf < extract) {
			try {
				System.out.println("提示：货架余额不足！");
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if(Shelf-extract < 0)
			return;
		Shelf -= extract;
		System.out.println(Thread.currentThread().getName() + "买下了" + pd[Shelf] + "个商品。");
		System.out.println(this);
		
		if(Shelf < maxShelf)
			this.notifyAll();
	}
	public String toString() {
		return "剩余数量为" + Shelf;
	}
}
