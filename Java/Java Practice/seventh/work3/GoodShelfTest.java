package work3;

public class GoodShelfTest {
	public static void main(String[] args) {
		GoodShelf gs = new GoodShelf(0);

		Producter pt1 = new Producter("腾讯: ");
		pt1.setGoodShelf(gs);

		Producter pt2 = new Producter("阿里巴巴： ");
		pt2.setGoodShelf(gs);

		Costumer ct1 = new Costumer("   顾客 王**: ");
		ct1.setGoodShelf(gs);

		Costumer ct2 = new Costumer("   顾客 李**: ");
		ct2.setGoodShelf(gs);

		Costumer ct3 = new Costumer("   顾客 梁*: ");
		ct3.setGoodShelf(gs);

		pt1.start();
		pt2.start();
		ct1.start();
		ct2.start();
		ct3.start();
	}
}
