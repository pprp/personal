package work2;

public class Part implements Runnable {
	static int nn = 800;

	@Override
	public void run() {
		int n = 0;
		while (nn > 0) {
			if (sell()) {
				n++;
			}
		}
		System.out.println(Thread.currentThread().getName() + "执行了" + n);
	}

	public synchronized boolean sell() {
		if (nn > 0) {
			System.out.println(Thread.currentThread().getName() + " 卖出第" + nn-- + "张票");
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

}
