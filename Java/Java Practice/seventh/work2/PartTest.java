package work2;

public class PartTest {
	public static void main(String[] args) {
		Part workspace = new Part();
		Thread ted1 = new Thread(workspace, "workshop1");
		Thread ted2 = new Thread(workspace, "workshop2");
		Thread ted3 = new Thread(workspace, "workshop3");
		Thread ted4 = new Thread(workspace, "workshop4");

		ted1.start();
		ted2.start();
		ted3.start();
		ted4.start();

	}
}
