package work3;

public class TestOfKeyWord {
	public static void main(String[] args) {
		KeyWord kw = new KeyWord();
		System.out.println("----test of String-----");
		System.out.println(kw.keywordSearch("able was I ere I sawelba", "a"));
		System.out.println(kw.keywordSearch("this is a test","is"));
		System.out.println(kw.keywordSearch("this is a test","tasta"));
		
		System.out.println("----test of StringBuffer----");
		StringBuffer t1 = new StringBuffer("able was I ere I sawelba");
		StringBuffer t2 = new StringBuffer("this is a test");
		System.out.println(kw.keywordSearch(t1, "a"));
		System.out.println(kw.keywordSearch(t2, "is"));
		System.out.println(kw.keywordSearch(t2,"tasta"));
	}
}
