package work3;

public class KeyWord {
	String input;
	String keyword;	
	//String: static String valueOf(int i)  
	public String keywordSearch(String parent,String child){
		this.input = parent;
		this.keyword = child;
		int cnt = 0;
		int index = 0;
		String ans = "";
		while((index = parent.indexOf(child,index)) != -1){			
			String tmp = String.valueOf(index);
			index += child.length();
			ans += tmp;
			ans += ",";
			cnt++;
		}
		System.out.print(cnt + ": ");			
		return ans;
	}
	//StringBuffer: int indexOf(String str, int fromIndex) 	 
	//StringBuffer: StringBuffer append(StringBuffer sb) 
	//StringBuffer: StringBuffer append(String str) 
	public StringBuffer keywordSearch(StringBuffer parent, String child){
		StringBuffer ans = new StringBuffer(1000);
		int cnt = 0;
		int index = 0;
		while((index = parent.indexOf(child,index)) != -1){
			String tmp = String.valueOf(index);
			index += child.length();
			ans.append(tmp);
			ans.append(",");
			cnt++;
		}
		System.out.print(cnt + ": ");
		return ans;
	}
}
