package work5;

public class User {
	String userName;
	String userCode;
	String email;
	String phoneNumber;

	User() {
	}

	User(String userName, String userCode, String email, String phoneNumber) {
		this.userName = userName;
		this.userCode = userCode;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public String getName() {
		return userName;
	}

	public String getCode() {
		return userCode;
	}

	public String getEmail() {
		return email;
	}

	public String getphoneNumber() {
		return phoneNumber;
	}
	/*
	 * user name [a-zA-Z_] mima "[a-zA-Z]\\p{Punct}{8,16}" youxiang
	 * "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,3}$"
	 * telephone "[0-9]{11}"
	 */
}
