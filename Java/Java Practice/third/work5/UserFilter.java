package work5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserFilter {
	String regex1 = "[a-zA-Z_]{6,}";
	String regex2 = "(?!^[0-9]+$)(?!^[A-z]+$)(?!^[^A-z0-9]+$)^.{6,16}";
	String regex3 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,3}$";
	String regex4 = "[0-9]{11}";

	public boolean check(User a) {
		if (!a.getName().matches(regex1)) {
			System.out.println("用户名有误，应由字母、数字或“_”组成，长度不少于6位");
			return false;
		}
		if (!a.getCode().matches(regex2)) {
			System.out.println("密码设置有误，必须包含字母、数字、特殊符号在内的8~16位字符");
			return false;
		}
		if (!a.getEmail().matches(regex3)) {
			System.out.println("请重新设置邮箱，不合法");
			return false;
		}
		if (!a.getphoneNumber().matches(regex4)) {
			System.out.println("输入的电话号码长度不合法");
			return false;
		}
		return true;
	}

	// Pattern类用于创建一个正则表达式,也可以说创建一个匹配模式,
	// 它的构造方法是私有的,不可以直接创建,
	// 但可以通过Pattern.complie(String regex)简单工厂方法创建一个正则表达式,
	public boolean check2(User a) {
		// public static Pattern compile(String regex)
		Pattern p1 = Pattern.compile(regex1);
		Pattern p2 = Pattern.compile(regex2);
		Pattern p3 = Pattern.compile(regex3);
		Pattern p4 = Pattern.compile(regex4);
		// 通过调用模式的 matcher 方法从模式创建匹配器。
		// 这里用到的是Matcher中的matches方法
		// matches ： public boolean matches()
		// 分开的话 Matcher m = p1.matcher(a.getName()); boolean b = m.matches();
		Matcher m1 = p1.matcher(a.getName());
		if (!m1.matches()) {
			System.out.println("用户名有误，应由字母、数字或“_”组成，长度不少于6位");
			return false;
		}
		Matcher m2 = p2.matcher(a.getCode());
		if (!m2.matches()) {
			System.out.println("密码设置有误，必须包含字母、数字、特殊符号在内的8~16位字符");
			return false;
		}
		 Matcher m3 = p3.matcher(a.getEmail());
		 if(!m3.matches()){
			System.out.println("请重新设置邮箱，不合法");
			return false;
		}
		 Matcher m4 = p4.matcher(a.getphoneNumber());
		 if(!m4.matches()){
			System.out.println("输入的电话号码长度不合法");
			return false;
		}
		return true;
	}

	public boolean check3(User a) {
		// boolean b2 = Pattern.matches(regex1,a.getName());
		if (!Pattern.matches(regex1, a.getName())) {
			System.out.println("用户名有误，应由字母、数字或“_”组成，长度不少于6位");
			return false;
		}
		if (!Pattern.matches(regex2, a.getCode())) {
			System.out.println("密码设置有误，必须包含字母、数字、特殊符号在内的8~16位字符");
			return false;
		}
		if (!Pattern.matches(regex3, a.getEmail())) {
			System.out.println("请重新设置邮箱，不合法");
			return false;
		}
		if (!Pattern.matches(regex4, a.getphoneNumber())) {
			System.out.println("输入的电话号码长度不合法");
			return false;
		}
		return true;
	}

}
