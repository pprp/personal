package work5;

import java.util.Scanner;

public class TestOfUser {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("请输入用户名（应由字母、数字或“_”组成，长度不少于6位）");
		String name = in.nextLine();
		System.out.println("请输入密码（必须包含字母、数字、特殊符号在内的8~16位字符）");
		String userCode = in.nextLine();
		System.out.println("请输入邮箱");
		String email = in.nextLine();
		System.out.println("请输入联系电话（长度为11位）");
		String phoneNumber = in.nextLine();
		User a = new User(name,userCode,email,phoneNumber);
		UserFilter b = new UserFilter();
		
		System.out.println("-----test1-----");
		if(b.check(a)){
			System.out.println("1:您的输入合法，欢迎注册");
		}
		System.out.println("-----test2-----");
		if(b.check2(a)){
			System.out.println("2:您的输入合法，欢迎注册");
		}
		System.out.println("-----test3-----");
		if(b.check3(a)){
			System.out.println("3:您的输入合法，欢迎注册");
		}
		in.close();

	}
}


//String regex2 = "(?!^[0-9]+$)(?!^[A-z]+$)(?!^[^A-z0-9]+$)^.{6,16}";
//Scanner in = new Scanner(System.in);
//String tmp = in.nextLine();
//if(tmp.matches(regex2)){
//	System.out.println("hhh");
//}else {
//	System.out.println("*_*");
//}
