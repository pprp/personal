package work1;

import java.util.Scanner;

class TestOfNullPointer {
	int a;
	int b;
	TestOfNullPointer() {
		a = 0;
		b = 0;
	}
}

public class TryCatchPractice {
	public static void main(String[] args) {
		// test one - ArithmeticException 类型被0除的异常
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		try {
			System.out.println((double) n / m);
		} catch (ArithmeticException e) {
			System.out.println("发生异常：" + e.getMessage());
		}
		// test two - NullPointerException 空指针异常
		TestOfNullPointer test = null;
		try {
			System.out.println(test.a + " " + test.b);
		} catch (NullPointerException e) {
			System.out.println("发生异常：" + e.toString());
		}
		// test three - ArrayIndexOutOfBoundsException数组下标越界异常
		int[] a = new int[10];
		try {
			for (int i = 0; i < 100; i++) {
				a[i] = i;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		in.close();
	}
}
