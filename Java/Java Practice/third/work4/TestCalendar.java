package work4;

import java.util.Calendar;
import java.util.Scanner;

class MyCalendar {
	// 创建日历对象
	Calendar now = Calendar.getInstance();
	static String[] week = { "日", "一", "二", "三", "四", "五", "六" };
	public void printNowDate() {
		// 得到当前时间的年月
		int dayOfYear = now.get(Calendar.YEAR);
		int dayOfMonth = now.get(Calendar.MONTH);
		dayOfMonth++;
		System.out.println("\t" + dayOfYear + "  年   " + dayOfMonth + "  月");
	}

	public int getDaysOfMonth() {
		// 计算这个月有多少天
		now.set(Calendar.DATE, 1);
		now.roll(Calendar.DATE, false);
		int numOfThisMonth = now.get(Calendar.DATE);
		return numOfThisMonth;
	}

	public void printAll() {
		printNowDate();
		int numOfThisMonth = getDaysOfMonth();
		for (int i = 0; i < 7; i++) {
			print();
			System.out.print(week[i]);
		}
		System.out.println();
		// 計算本月的第一天是星期幾？
		now.set(Calendar.DAY_OF_MONTH, 1);
		int record = now.get(Calendar.DAY_OF_WEEK);
		// 5 列数字
		int cntDate = 1;
		for (int i = 0; i < 6; i++) {
			if (i == 0) {
				for (int j = 0; j < record; j++) {
					print();
				}
				for (int j = record; j <= 7; j++) {
					System.out.print(cntDate);
					cntDate++;
					print();
					if (cntDate > numOfThisMonth)
						break;
				}
			} else {
				for (int j = 0; j < 7; j++) {
					print();
					System.out.print(cntDate);
					cntDate++;
					if (cntDate > numOfThisMonth)
						break;
				}
			}
			System.out.println();
		}
	}
	static void print() {
		System.out.print('\t');
	}
	public void setCalendar(String a){
		if(a.equals("p")){
			now.roll(Calendar.MONTH, true);
		}else if(a.equals("n")){
			now.roll(Calendar.MONTH,false);
		}else{
			System.out.println("您的输入不合法");
		}
	}
}

public class TestCalendar {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		MyCalendar mc = new MyCalendar();
		mc.printAll();
		String judge;
		while(true){
			System.out.println("previous(p) or next(n) month?");
			judge = in.nextLine();
			mc.setCalendar(judge);
			mc.printAll();
		}
	}
}
