package work2;
import java.util.Scanner;
public class StasticScore {
	static int score[] = new int[100];
	static int sum = 0;
	static int cnt = 0;
	public int getSum(){
		return sum;
	}
	static void input() throws ScoreException{
		Scanner in = new Scanner(System.in);
		System.out.println("请输入班级人数：");
		int num = in.nextInt();
		for(int i = 0 ; i < num; i++){
			System.out.print("请输入成绩：");
			score[cnt] = in.nextInt();
			if(score[cnt] <= 100 && score[cnt] >=0)
				sum += score[cnt];
			if(score[cnt] > 100 || score[cnt] < 0){
				try{
					throw new ScoreException(score[cnt]);
				}
				catch(ScoreException e){
					System.out.println(e.warnMess());
					num++;
					cnt--;
				}
			}
			cnt++;
		}
		in.close();
	}
}
