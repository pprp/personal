package work2;

public class ExceptionTest {
	public static void main(String[] args){
		try{
			StasticScore.input();
			System.out.println("----我是分割线-----");
			double ans = StasticScore.sum/(double)(StasticScore.cnt);
			String s = String.format("%.1f", ans);
			System.out.println("平均成绩："+s);
		}catch(ScoreException e){
			System.out.println("出现如下问题：");	
			System.out.println(e.warnMess());
		}
	}
}
