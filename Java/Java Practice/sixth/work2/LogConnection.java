package work2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LogConnection {
	Connection con = null;
	Statement sql = null;

	public LogConnection() {
		// 1 drive
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("驱动加载成功");
		} catch (ClassNotFoundException e) {
			System.out.println("驱动加载失败");
			e.printStackTrace();
		}
		// 2 connect

		String url = "jdbc:mysql://localhost/test?useSSL=true";
		String user = "root";
		String password = "root";
		try {
			con = DriverManager.getConnection(url, user, password);
			System.out.println("链接成功");
		} catch (SQLException e) {
			System.out.println("链接失败");
			e.printStackTrace();
		}
		// 3 statement object
		try {
			sql = con.createStatement();
			System.out.println("成功生成statement对象");
		} catch (SQLException e) {
			System.out.println("生成statement对象失败");
			e.printStackTrace();
		}

	}

	public boolean Judge(String name, String passwd) {
		ResultSet rs = null;
		String str = "select * from log where logname = '" + name + "'";
		try {
			rs = sql.executeQuery(str);
			if (!rs.next())
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		try {
			String cmpPwd = rs.getString(3);
			if (cmpPwd.equals(passwd))
				return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void close(){
		try {
			con.close();
			System.out.println("关闭成功");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("关闭失败");
		}
	}
}
