package work2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import work3.StudentFrame;

@SuppressWarnings("serial")
public class LoginFrame extends JFrame {

	private JPanel contentPane;
	private JTextField userFiled;
	private JPasswordField pwd;
	private LogConnection lc = null;

	public static void main(String[] args) {
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		LoginFrame frame = new LoginFrame();
		frame.setVisible(true);
	}

	public LoginFrame() {
		setTitle("学生信息管理系统");
		lc = new LogConnection();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("\u7528\u6237\u540D\uFF1A");
		lblNewLabel.setBounds(63, 68, 54, 15);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("\u5BC6  \u7801\uFF1A");
		lblNewLabel_1.setBounds(63, 128, 54, 15);
		contentPane.add(lblNewLabel_1);

		userFiled = new JTextField();
		userFiled.setBounds(127, 65, 224, 23);
		contentPane.add(userFiled);
//		userFiled.setColumns(10);

		pwd = new JPasswordField();
		pwd.setBounds(127, 124, 224, 23);
		contentPane.add(pwd);

		JButton loginBtn = new JButton("\u786E\u5B9A");
		loginBtn.setBounds(63, 204, 93, 23);
		contentPane.add(loginBtn);

		JButton cancelBtn = new JButton("\u53D6\u6D88");
		cancelBtn.setBounds(268, 204, 93, 23);
		contentPane.add(cancelBtn);

		loginBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String name = userFiled.getText().trim();
				String pwdText = String.valueOf(pwd.getPassword()).trim();
				if (lc.Judge(name, pwdText)) {
					JOptionPane.showMessageDialog(LoginFrame.this, "欢迎您，"
							+ name);
					@SuppressWarnings("unused")		
					StudentFrame tmp = new StudentFrame();
					tmp.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(LoginFrame.this, "用 户名或密码错误");
				}
			}

		});
		cancelBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.exit(0);
				lc.close();
			}

		});
		JOptionPane.showMessageDialog(LoginFrame.this, "欢迎登陆学生信息管理系统，请输入账号密码");
	}
}
