package work3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Student {
	Connection con = null;
	Statement sql = null;
	ResultSet rs = null;

	public Student() {
		// 1 drive
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("驱动加载成功");
		} catch (ClassNotFoundException e) {
			System.out.println("驱动加载失败");
			e.printStackTrace();
		}
		// 2 connect
		String url = "jdbc:mysql://localhost/test?useSSL=true";
		String user = "root";
		String password = "root";
		try {
			con = DriverManager.getConnection(url, user, password);
			System.out.println("链接成功");
		} catch (SQLException e) {
			System.out.println("链接失败");
			e.printStackTrace();
		}
		// 3 statement object
		try {
			sql = con.createStatement();
			System.out.println("成功生成statement对象");
		} catch (SQLException e) {
			System.out.println("生成statement对象失败");
			e.printStackTrace();
		}
	}
	
	public String selectNameAge(int sno){
		
		String sqls = "select sname,age from student where sno = '"+String.valueOf(sno)+"'";
		try {
			 rs = sql.executeQuery(sqls);
			if(!rs.next()){
				return ("不存在！");
			}
			System.out.println("查询成功");
			String stuname = rs.getString(1);
			int stuage = rs.getInt(2);
			return  ("学号为"+sno+"的学生的姓名为:"+stuname+" ,年龄为:" + stuage);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("查询失败");
			e.printStackTrace();
		}	
		return "不存在";
	}
	
	public void add(String name,int age){
		String str = "insert into Student(sname,age) values('"+name+"',"+age+")";
		try {
			sql.executeUpdate(str);
			System.out.println("插入成功");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("插入失败");
			e.printStackTrace();
		}
	}
	
	public boolean DeleteInf(int no){
		String str = "delete from Student where sno = "+no+"";
		try {
			sql.executeUpdate(str);
			System.out.println("删除成功");
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("删除失败");
			e.printStackTrace();
		}
		return false;
	}
}
