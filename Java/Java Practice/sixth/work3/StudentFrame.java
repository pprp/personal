package work3;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import work2.LoginFrame;

@SuppressWarnings("serial")
public class StudentFrame extends JFrame {

	private JPanel contentPane;
	private JTextField searchStuNo;
	private JTextField NameTextField;
	private JTextField AgeTextField;
	LoginFrame lf = null;
	Student stu = null;

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch(Exception e){
			e.printStackTrace();
		}
		StudentFrame frame = new StudentFrame();	
		frame.setVisible(true);
	}

	public StudentFrame() {
		stu = new Student();
		
		setTitle("学生信息查询系统");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(Color.PINK);

		JLabel label = new JLabel("\u67E5\u8BE2\u7684\u5B66\u53F7\uFF1A");
		label.setBounds(25, 29, 100, 27);
		contentPane.add(label);

		searchStuNo = new JTextField();
		searchStuNo.setBounds(135, 32, 218, 24);
		contentPane.add(searchStuNo);
		searchStuNo.setColumns(10);

		JButton SearchBtn = new JButton("\u67E5\u8BE2");
		SearchBtn.setBounds(94, 66, 77, 23);
		contentPane.add(SearchBtn);

		JButton DeleteBtn = new JButton("\u5220\u9664");
		DeleteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		DeleteBtn.setBounds(276, 66, 77, 23);
		contentPane.add(DeleteBtn);

		JLabel label_1 = new JLabel(
				"\u52A0\u5165\u5B66\u751F\u4FE1\u606F\uFF1A");
		label_1.setBounds(25, 138, 100, 37);
		contentPane.add(label_1);

		NameTextField = new JTextField();
		NameTextField.setBounds(177, 146, 58, 21);
		contentPane.add(NameTextField);
		NameTextField.setColumns(10);

		AgeTextField = new JTextField();
		AgeTextField.setBounds(286, 146, 58, 21);
		contentPane.add(AgeTextField);
		AgeTextField.setColumns(10);

		JLabel label_2 = new JLabel("\u59D3\u540D:");
		label_2.setBounds(135, 149, 36, 15);
		contentPane.add(label_2);

		JLabel label_3 = new JLabel("\u5E74\u9F84:");
		label_3.setBounds(251, 149, 54, 15);
		contentPane.add(label_3);

		JButton SureBtn = new JButton("\u786E\u5B9A");
		SureBtn.setBounds(94, 203, 77, 23);
		contentPane.add(SureBtn);

		JButton ExitBtn = new JButton("\u9000\u51FA");
		ExitBtn.setBounds(276, 203, 77, 23);
		contentPane.add(ExitBtn);

		SearchBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int ssno = Integer.parseInt(searchStuNo.getText());
				String tmp = stu.selectNameAge(ssno);
				JOptionPane.showMessageDialog(StudentFrame.this, tmp);
				searchStuNo.setText("");
			}
		});
		DeleteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int ssno = Integer.parseInt(searchStuNo.getText());
				if(stu.DeleteInf(ssno)){
					JOptionPane.showMessageDialog(StudentFrame.this, "删除成功！");
				}else {
					JOptionPane.showMessageDialog(StudentFrame.this, "删除失败！");
				}
				searchStuNo.setText("");
			}
		});

		ExitBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		SureBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String ssname = NameTextField.getText();
				int sage = Integer.parseInt(AgeTextField.getText());
				stu.add(ssname,sage);
				NameTextField.setText("");
				AgeTextField.setText("");
				JOptionPane.showMessageDialog(StudentFrame.this, "插入成功！");
			}
		});
	}
}
