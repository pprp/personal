package work1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class EmloyeeConnection {
	
	public static void main(String[] args) {
		Connection con = null;
		Statement sql = null;
		
		// 1 drive
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("驱动加载成功");
		} catch (ClassNotFoundException e) {
			System.out.println("驱动加载失败");
			e.printStackTrace();
		}
		// 2 connect
		String url = "jdbc:mysql://localhost/test?useSSL=true";
		String user = "root";
		String password = "root";
		try {
			con = DriverManager.getConnection(url, user, password);
			System.out.println("链接成功");
		} catch (SQLException e) {
		System.out.println("链接失败");
			e.printStackTrace();
		}
		// 3 statement object
		try {
			sql = con.createStatement();
			System.out.println("成功生成statement对象");
		} catch (SQLException e) {
			System.out.println("生成statement对象失败");
			e.printStackTrace();
		}
		
		// 4 execute sql statement
		String sqlUpdate = "update employee set salary = salary + 1500";
		try {
			sql.executeUpdate(sqlUpdate);
			System.out.println("执行更新操作成功");
		} catch (SQLException e) {
			System.out.println("执行更新操作失败");
			e.printStackTrace();
		}
		
		// 5 close
		try {
			con.close();
			System.out.println("关闭成功");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("关闭失败");
		}
	}

}
