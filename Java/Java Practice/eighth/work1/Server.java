package work1;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	public static void main(String[] args) {
		ServerSocket sSocket = null;
		Socket socket = null;
		try {
			sSocket = new ServerSocket(2018);
		} catch (IOException e1) {
			System.out.println("端口已被占用！");
		}
		try {
			socket = sSocket.accept();
			System.out.println(socket.getInetAddress());
			System.out.println(socket.getPort());
			System.out.println(socket.getLocalSocketAddress());
			System.out.println(socket.getLocalPort());
		} catch (IOException e) {
			System.out.println("链接客户端异常。");
		}
		
	}

}
