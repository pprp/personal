package work1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Client {
	public static void main(String[] args) throws IOException {
		Socket socket = new Socket();									//c1
		InetSocketAddress isa = new InetSocketAddress("127.0.0.1",2018);
		try {
			socket .connect(isa);										//c2
			System.out.println(socket.getInetAddress());				//c3
			System.out.println(socket.getPort());						//c4
			System.out.println(socket.getLocalSocketAddress());			//c5
			System.out.println(socket.getLocalPort());					//c6
		} catch (IOException e) {
			System.out.println("���ӷ������쳣��");
		}		
		socket.close();
	}

}
