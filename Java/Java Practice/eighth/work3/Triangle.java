package work3;

import java.io.Serializable;
/*
 * 序列化是为了便于在网络的传输和保存
 */
public class Triangle implements Serializable{
	double sideA;
	double sideB;
	double sideC;
	double area;
	public Triangle(){
		sideA = sideB = sideC = 0;
	}
	
	public Triangle(double a, double b, double c){
		this.sideA = a;
		this.sideB = b;
		this.sideC = c;
	}
	public boolean isLegal(){
		if((sideB + sideA > sideC)&&(sideB - sideA < sideC))
			return true;
		if((sideC + sideA > sideB)&&(sideC - sideA < sideB))
			return true;
		if((sideB + sideC > sideA)&&(sideB - sideC < sideA))
			return true;
		return false;
	}
	public double calculateArea(){
		double p = (sideA+sideB+sideC)/2.0;
		area = Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC));
		return area;
	}	
}
