package work3;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class TCPClient {
	Socket ss = null;
	DataInputStream dis = null;
	DataOutputStream dos = null;

	public TCPClient() {
		ss = new Socket();
	}

	public boolean link() {
		InetSocketAddress isa = new InetSocketAddress("127.0.0.1", 6660);
		try {
			ss.connect(isa);
			dis = new DataInputStream(ss.getInputStream());
			dos = new DataOutputStream(ss.getOutputStream());
			System.out.println("连接成功！");
			return true;
		} catch (IOException e) {
			System.out.println("连接失败！");
			return false;
		}
	}

	public double interact(String a1, String b1, String c1) {
		double area = 0;
		try {

			double a = Double.valueOf(a1);
			dos.writeDouble(a);
			double b = Double.valueOf(b1);
			dos.writeDouble(b);
			double c = Double.valueOf(c1);
			dos.writeDouble(c);

			area = dis.readDouble();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return area;
	}
}

