package work3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;

public class TriangleFrame extends JFrame {
	TCPClient tc = null;
	
	JPanel north;
	JPanel center;
	JPanel south;
	
	JButton link;
	JButton send;
	
	JTextField textA;
	JTextField textB;
	JTextField textC;
	
	JTextArea area;
	
	public TriangleFrame() {
		tc = new TCPClient();
		
		link = new JButton("连接到服务器");
		send = new JButton("SEND");
		
		textA = new JTextField();
		textB = new JTextField();
		textC = new JTextField();
		
		area = new JTextArea();
		
		north = new JPanel();
		center = new JPanel();
		south = new JPanel();
		
		INIT();
		
		north.setLayout(new BorderLayout());
		north.add(link,BorderLayout.CENTER);
		
		center.setLayout(new FlowLayout());
		center.add(new JLabel("SideA:"));
		center.add(textA);
		center.add(new JLabel("SideB:"));
		center.add(textB);
		center.add(new JLabel("SideC"));
		center.add(textC);
		center.add(send);
		
		south.setLayout(new BorderLayout());
		south.add(area);
		area.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		this.setTitle("计算三角形面积");
		this.setVisible(true);
		this.setSize(new Dimension(600,400));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.add(north,BorderLayout.NORTH);
		this.add(center, BorderLayout.CENTER);
		this.add(south, BorderLayout.SOUTH);
	}
	
	void INIT(){
		
		area.setRows(16);
		textA.setPreferredSize(new Dimension(100,30));
		textB.setPreferredSize(new Dimension(100,30));
		textC.setPreferredSize(new Dimension(100,30));
		
		link.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tc.link()) {
					area.setText("服务器连接成功！,IP地址为：" + tc.ss.getInetAddress() + "\n");
					area.append("端口地址为：" + tc.ss.getLocalPort() + "\n");
				} else {
					area.setText("服务器连接失败!\n");
				}
			}
			
		});
		send.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				String a = null, b = null, c = null;
				a = textA.getText();
				b = textB.getText();
				c = textC.getText();

				Double Sarea = tc.interact(a, b, c);
				if (Sarea == -1)
					area.append("输入的三个边长不能组成三角形！");
				else
					area.append("三角形的面积为：" + Sarea + "\n");
			}			
		});
	}
	public static void main(String[] args) {
		new TriangleFrame();
	}
}

