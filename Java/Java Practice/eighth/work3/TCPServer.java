package work3;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer {
	ServerSocket ss = null;
	Socket socket = null;
	DataInputStream dis = null;
	DataOutputStream dos = null;
	
	public TCPServer() {
		try {
			ss = new ServerSocket(6660);
			socket = ss.accept();
			dis = new DataInputStream(socket.getInputStream());
			dos = new DataOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			System.out.println("初始化失败：端口被占用");
		}
	}

	public void interact() {
		Triangle t = null;
		while(true) {
			try {
				double a = dis.readDouble();
				double b = dis.readDouble();
				double c = dis.readDouble();
				t = new Triangle(a, b, c);
				
				double area = -1;
				if(t.isLegal() == false)
				dos.writeDouble(area);
				else{
					area = t.calculateArea();
					dos.writeDouble(area);
				}
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public static void main(String[] args) throws IOException {
		new TCPServer().interact();
	}
}
