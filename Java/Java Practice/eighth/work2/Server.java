package work2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	ServerSocket ss = null;
	Socket socket = null;

	FileInputStream fis = null;
	DataOutputStream dos = null;
	DataInputStream dis = null;

	public Server() {
		try {
			ss = new ServerSocket(5036);
			socket = ss.accept();	
			dos = new DataOutputStream(socket.getOutputStream());
			dis = new DataInputStream(socket.getInputStream());
		} catch (IOException e) {
			System.out.println("服务器: 链接失败");
			e.printStackTrace();
		}		
	}
	public void interact() {
		while(true){
			try {			
				byte[] b = new byte[1024];
				int lens = dis.read(b);
				
				String fileName = new String(b,0,lens);
				System.out.println("fileName:" + fileName);

				fis = new FileInputStream(".\\source\\" + fileName);			
				byte[] buffer = new byte[1024*600];
				int len;
				System.out.println("服务器： 传送内容");	
				while ((len = fis.read(buffer)) != -1) {
					dos.write(buffer,0,len);								
				}
				dos.write("END".getBytes());
				System.out.println("文件发送完毕！");
			} catch (IOException e) {
				System.out.println("服务器： 读入失败");
				e.printStackTrace();
			}
			try {
				fis.close();
			} catch (IOException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}
	}
	public static void main(String[] args) {
			new Server().interact();
	}
}
