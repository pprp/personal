package work2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	Socket ss = null;
	Scanner in = new Scanner(System.in);

	FileOutputStream fos = null;
	String fileName;
	DataOutputStream dos = null;
	DataInputStream dis = null;

	public Client() {
		ss = new Socket();
		InetSocketAddress isa = new InetSocketAddress("127.0.0.1", 5036);
		try {
			ss.connect(isa);
			dos = new DataOutputStream(ss.getOutputStream());
			dis = new DataInputStream(ss.getInputStream());
		} catch (IOException e) {
			System.out.println("客户端  链接失败");
		}				
	}

	public void interact() throws IOException {
		while(true){
			System.out.println("请输入一个文件名：");
			fileName = in.nextLine();
			byte[] b = fileName.getBytes();
			
			dos.write(b);
			
			int length = 0;
			byte[] bb = new byte[1024*600];
			
			fos = new FileOutputStream(".\\destination\\" + fileName);
			try {
				while((length = dis.read(bb)) != -1){
					if(new String(bb,length-3,3).equals("END")){
						break;
					}
					fos.write(bb,0,length);		
				}
				System.out.println("文件接收完毕！");
			} catch (IOException e) {
				System.out.println("客户端 接受服务器发送的数据失败！");
			}
			fos.close();
		}
	}
	public static void main(String[] args) throws IOException {
		Client t = new Client();
		t.interact();
	}
}
