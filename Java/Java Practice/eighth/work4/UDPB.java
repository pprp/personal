package work4;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Date;

public class UDPB {
	DatagramSocket getS;
	DatagramPacket getP;
	byte [] data = new byte[1024*60];
	byte [] data2 = new byte[1024*60];
	DatagramSocket sendS;
	DatagramPacket sendP;
	InetSocketAddress isa = new InetSocketAddress("127.0.0.1",8989);
	
	public UDPB(){
		try {
			getS = new DatagramSocket(9898);
			getP = new DatagramPacket(data,data.length,isa);
			
			sendS = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	public void interact(){
		try {
			getS.receive(getP);
			System.out.println("接受成功！");
			String str = new String(data,0,getP.getLength());
			System.out.println(str);
			if(str.equals("TIME")){
				String d = new Date().toString();	
				System.out.println(d);
				data2 = d.getBytes();
				sendP = new DatagramPacket(data2,data2.length,isa);	
				
				sendS.send(sendP);
				System.out.println("发送成功！");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		getS.close();
		sendS.close();
	}
	public static void main(String[] args) {
		new UDPB().interact();
	}
}
