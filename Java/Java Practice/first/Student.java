package student;

import java.lang.String;
import java.util.Scanner;

class SchoolStudent{
	String name;
	int grade;	
	void set(String name, int grade){
		this.name = name;
		this.grade = grade;
	}
	SchoolStudent get(SchoolStudent st){
		st.name = this.name;
		st.grade = this.grade;
		return st;
	}
	SchoolStudent(String name, int grade){
		this.name = name;
		this.grade = grade;
	}
	SchoolStudent(){
		this.name = "";
		this.grade = 0;
	}
}
	public class Student{
		public static void main(String args[]){
			while(true){
				Scanner reader = new Scanner(System.in);
				System.out.println("姓名：");
				String name = reader.nextLine();
				System.out.println("成绩：");
				int grade = reader.nextInt();
				SchoolStudent tom = new SchoolStudent(name,grade);
				System.out.println("学生姓名：" + tom.name);
				System.out.println("成绩：" + tom.grade);	
			}		
		}	
}

