import java.util.Scanner;

public class wuziqi
{
    static int maxn = 16;
    private static char[][] map = new char[maxn][maxn];
    private static char[] baihei= {'○','●'};
    private static int[] tox = {-1,0,-1,-1,1,1,0,1};
    private static int[] toy = {0,-1,-1,1,-1,1,1,0};

    public static void init()
    {
        for(int i=1; i<maxn; i++)
            for(int j=1; j<maxn; j++)
                map[i][j] = '＋';
    }
    public static void paint()
    {
        for(int i=1; i<maxn; i++)
        {
            for(int j=1; j<maxn; j++)
            {
                System.out.print(map[i][j]);
            }
            System.out.println();
        }
    }
    public static boolean isLegal(int i, int j)
    {
        if(i>15 || i<1 || j>15 || j<1)
        {
            return false;
        }
        return true;
    }

    private static int isWin(int p,int x,int y)
    {
        for(int i=0; i<4; i++)
        {
            int sum = 1;
            int xx = x+tox[i];
            int yy = y+toy[i];
            while(map[xx][yy]==baihei[p%2] && isLegal(xx,yy))
            {
                sum++;
                xx = xx+tox[i];
                yy = yy+toy[i];
            }
            xx = x+tox[7-i];
            yy = y+toy[7-i];
            while(map[xx][yy]==baihei[p%2] && isLegal(xx,yy))
            {
                sum++;
                xx = xx+tox[7-i];
                yy = yy+toy[7-i];
            }
            if(sum>=5)
                return 1;
        }
        return 0;
    }
    public static void main(String[] args)
    {
        init();
        int cnt = -1; //白先
        while(true)
        {
            System.out.println("请输入落棋的坐标，以X Y的形式:");
            Scanner reader = new Scanner(System.in);
            int x1 = reader.nextInt();
            int y1 = reader.nextInt();
            while(!isLegal(x1,y1) || map[x1][y1] != '＋')
            {
                System.out.println("请您重新输入落棋的坐标:");
                reader = new Scanner(System.in);
                x1 = reader.nextInt();
                y1 = reader.nextInt();
            }
            cnt++;
            map[x1][y1] = baihei[cnt%2];
            paint();
            if(isWin(cnt,x1,y1)==1)
            {
                System.out.println("恭喜您赢了");
                break;
            }
            if(cnt == 15*15-1)
            {
                System.out.println("平局");
                break;
            }
        }
    }
}
