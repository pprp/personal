import java.lang.Math;
import java.util.Scanner;
public class Solve{
	public static void main(String args[]){
		Scanner reader = new Scanner(System.in);
		int a = reader.nextInt();
		int b = reader.nextInt();
		int c = reader.nextInt();
		double ans1 = 0.0, ans2 = 0.0;
		double det = b * b - 4 * a * c;
		if(a == 0 && b != 0){
			System.out.println("该方程只有一个根：" + -1*c/b);
			return ;
		}
		if(a == 0 && b == 0){
			System.out.println("没有解");
			return ;
		}		
		if(det < 0){
			System.out.println("没有解");
			return ;
		}
		else if(det == 0){
			ans1 = -1*b/(2*a);
			System.out.println("该方程只有一个根：" + ans1);
		}else{
			det = Math.sqrt(det);
			ans1 = -1*b + det;
			ans1 /= 2.0*a;
			ans2 = -1*b - det;
			ans2 /= 2.0*a;
			System.out.println("该方程有且仅有两个：");
			System.out.println("第一个根为：" + ans1);
			System.out.println("第二个根为：" + ans2);
		}
	}
	
}