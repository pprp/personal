import java.util.Arrays;

public class Test{
	public static void main(String[] args){
		Country American = new Country(46,37,38);
		Country English = new Country(27,23,17);
		Country China = new Country(26,18,26);
		Country Russia = new Country(19,18,19);
		Country Germany = new Country(17,10,15);		
			
	
		Country[] countrys = new Country[5];
		countrys[0] = American;
		countrys[1] = English;
		countrys[2] = China;
		countrys[3] = Russia;
		countrys[4] = Germany;
		Arrays.sort(countrys);
		for(Country cty:countrys)
			System.out.println(cty.gold + " " + cty.silver + " " + cty.copper + " " + cty.sum);
		
	}
}