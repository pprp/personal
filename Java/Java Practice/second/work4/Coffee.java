package coffee;

//增加的方法
abstract class AddImplement{
	private String name;
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	public abstract void add();	
}
class AddSugarImplement extends AddImplement{
	public void add(){
		System.out.println("you have added the Sugar^_^");
	}
}
class AddMilkImplement extends AddImplement{
	public void add(){
		System.out.println("you have added the Milk ^_^");
	}
}
//抽象类的实现
public abstract class Coffee{
	private AddImplement addimpl;
	//然后在调用add进行输出
	public void add(){
		addimpl.add();
	};
	//应该先调用这个设置函数
	public void setAddImpl(AddImplement addimpl){
		this.addimpl = addimpl;
	};
}

class InstantCoffee extends Coffee{
	public void setAddImpl(AddImplement addimpl){
		System.out.println("This is an InstantCoffee^_^");
		super.setAddImpl(addimpl);
	}
}

class LatteCoffee extends Coffee{
	public void 
	setAddImpl(AddImplement addimpl){
		System.out.println("This is a LatteCoffee^_^");
		super.setAddImpl(addimpl);
	}
}

class MochaCoffee extends Coffee{
	public void setAddImpl(AddImplement addimpl){
		System.out.println("This is a MochaCoffee^_^");
		super.setAddImpl(addimpl);
	}	
}





