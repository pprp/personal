package test;

//系统成分类
abstract class attribute{
	int cnt;
//	public abstract void operation1();
	public abstract void output();
	public abstract void copy();
	public abstract void traverseDirectory();
}
//目录类
class Folder extends attribute{
	private attribute [] parts = new attribute[100];
	private String name;
	public int cnt = 0;
	private int index = 0;
	//构造函数部分
	Folder(){}
	Folder(String name){
		this.name = name;
	}
	//复制操作
//	public void operation1(){
//		System.out.println("复制文件：" + name);
//	}
	public void output(){
		System.out.println("+" + name);
	}
	public void add(Folder name){
		parts[index] = name;
		index++;
		name.cnt = this.cnt + 1;		
	}
	public void add(File name){
		parts[index] = name;
		index++;
		name.cnt = this.cnt + 1;
	}
	public boolean remove(attribute a){
		for(int i = 0 ; i < index ;i++){
			if(parts[i].equals(a)){
				parts[i] = null;
				return true;
			}
		}
		return false;
	}
	//有几个操作
	public int getchild(){
		return index;
	}
	//遍历操作
	public void traverseDirectory(){
		this.output();
		for(int i = 0 ; i < index; i++){	
			for(int k = 0 ; k <= this.cnt; k++){
				System.out.print("    ");
			}
			this.parts[i].traverseDirectory();
		}
	}
	public void copy(){
		System.out.println("复制文件：" + name);
		for(int j = 0 ; j < this.index ;j++){
			this.parts[j].copy();
		}
	}
}
//文件类
class File extends attribute{
	public String name;
	public int cnt = 0;
	File(String name){
		this.name = new String(name);
	}
	public void output(){
		System.out.println("-" + name);
	}
	public void copy(){
		System.out.println("复制文件:" + name);
	}
//	public void operation1(){
//		System.out.println("复制文件:" + name);
//	}
	public void traverseDirectory(){
		this.output();
	}
}

public class Test{
	public static StringBuffer st = new StringBuffer();
	public static void main(String[] args){
		Folder document = new Folder("我的资料");
		File book = new File("Java编程思想.pdf");
		Folder music = new Folder("我的音乐");
		File music1 = new File("你是我的眼.mp3");
		File music2 = new File("Without You.mp3");
		Folder picture = new Folder("我的照片");
		File pic1 = new File("我在美国白宫的照片");
		File pic2 = new File("我在新加坡的照片");
		File pic3 = new File("我在南极的照片");
		File pic4 = new File("我在南非的照片");
		File pic5 = new File("我与习大大的合影");
		
		document.add(book);
		document.add(music);
		music.add(music1);
		music.add(music2);
		picture.add(pic1);
		picture.add(pic2);
		picture.add(pic3);
		picture.add(pic4);
		picture.add(pic5);
		
		
		document.copy();
		System.out.println("-------------------------------");
		document.traverseDirectory();
		picture.traverseDirectory();
	}
}
