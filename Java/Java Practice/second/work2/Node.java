public class Node{
	private int data;
	private Node next;	
	//construction
	public Node(){
		this.data = 0;
		this.next = null;
	}
	public Node(int data){
		this.data = data;
		this.next = null;
	}
	//get 
	public Node getNext(){
		return this.next;
	}
	public int getData(){
		return data;
	}
	
	//set 
	public void setNext(Node next){
		this.next = next;
	}
	public void setData(int data){
		this.data = data;
	}
	//show 
	public void nodeDisplay(){
		System.out.println("{"+data+"}");
	}	
}
