public class Link{
	private Node first;
	public Link(){
		this.first = null;
	}
	//判断是否为空
	public boolean isEmpty(){
		if(this.first == null)
			return true;
		return false;
	}
	//插入头结点
	public void insertHeadNode(int data){
		Node tmp = new Node(data);
		if(this.first == null)
			first = tmp;
		else
		{
			tmp.setNext(first.getNext());
			first.setNext(tmp);			
		}
		return ;
	}
	//在第index下插入data节点
	public void insertNode(int data, int index){
		Node p = this.first;
		int cnt = 0;
		while(cnt != index){
			p = p.getNext();			
			cnt++;
		}
		Node newNode = new Node(data);
		newNode.setNext(p.getNext());
		p.setNext(newNode);
		return ;
	
	}
	//delete the head node
	public Node deleteHeadNode(){
		Node tmp = first;
		this.first = tmp.getNext();
		return tmp;
	}
	//find the data in the link
	public Node findNode(int data){
		Node p = this.first;
		while(p != null){			
			if(p.getData() == data)
				return p;
			if(p == null)
				break;
			p = p.getNext();
		}	
		return null;
	}
	//display the link
	public void displayLink(){
		Node p = first;
		while(p != null){
			System.out.println(p.getData());
			p = p.getNext();
		}
		return ;
	}	
}

