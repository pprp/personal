
public class TestOfNode{
	public static void main(String[] args){
		//test of Node
		Node tmp1 = new Node(100);
		tmp1.nodeDisplay();
		
		Link tmp2 = new Link();
		//test of insertHeadNode & displayLink
		tmp2.insertHeadNode(00);
		tmp2.displayLink();
		//test of isEmpty
		System.out.println(tmp2.isEmpty());
		System.out.println("+++++\n\n");
		//test of insertNode
		tmp2.insertNode(11,0);
		tmp2.insertNode(22,1);
		tmp2.insertNode(33,2);
		tmp2.insertNode(44,3);
		tmp2.insertNode(55,4);
		tmp2.insertNode(66,5);
		tmp2.displayLink();
		//test of delete the head Node
		System.out.println("++++");
		tmp2.deleteHeadNode();
		tmp2.displayLink();
		//test of find the data
		if(tmp2.findNode(3310)!=null){
			System.out.println("truely find the data:" + 3310);
		}else{
			System.out.println("fasle");
		}
		
		System.out.println("++++++");
		
		//test of InsertHeadNode
		tmp2.insertHeadNode(1111);
		tmp2.insertHeadNode(2222);
		tmp2.insertHeadNode(3333);
		tmp2.displayLink();
	}
}