package comparable;  
import java.util.Arrays;
public class Student implements Comparable{
	int stuNumber;
	String name;
	
	public Student(int number,String name) {
		this.stuNumber = number;
		this.name = name;
	}
	
	public int compareTo(Object o) {
		Student stu = (Student)o ; // ǿ��ת��
		return  this.stuNumber - stu.stuNumber;
	}
}

class StuTest{
	public static void main(String [] args){
		Student s1 = new Student(2001,"zhangsan");
		Student s2 = new Student(1992,"lisi");
		Student s3 = new Student(1995,"wangwu");
		
		Student[] students = new Student[3];
		students[0] = s1;
		students[1] = s2;
		students[2] = s3;
		
		Arrays.sort(students);
		System.out.println(students[0].stuNumber);
		System.out.println(students[1].stuNumber);
		System.out.println(students[2].stuNumber);
	}
}
