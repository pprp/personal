package work3;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	Socket ss = null;
	DataInputStream dis = null;
	DataOutputStream dos = null;
	Scanner in = new Scanner(System.in);

	public Client() {
		try {
			ss = new Socket("127.0.0.1", 6660);
			dis = new DataInputStream(ss.getInputStream());
			dos = new DataOutputStream(ss.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void interact() {
		
		while (true) {
			try {
				double r = in.nextDouble();
				dos.writeDouble(r);
				double area = dis.readDouble();
				System.out.println("�����" + area);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		new Client().interact();
	}

}
