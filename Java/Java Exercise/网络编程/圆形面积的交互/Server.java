package work3;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	ServerSocket ss = null;
	Socket socket = null;
	DataInputStream dis = null;
	DataOutputStream dos = null;
	Circle circle = null;
	
	public Server(){
		try {
			ss = new ServerSocket(6660);
			socket = ss.accept();
			dis = new DataInputStream(socket.getInputStream());
			dos = new DataOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void interact(){
		circle = new Circle();
		while(true){
			try {
				double radius = dis.readDouble();
				circle.setRadius(radius);
				double area = circle.getArea();
				dos.writeDouble(area);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public static void main(String[] args) {
		new Server().interact();
	}

}
