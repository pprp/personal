package work3;

public class Circle {
	double r;
	public void setRadius(double r){
		this.r = r;
	}
	public double getArea(){
		return Math.PI * r * r;
	}
}
