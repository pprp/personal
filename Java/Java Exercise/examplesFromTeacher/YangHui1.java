package chapter01.yanghui;

public class YangHui1 {

	public static void main(String[] args) {
		int size = 6;
		int [][] YH = new int[size][];
		for(int i=0;i<size;i++){
			YH[i] = new int[size +1];
		}
		
		for(int i=0;i<size;i++){
			YH[i][0] = YH[i][i] = 1;
			for(int j=1;j<i;j++)
				YH[i][j] = YH[i-1][j-1] +YH[i-1][j]; 
		}
		
		for(int i=0;i<size;i++){
	
			for(int j=0;j<=i;j++)
				System.out.print(YH[i][j] + "   ");
			System.out.println();
		}
	}

}
