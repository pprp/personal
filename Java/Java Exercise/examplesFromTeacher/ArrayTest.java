package chapter01.arrays;

public class ArrayTest {

	public static void main(String[] args) {
		int [] array1  ;                    //1. 声明一个一维数组
//		System.out.println(array1.length);  // 错误：初始化之前不能访问数组的长度		
		array1 = null; 						// 实用null常量初始化数组
//		System.out.println("array1.length: " + array1.length);  // 空指针异常
//		array1 = {3,4};  					// 错误：数组常量只能在初始化操作中使用
		
		array1 = new int[6];				//2. 动态初始化：下标从 0 ~ n-1，默认全0
		
		int [] array2 = {1,2,3};			//3. 静态初始化
		System.out.println("array2.length: " + array2.length); 	
		//4. 数组的length：代表数组的元素个数 
		
		int [][] array3 = new int[6][7];	//5. 声明一个6*7的二维数组
		System.out.println(array3);
		array3[2] =new int[6];              // 改变下标为[2]的数组引用    
		System.out.println("array3[4][4]: "+array3[4][4]); 	// 动态初始化值：0
		
		int [][] array4 = new int[6][]; 	//6. 动态初始化二维数组必须制定第一维的length
		System.out.println("array4[0]: "+array4[0]); // 每一个一维数组为 null
		
		/* 引用测试 */
		int [] test1 = {0,1,2,3,4,5};
		int [] test2 = test1;         // 将test1的引用值赋给test2
		test2[2] = 0;
		for(int i:test2)
			System.out.print(i + " ");
		System.out.println();
		System.out.println(test1);
		System.out.println(test1[2]);
		
	}

}
