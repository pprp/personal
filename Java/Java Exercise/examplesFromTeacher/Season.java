package chapter01.enumtest;

enum Seas {   				// 枚举类型举例
	SPRING, SUMMER, AUTUMN, WINER;  // 分号可以没有
}

public class Season{
	public static void main(String args[])
	{
		Seas seasons = Seas.SPRING;
		System.out.println(seasons);
	}
}

