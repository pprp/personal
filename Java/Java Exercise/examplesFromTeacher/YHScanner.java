package chapter02.scanner;
import java.util.Scanner;   // util 是 java 实用工具包

public class YHScanner {

	public static void main(String[] args) {
		
		Scanner  scanner = new Scanner(System.in); // 识别标准输入的文本扫描器
		System.out.print("输入一个整数值：");
		int size = scanner.nextInt(); 			   // nextInt:阻塞当前执行过程

		int [][] YH = new int[size][];
		for(int i=0;i<size;i++){
			YH[i] = new int[size +1];
		}
		
		for(int i=0;i<size;i++){
			YH[i][0] = YH[i][i] = 1;
			for(int j=1;j<i;j++)
				YH[i][j] = YH[i-1][j-1] +YH[i-1][j]; 
		}
		
		for(int i=0;i<size;i++){
	
			for(int j=0;j<=i;j++)
				System.out.print(YH[i][j] + "   ");
			System.out.println();
		}
	}

}

