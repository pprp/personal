package part5.layout.cardlayout;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class SystemProperty extends JFrame{
	JButton computerName;
	JButton hardware;
	JButton OK;
	JButton cancel;
	JPanel computerNamePanel;
	JPanel hardwarePanel;
	public SystemProperty(){
		init();
		setTitle("系统属性");
		setVisible(true);
		setSize(300,400); // 
		setBounds(300,400,123,111);
//		setLocationRelativeTo(null); //窗口居中
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void init(){
		setIconImage(this.getToolkit().getImage(".\\image\\dog2.png"));

	}
	public static void main(String []args){
		new SystemProperty();
	}
}
