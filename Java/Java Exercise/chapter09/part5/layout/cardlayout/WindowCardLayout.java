package part5.layout.cardlayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class WindowCardLayout extends JFrame implements ActionListener {
    JButton [] button;
    CardLayout card;
    JPanel panel;
    public WindowCardLayout() {
       init();
       setBounds(100,100,600,300);
       setVisible(true);
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    void init() {
       card=new CardLayout();
       panel=new JPanel(); 
       panel.setLayout(card);
       button=new JButton[16];
       for(int i=0;i<button.length;i++) {
          button[i]=new JButton("I am "+i);
          panel.add("we"+i,button[i]);
          button[i].addActionListener(this);
       } 
       add(panel,BorderLayout.CENTER);      
    }
    public void actionPerformed(ActionEvent e){
        card.next(panel);
    }
}

    

