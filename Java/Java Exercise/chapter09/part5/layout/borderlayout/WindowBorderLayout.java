package part5.layout.borderlayout;

import javax.swing.*;
import java.awt.*;
import static java.awt.BorderLayout.*; //引入BorderLayout类的静态常量
public class WindowBorderLayout extends JFrame {
    public WindowBorderLayout() {
       init();
 //      setLayout(new BorderLayout());
       setBounds(100,100,600,300);
       setVisible(true);
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    void init() {
    	
    	
       JButton bSouth=new JButton("我在南边");
       JButton bNorth=new JButton("我在北边");
       JButton bEast =new JButton("我在东边");
       JButton bWest =new JButton("我在西边");
       JButton bCenter=new JButton("我在中心");
       add(bNorth,BorderLayout.NORTH);
       add(bSouth,BorderLayout.SOUTH);
       add(bEast, BorderLayout.EAST);
       add(bWest, BorderLayout.WEST); 
       add(bCenter,BorderLayout.CENTER);
       
       
       
   }
}

    

