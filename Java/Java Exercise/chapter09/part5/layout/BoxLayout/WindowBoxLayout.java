package part5.layout.BoxLayout;

import javax.swing.*;
public class WindowBoxLayout extends JFrame  {
    Box baseBox,boxV1,boxV2; 
    public WindowBoxLayout() {
        setLayout(new java.awt.FlowLayout());
        init();
        setBounds(100,100,600,300);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    void init() {
        boxV1=Box.createVerticalBox();
        boxV1.add(new JLabel("输入您的姓名"));
        boxV1.add(Box.createVerticalStrut(8));
        boxV1.add(new JLabel("输入email"));
        boxV1.add(Box.createVerticalStrut(8));
        boxV1.add(new JLabel("输入您的职业"));
        
        boxV2=Box.createVerticalBox();
        boxV2.add(new JTextField(16));
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(new JTextField(16));
        boxV2.add(Box.createVerticalStrut(8));
        boxV2.add(new JTextField(16));
        
        baseBox=Box.createHorizontalBox();
        baseBox.add(boxV1);
        baseBox.add(Box.createHorizontalStrut(10));
        baseBox.add(boxV2);
        add(baseBox);       
    }
}

    

