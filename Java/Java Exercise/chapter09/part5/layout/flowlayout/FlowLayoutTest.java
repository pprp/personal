package part5.layout.flowlayout;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FlowLayoutTest {
	JFrame frame;
	FlowLayoutTest(){
		frame = new JFrame("流布局测试");
		frame.setLayout(new FlowLayout());	
//		Container contentPane = frame.getContentPane();
		for(int i=1;i<11;i++){
			frame.add(new JButton("按钮"+i));
//			contentPane.add(new JButton("按钮"+i));
		}
		
		frame.setBounds(100,200,300,400);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new FlowLayoutTest();
	}

}
