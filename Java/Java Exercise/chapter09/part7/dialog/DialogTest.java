package part7.dialog;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class DialogTest {

	public static void main(String[] args) {
		new MyJFrame();

	}

}

class MyJFrame extends JFrame implements ActionListener {
	private JButton messageButton;
	private JButton inputButton;
	private JButton confirmButton;
	private JButton fileOpenButton;
	private JButton fileSaveButton;
	
	public MyJFrame() {
		messageButton = new JButton("消息对话框");
		inputButton = new JButton("输入对话框");
		confirmButton = new JButton("确认对话框");
		fileOpenButton = new JButton("打開对话框");
		fileSaveButton = new JButton("保存对话框");
		
		setLayout(new FlowLayout());

		add(messageButton);
		add(inputButton);
		add(confirmButton);
		add(fileOpenButton);
		add(fileSaveButton);
		
		messageButton.addActionListener(this);
		inputButton.addActionListener(this);
		confirmButton.addActionListener(this);
		fileOpenButton.addActionListener(this);
		fileSaveButton.addActionListener(this);
		
		setVisible(true);
		setBounds(300, 400, 400, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == messageButton) {
			JOptionPane.showMessageDialog(null, "消息", "消息对话框",JOptionPane.ERROR_MESSAGE);
		}
		if (e.getSource() == inputButton) {
			String str = JOptionPane.showInputDialog(this,"请输入","输入对话框",JOptionPane.QUESTION_MESSAGE);
			System.out.println(str);
		}
		if (e.getSource() == confirmButton){
			JOptionPane.showConfirmDialog(this, "请选择", "确认对话框", JOptionPane.YES_NO_OPTION);
		}
		if (e.getSource() == fileOpenButton) {
			JFileChooser fileChooser = new JFileChooser("C:\\");
			int returnVal = fileChooser.showOpenDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				String imagePath = fileChooser.getSelectedFile().getAbsolutePath();
				System.out.println(imagePath);
			}
		}
		if (e.getSource() == fileSaveButton) {
			JFileChooser fileChooser = new JFileChooser("C:\\");
			int returnVal = fileChooser.showSaveDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				String imagePath = fileChooser.getSelectedFile().getAbsolutePath();
				System.out.println(imagePath);
			}
		}
		
	}
}