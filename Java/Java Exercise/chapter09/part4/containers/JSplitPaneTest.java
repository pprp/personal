package part4.containers;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

public class JSplitPaneTest extends JFrame {
	JPanel topPanel ;
	JPanel bottomPanel ;
	JPanel leftPanel;
	JPanel rightPanel ;
	JSplitPane verticalSplit;
	JSplitPane horizontalSplit;
	public JSplitPaneTest() {
		init();
		setTitle("测试JSplitPane");
		setSize(488, 308);
		setVisible(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void init() {
		topPanel = new JPanel();
		
		bottomPanel = new JPanel();
		bottomPanel.add(new JLabel("下标签"));
		
		leftPanel = new JPanel();
		leftPanel.add(new JLabel("左标签"));
		
		rightPanel = new JPanel();
		rightPanel.add(new JLabel("右标签"));
		
		verticalSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
				topPanel, bottomPanel);
		verticalSplit.setResizeWeight(0.4);
		add(verticalSplit, BorderLayout.CENTER);
		
		topPanel.setLayout(new BorderLayout());
		horizontalSplit = new JSplitPane(
				JSplitPane.HORIZONTAL_SPLIT, leftPanel, rightPanel);
		
		topPanel.add(horizontalSplit, BorderLayout.CENTER);
		horizontalSplit.setResizeWeight(0.5);
	}

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		new JSplitPaneTest();
	}

}
