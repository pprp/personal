package part4.containers;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class JScrollPaneTest extends JFrame{
	JScrollPane scrollPane ;  
	public JScrollPaneTest(){
		init();
	    setTitle("测试ScrollPane");
	    setBounds(60,100,488,308);
	    setVisible(true);
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    setLocationRelativeTo(null);
	}
	
	public void init(){
		scrollPane = new JScrollPane(new JTextArea());
		add(scrollPane);
	}
	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		new JScrollPaneTest();
	}

}
