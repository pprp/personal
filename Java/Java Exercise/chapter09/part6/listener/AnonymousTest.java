package part6.listener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class AnonymousTest {
	public static void main(String args[]){
		MyJFrame1 mjf= new MyJFrame1();		
	}
}

class MyJFrame1 extends JFrame {
	JTextField text1, text2;
	JLabel label;
	JButton button;
	public MyJFrame1() {
		setLayout(new FlowLayout());
		text1 = new JTextField(6);
		text1.setFont(new Font("宋体", Font.BOLD, 40));
		text2 = new JTextField(6);
		text2.setFont(new Font("宋体", Font.BOLD, 40));
		button = new JButton("平方");
		label = new JLabel("结果：");		
		add(text1);
		add(label);
		add(text2);
		add(button);		
		text1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				String str = text1.getText();
				int n = Integer.parseInt(str);
				text2.setText(""+n*n);
			}
		});
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				String str = text1.getText();
				int n = Integer.parseInt(str);
				text2.setText(""+n*n);
			}
		});
	
		text2.setEditable(false);
		setSize(400,300);
		setLocation(400,200);
		setVisible(true);	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
