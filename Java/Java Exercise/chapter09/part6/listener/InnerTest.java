package part6.listener;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
//
//import swing.example.MyListener;

public class InnerTest {
	public static void main(String [] args){
		new MyJFrame2();
	}
}

class MyJFrame2 extends JFrame {
	JTextField text1, text2;
	JLabel label;
	JButton button;
	MyListener listener;

	public MyJFrame2() {
		setLayout(new FlowLayout());
		text1 = new JTextField(6);
		text1.setFont(new Font("宋体", Font.BOLD, 40));
		text2 = new JTextField(6);
		text2.setFont(new Font("宋体", Font.BOLD, 40));
		button = new JButton("平方");
		label = new JLabel("结果：");
		listener = new MyListener();

		add(text1);
		add(label);
		add(text2);
		add(button);


		text2.setEditable(false);
		setSize(400, 300);
		setLocation(400, 200);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		

		text1.addActionListener(listener);
		button.addActionListener(listener);
	}

	class MyListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			String str = text1.getText();
			int n = Integer.parseInt(str);
			text2.setText("" + n * n);
		}
	}
}
