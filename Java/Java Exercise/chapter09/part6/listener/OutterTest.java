package part6.listener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class OutterTest {
	public static void main(String args[]){
		MyJFrame3 mjf= new MyJFrame3();		
	}
}

class MyJFrame3 extends JFrame {
	JTextField text1, text2;
	JLabel label;
	JButton button;
	MyListener listener;
	public MyJFrame3() {
		setLayout(new FlowLayout());
		text1 = new JTextField(6);
		text1.setFont(new Font("宋体", Font.BOLD, 40));
		text2 = new JTextField(6);
		text2.setFont(new Font("宋体", Font.BOLD, 40));
		button = new JButton("平方");
		label = new JLabel("结果：");
		listener = new MyListener();
		
		add(text1);
		add(label);
		add(text2);
		add(button);
		
		text1.addActionListener(listener);
		button.addActionListener(listener);
		listener.setJTextField(text2);
		listener.setJTextSource(text1);
		text2.setEditable(false);
		setSize(400,300);
		setLocation(400,200);
		setVisible(true);	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}

class MyListener implements ActionListener{
	JTextField text;
	JTextField source;
	public void setJTextField(JTextField text){
		this.text = text;		
	}
	public void setJTextSource(JTextField text){
		this.source = text;		
	}
	public void actionPerformed(ActionEvent e){
		int n=0;
		String str = source.getText();
		try{
		n = Integer.parseInt(str);
		}catch(NumberFormatException nfe){
			JOptionPane.showMessageDialog(source, "请输入一个整数！");
		}
		text.setText(""+n*n);
	}
}