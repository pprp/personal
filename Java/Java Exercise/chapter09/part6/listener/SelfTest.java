package part6.listener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class SelfTest {
	public static void main(String args[]) {
		MyJFrame4 mjf = new MyJFrame4();
	}
}

class MyJFrame4 extends JFrame implements ActionListener {
	JTextField text1, text2;
	JLabel label;
	JButton button,button2;

	public MyJFrame4() {
		setLayout(new FlowLayout());
		text1 = new JTextField(6);
		text1.setFont(new Font("宋体", Font.BOLD, 40));
		text2 = new JTextField(6);
		text2.setFont(new Font("宋体", Font.BOLD, 40));
		button = new JButton("平方");
		button2 = new JButton("立方");
		label = new JLabel("结果：");

		add(text1);
		add(label);
		add(text2);
		add(button);
		add(button2);

		text1.addActionListener(this);
		button.addActionListener(this);
		button2.addActionListener(this);

		text2.setEditable(false);
		setSize(400, 300);
		setLocation(400, 200);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == text1 || e.getSource() == button) {
			String str = text1.getText();
			int n = Integer.parseInt(str);
			text2.setText("" + n * n);
		} else if (e.getSource() == button2) {
			String str = text1.getText();
			int n = Integer.parseInt(str);
			text2.setText("" + n * n * n);
		}
	}
}
