package part6.event;

import java.awt.FlowLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class FocusEvent_Example extends JFrame { // 继承窗体类JFrame

	private JTextField textField;
	private JButton button;
	public static void main(String args[]) {
		FocusEvent_Example frame = new FocusEvent_Example();
		frame.setVisible(true);
	}

	public FocusEvent_Example() {
		super();
		setTitle("焦点事件示例");
		setBounds(100, 100, 500, 375);
		getContentPane().setLayout(new FlowLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final JLabel label = new JLabel();
		label.setText("出生日期：");
		getContentPane().add(label);
		
		TextFieldFocus textFF = new TextFieldFocus();
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.addFocusListener(textFF); // 为文本框添加焦点监听器
		getContentPane().add(textField);

		JButton button = new JButton();
		button.setText("确定");
		button.addFocusListener(textFF); // 为按钮添加焦点监听器
		getContentPane().add(button);
	}

	class TextFieldFocus implements FocusListener {
		public void focusGained(FocusEvent e) {
			if(e.getSource()==textField){
				textField.setText("2008-11-12");
			}
			if(e.getSource()==button){
//			textField.setText("button 得到焦点");
				System.out.println("button 得到焦点");
			}
		}

		public void focusLost(FocusEvent e) {
			if(e.getSource()==textField){
				textField.setText("");
			}
			if(e.getSource()==button){
//				textField.setText("button 失去焦点");
				System.out.println("button 失去焦点");
			}
		}
	}

}
