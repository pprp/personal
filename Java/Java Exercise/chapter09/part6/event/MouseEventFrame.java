package part6.event;

import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
// 窗口类作为监听器类
public class MouseEventFrame extends JFrame implements MouseListener{
	
	JButton pressButton = new JButton("按下");
	JButton upButton = new JButton("弹起");
	JButton clickButton = new JButton("单击");
	JButton doubleClickButton = new JButton("双击");
	
	MouseEventFrame(){
		setLayout(new FlowLayout());
		add(pressButton);
		add(upButton);
		add(clickButton);
		add(doubleClickButton);

		pressButton.addMouseListener(this);
		upButton.addMouseListener(this);
		clickButton.addMouseListener(this);
		doubleClickButton.addMouseListener(this);
		
		setSize(400,500);
		setVisible(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		new MouseEventFrame();
	}

	
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO 自动生成的方法存根
		if(arg0.getSource() == clickButton){
			// this就是窗口对象
			JOptionPane.showMessageDialog(this,"您点击了单击按钮","消息对话框",JOptionPane.WARNING_MESSAGE);
		}
		if(arg0.getSource() == doubleClickButton && arg0.getClickCount() == 2 ){
			// this就是窗口对象
			JOptionPane.showMessageDialog(this,"您双击了双击按钮","消息对话框",JOptionPane.WARNING_MESSAGE);
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO 自动生成的方法存根
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		if(arg0.getSource() == pressButton){
			// this就是窗口对象
			JOptionPane.showMessageDialog(this,"您点击了按下按钮","消息对话框",JOptionPane.WARNING_MESSAGE);
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		if(arg0.getSource() == upButton){
			// this就是窗口对象
			JOptionPane.showMessageDialog(this,"您点击了弹起按钮","消息对话框",JOptionPane.WARNING_MESSAGE);
		}
		
	}

}
