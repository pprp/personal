package part6.event;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class CalculateFrame extends JFrame{

	private JTextField inputNumberOne;
	private JComboBox  choiceFuhao;
	private JTextField inputNumberTwo;
	private JButton	   computeButton;
	private JTextArea  textShow;
	private String     operator;
	
	public CalculateFrame(){
	
		super("简单计算器");
		init();
		this.setSize(400,500);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void init(){
		
		Font  font = new Font("宋体",Font.BOLD,40);
		
		inputNumberOne = new JTextField(5);
		inputNumberOne.setFont(font);
		choiceFuhao    = new JComboBox();
		choiceFuhao.addItem("+");
		choiceFuhao.addItem("-");
		choiceFuhao.addItem("*");
		choiceFuhao.addItem("/");
		choiceFuhao.setFont(font);
		choiceFuhao.addItemListener(new ItemListener(){

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				// TODO 自动生成的方法存根
				operator = (String)choiceFuhao.getSelectedItem();
				textShow.append("您选择了"+ operator + "\n");
			}
			
		});
		
		inputNumberTwo = new JTextField(5);
		inputNumberTwo.setFont(new Font("宋体",Font.BOLD,40));
		computeButton  = new JButton("计算");
		
		JPanel  calculatePanel = new JPanel();
		calculatePanel.add(inputNumberOne);
		calculatePanel.add(choiceFuhao);
		calculatePanel.add(inputNumberTwo);
		calculatePanel.add(computeButton);
		
		add(calculatePanel,BorderLayout.NORTH);
		
		textShow = new JTextArea();
		textShow.setFont(font);
		JScrollPane showPane = new JScrollPane(textShow);
		
		add(showPane,BorderLayout.CENTER);
		
		computeButton.addActionListener(new MyActionListener());
	}
	// 内部类作为监听器类
	class MyActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) throws NumberFormatException{
			if(e.getSource() == computeButton){
				String numberStr1 = inputNumberOne.getText();
				double  number1 = Double.parseDouble(numberStr1);
				String numberStr2 = inputNumberTwo.getText();
				double  number2 = Double.parseDouble(numberStr2);
						
				double result = 0;
				
				operator = (String)choiceFuhao.getSelectedItem();
				switch(operator){
				case "+":
					result = number1 + number2;
					break;
				case "-":
					result = number1 - number2;
					break;
				case "*":
					result = number1 * number2;
					break;
				case "/":
					result = number1 / number2;
					break;
				}
				
				textShow.append(number1 + " " + operator + " " + number2 + " = "+ result + "\n");
			}
			
		}
		
	}
	
	public static void main(String[]args){
		new CalculateFrame();
	}
}
