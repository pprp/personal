package part6.event;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MouseMotionTest {

	public static void main(String[] args) {
		new MouseMotionFrame();
	}

}

class MouseMotionFrame extends JFrame implements MouseMotionListener{
	
	int x;
	int y;
	JLabel label = new JLabel();
	JPanel panel = new JPanel();
	
	public  MouseMotionFrame(){
		super("测试鼠标在组件中的坐标");
		setBounds(300,300,500,500);
		setContentPane(panel);
		panel.add(label);
		panel.addMouseMotionListener(this);
		setVisible(true);
	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO 自动生成的方法存根
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		
		x = arg0.getX();
		y = arg0.getY();
		label.setText(x+"  "+y);
	}
}