package part1.jframe;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyJFrame1 {

	public static void main(String[] args) {
	       JFrame window1=new JFrame("第一个窗口");
	       JFrame window2=new JFrame("第二个窗口");
	       Container con=window1.getContentPane(); //得到窗口的内容窗格
	       con.setBackground(Color.yellow) ;       //设置窗口的背景色
	       System.out.println(window1.getLayout());
	       con.setLayout(new FlowLayout());
	       con.add(new JButton("aaaaa"));
	       JPanel pan = new JPanel();
	       System.out.println(pan.getLayout());
	       window1.setBounds(60,100,288,208);
	       window2.setBounds(260,100,288,208);
	       window1.setVisible(true);
	       window1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	       window2.setVisible(true);
	       window2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	       window1.setResizable(false);
//	       window1.setLocationRelativeTo(null);
	       window2.setLocationRelativeTo(window1);

	}

}
