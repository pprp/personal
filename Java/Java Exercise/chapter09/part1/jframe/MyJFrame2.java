package part1.jframe;

import java.awt.Toolkit;

import javax.swing.JFrame;


public class MyJFrame2 extends JFrame{
	
	public MyJFrame2(){
		setTitle("MyJFrame2");
		//使用默认工具包创建Image对象
		setIconImage(Toolkit.getDefaultToolkit().createImage("icons\\d.gif"));
		setVisible(true);
		setSize(400,300);
		this.setLocationRelativeTo(null);
	}



}

class MyJFrame2Test{
	public static void main(String[] args) {
		new MyJFrame2();
	}
}