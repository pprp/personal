package part2.jmenu;

import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class MyMenuTest {

	public static void main(String[] args) {
		new MyMenuFrame();

	}

}

class MyMenuFrame extends JFrame{
	
	public MyMenuFrame(){
		init();
		setTitle("MyMenuFrame");
		setIconImage(Toolkit.getDefaultToolkit().createImage("icons\\d.gif"));
		setVisible(true);
		setSize(400,300);
		setLocation(100,100);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void init(){
		JMenuBar menubar = new JMenuBar();
		JMenu    menu  = new JMenu("文件");
		JMenu    menu2 = new JMenu("运行");
		
		JMenuItem createItem = new JMenuItem("新建",new ImageIcon("icons\\ok.gif"));
		createItem.setAccelerator(KeyStroke.getKeyStroke('N'));
		
		JMenuItem openItem = new JMenuItem("打开文件",new ImageIcon("icons\\info.gif"));
		openItem.setAccelerator(KeyStroke.getKeyStroke('O'));
		
		JMenuItem saveItem = new JMenuItem("保存");
		saveItem.setEnabled(false);
		saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, 
				InputEvent.CTRL_MASK));
		
		setJMenuBar(menubar);
		
		menubar.add(menu);
		menubar.add(menu2);
		
		menu.add(createItem);
		menu.add(openItem);
		menu.addSeparator();
		menu.add(saveItem);
	}
}