package io1.file;
/*
 * 创建文件、目录
 * File类的基本方法
 * File对象不能修改文件内部数据
 */
import java.io.File;
import java.io.IOException;

public class FileTest {

	public static void main(String[] args) {
		
		File f1 = new File("D:\\file1.txt");  // 绝对路径实例化File对象
		if(! f1.exists() ){
			try {
				f1.createNewFile();
			} catch (IOException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}else{
			System.out.println(f1.getName());  	
			System.out.println(f1.getAbsolutePath());
			System.out.println(f1.getParent());
			System.out.println(f1.canRead());	
			System.out.println(f1.length());	
			System.out.println(f1.isFile());	
			
		}
		f1.delete();
		
		
		File f2 = new File("D:\\dir");
		if(! f2.exists()){
			f2.mkdir();
		}else{
			System.out.println(f2.isDirectory());
			String [] fileList1 = f2.list();
			File [] fileList2 = f2.listFiles();
		}
		
	}

}
