package lesson2.datainputstream;
import java.io.*;

public class DataInputStreamTest
{
	public static void main(String [] args){
		try{
		File f = new File("data.txt");
		FileOutputStream fos = new FileOutputStream(f);
		DataOutputStream dos = new DataOutputStream(fos);

		int [] numbers = {2012110001,2012110002,2012110003};
		String [] names = {"张三","李四","王五"};
		double [] heights = {1.76,1.80,1.77};
		for(int i=0;i<3;i++){
			dos.writeInt(numbers[i]);
			dos.writeUTF(names[i]);
			dos.writeDouble(heights[i]);
		}
		dos.close();
		fos.close();
		
		FileInputStream fis = new FileInputStream(f);
		DataInputStream  dis = new DataInputStream(fis);
		
		int number;String name;double height;
		do{
			number = dis.readInt();
			name = dis.readUTF();
			height = dis.readDouble();
			System.out.println(number + "  "+ name + "   " + height);
		} while(true);
		}catch(IOException e){
			System.out.println("文件读取结束。");
		}
	}
}