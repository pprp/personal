package lesson2.serializable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializableTest {

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		// TODO 自动生成的方法存根
		Monkey m1 = new Monkey("sun",150);
		Monkey m2 = m1.serializableClone();
		System.out.println(m1.name == m2.name);
	}
	


}
