package lesson2.serializable;

public class CloneTest {

	public static void main(String[] args) throws CloneNotSupportedException {
		// TODO 自动生成的方法存根
		Monkey sun = new Monkey("孙悟空",130);
		sun.setName("斗战胜佛");
		
		Monkey sunCopy = sun.clone();
		sunCopy.setHeight(180);
		
		System.out.println(sun.name == sunCopy.name);
		System.out.println(sun.height == sunCopy.height);
	}

}
