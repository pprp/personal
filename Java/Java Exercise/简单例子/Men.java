package test;

class Person
{
	int a;
    Person()
    {
    	a = 10;
    	System.out.println("这是父类的构造函数！");
    }
}

public class Men extends Person //这样才能继承
{
	int b;
	Men()
	{
		b = 20;
		System.out.println("这是子类的构造函数！");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Men m = new Men();
		System.out.println(m.a + " " + m.b);

	}

}
