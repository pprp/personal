package test;

class Person
{
	 int a;
	 int b;
	 int c;
	 Person()
	 {
		 a = 1;
	 }
	 Person(int a)
	 {
		 this.a = a;
	 }
	 Person(int a, int b, int c)
	 {
		 this.a = a;
		 this.b = b;
		 this.c = c;
	 }
}

public class Men1 extends Person
{
	int b;
	Men1(int a, int b, int c)
	{
		//super(a);//调用父类的函数，相当于父类的构造函数
		super(a,b,c);
		this.b = b;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Men1 mm = new Men1(10,20,32);
		System.out.println(mm.a + " " + mm.b + " " + mm.c);
	}

}
