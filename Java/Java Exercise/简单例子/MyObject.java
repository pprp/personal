package test;

//学习测试对象中访问控制符的用法
//private是只能在声明他的那个类中才能使用
//default是只有同一个包中的类才能访问
//protected意味着不仅同一个包中的类可以访问，并且位于其他包中类子类也可以访问
public class MyObject {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		pril p = new pril();
		p.setX(10);
		System.out.println(p.getX());
		
		pril1 p1 = new pril1();
		System.out.println(p1.x);
		p1.print(10);
	}

}
//public控制符的测试，任何类都可以使用
class pril1
{
	public int x = 1;
	public void print(int y )
	{
		System.out.println(2 * y);
	}
}

//private控制符的测试
class pril
{
	private int x;
	void setX(int y)
	{
		x = y;
	}
	int getX()
	{
		return x;
	}
}

