
package test;

public class Duotai {
	int x, y, z;
	void print(int x)
	{
		System.out.println(x*2);
	}
	void print(int x, int y)
	{
		System.out.println( 2 * x + y);
	}
	void print(int x, int y, int z)
	{
		System.out.println(2 * x + y * z);
	}
	void print(double a)
	{
		System.out.println(2 * a);
	}
	void print(double a, double b)
	{
		System.out.println(a * b);
	}
	public static void main(String[] args)
	{
		Duotai dt = new Duotai();
		dt.print(2);
		dt.print(2,3);
		dt.print(2,3,4);
		dt.print(1.1,2.2);
		dt.print(1.232);
	}

}
