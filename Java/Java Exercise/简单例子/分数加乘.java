package test;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Fraction a = new Fraction(in.nextInt(), in.nextInt());
		Fraction b = new Fraction(in.nextInt(),in.nextInt());
		a.print();
		b.print();
		a.plus(b).print();
		a.multiply(b).plus(new Fraction(5,6)).print();
		a.print();
		b.print();
		in.close();
	}
}
class Fraction{
	int mu;
	int zi;
	Fraction(){
		mu = 0;
		zi = 0;
	}
	Fraction(int a, int b){
		int tmp = gcd(a,b);
		this.mu = b/tmp;
		this.zi = a/tmp;
	}
	int gcd(int a, int b){
		return b == 0? a:gcd(b,a%b);
	}
	void print(){
		if(zi == 0)
		{
			System.out.println(0);
			return;
		}
		if(mu == 1)
		{
			System.out.println(zi);
			return;
		}
		else
		{
			System.out.println(zi+"/"+mu);
			return ;
		}
	}
	Double toDouble()
	{
		return this.zi*1.0/this.mu*1.0;
	}
	Fraction plus(Fraction a){
		if(a.mu == 0 && this.mu == 0)
			System.out.println("illegal");
		Fraction tmp = new Fraction();
		tmp.mu = a.mu * this.mu;
		tmp.zi = a.zi * this.mu + a.mu * this.zi;
		int gg = gcd(tmp.mu,tmp.zi);
		tmp.mu /= gg;
		tmp.zi /= gg;
		return tmp;
	}
	Fraction multiply(Fraction b){
		Fraction tmp = new Fraction();
		if(b.zi == 0 || this.zi == 0)
			return tmp;
		tmp.zi = b.zi * this.zi;
		tmp.mu = b.mu * this.mu;
		return tmp;		
	}
	
}