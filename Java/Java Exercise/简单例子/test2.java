package test;
import java.util.*;

public class test2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("welcome to test2");
	    //记住数组初始化应该这样
	    int[] d = new int[]{1,2,3,4};
	    //字符串的处理
	    String str;
	    str = "everybody - AI";
	    System.out.println(str);
	    
	    //自己看了一下Java的输入操作，很麻烦
	    Scanner in = new Scanner(System.in);
	    int a = in.nextInt();
	    System.out.println(a);
	    
	    //字符串类型的输入
	    String tmp = in.next();
	    System.out.println(tmp);
	    
	    in.close();
	    
	    //字符串相关操作
	    String str1 = "very";
	    str1 = str1 + " good";
	    System.out.println(str1);
	    
	    //字符串的处理类String
	    String str3 = new String();
	    System.out.println(str3);
	    
	    //字节参数的构造器
	    byte[] b = {97,98,99,100,101,102,103};//分别代表abc
	    String str4 = new String(b);
	    System.out.println(str4);
	    
	    //字符串数组的构造器
	    char[] c = {'w','r','i','t','e','r'};
	    String str6 = new String(c);
	    System.out.println(str6);
	    
	    String str7 = new String(c,3,2);
	    System.out.println(str7);
	  
	    //获取指定字节的构造器,从第三位开始取三位
	    String str5 = new String(b,3,3);
	    System.out.println(str5);
	    
	    //字符串的处理方法
	    //1、串链接 用＋号或者用函数concat
	    String str2 = "you";
	    String str8 = "welcome";
	    System.out.println(str8.concat(str2));
	    System.out.println(str8);//函数没有改变这个子串本身
	    
	    //提取子字符串			
	    String str9 = "Without tears and sweats ,nothing will come for you";
	    System.out.println(str9.substring(0,6));
	    System.out.println(str9.substring(8));
	    
	    //分解字符
	    System.out.println(str9.charAt(0));
	    //长度
	    System.out.println(str9.length());
	    
	    //字符串是否相等
	    String s0 = "administrator";
	    String s1 = "Administrator";
	    if(s0.equals(s1))
	    {
	    	System.out.println("test1: equal");
	    }
	    if(s0.equalsIgnoreCase(s1))
	    {
	    	System.out.println("test2: equal");
	    }
	    
	    //查找特定子串
	    String s2 = "没有付出汗水和泪水，就什么也不会得到";
	    System.out.println(s2.indexOf("和泪水"));
	    System.out.println(s2.startsWith("没有"));
	    System.out.println(s2.endsWith("得到"));
	    
	    //StringBuffer缓冲字符串
	    StringBuffer sb1 = new StringBuffer();
	    System.out.println(sb1.capacity() + "　　" +sb1.length());
	    
	    
	    //StringBuffer的处理
	    StringBuffer sb2 = new StringBuffer("I wanna become ");
	    sb2.append("a great acmer");
	    System.out.println(sb2);
	    sb2.setLength(25);
	    System.out.println(sb2);
	    System.out.println(sb2.charAt(1));
	    sb2.setCharAt(1, 'P');
	    System.out.println(sb2);
	    
	    //指定位置插入字符
	    sb2.insert(1," writer:pprp ");
	    System.out.println(sb2);
	    
	    //从尾部插入子串
	    sb2.append("date:2017/8/23");
	    System.out.println(sb2);
	    
	    //倒置
	    System.out.println(sb2.reverse());
	}

}
