import java.util.Scanner;
import java.util.*;

public class pro2{
	static int maxn = 1000;
	static boolean notPrime[] = new boolean[maxn];
	public static void main(String [] args){
		Scanner reader = new Scanner(System.in);
		int a = reader.nextInt();
		int b = reader.nextInt();
		clear();
		getPrime();
		for(int i =  a; i <= b; i++){
			    if(!notPrime[i])
				    System.out.println(i);
		}	
	}
	private static void clear()
	{
		for(int i = 0 ; i < maxn ; i++)
		{
			notPrime[i] = false;
		}
		notPrime[0] = notPrime[1] = true;
	}
	private static void getPrime()
	{	
		for(int i = 2 ; i < maxn; i++)
		{
			if(i > maxn/i)
				continue;
			if(notPrime[i] != true)
			{
				for(int j = i*i;j < maxn; j+=i)
				{
					notPrime[j] = true;
				}
			}			
		}		
	}
}