package test;

public class Student {
	//创建成员变量
	String name = "";
	String code = "";
	String sex = "";
	double achievement;
	public Student(String name)
	{
		this.name = name;
	}
	void set(String name, String code, String sex)
	{
		this.name = name;
		this.code = code;
		this.sex = sex;
	}
	public String getname()
	{
		return name;
	}
	public String getcode()
	{
		return code;
	}
	public String getsex()
	{
		return sex;
	}
	public void setachi(double achievement)
	{
		this.achievement = achievement;
	}
	public double getachi()
	{
		return achievement;
	}
	public void print()
	{
		System.out.println("student:" + name + " code" + code + " sex" + sex);
	}
	public String tostring()
	{
		String infor = "student name: " + name + " code " + code + "sex" + sex;
		return infor;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student st1 = new Student("name1");
		Student st2 = new Student("name2");
		Student st3 = new Student("name3");
		Student st4 = new Student("name4");
		Student st5 = new Student("name5");
		Student st6 = new Student("name6");
		Student st7 = new Student("name7");
		Student st8 = new Student("name8");
		Student st9 = new Student("name9");
		Student st10 = new Student("name10");
		
		Student[] st = new Student[]
				{st1,st2,st3,st4,st5,
				st6,st7,st8,st9,st10};
		st1.set("name1","1","man");
		st2.set("name2","2", "female");
		st3.set("name3","3", "man");
		st4.set("name4","4","man");
		st5.set("name5","5","female");
		st6.set("name6","6","male");
		st7.set("name7","7","female");
		st8.set("name7","7","male");
		st9.set("name8","8","female");
		st10.set("name10","10","female");
		
		System.out.println(st1.tostring());
		System.out.println(st2.tostring());
		System.out.println(st3.tostring());
		System.out.println(st4.tostring());
		System.out.println(st5.tostring());
		System.out.println(st6.tostring());
		System.out.println(st7.tostring());
		System.out.println(st8.tostring());
		System.out.println(st9.tostring());
		System.out.println(st10.tostring());
		
		st1.setachi(Math.random() * 100);
		st2.setachi(Math.random() * 100);
		st3.setachi(Math.random() * 100);
		st4.setachi(Math.random() * 100);
		st5.setachi(Math.random() * 100);
		st6.setachi(Math.random() * 100);
		st7.setachi(Math.random() * 100);
		st8.setachi(Math.random() * 100);
		st9.setachi(Math.random() * 100);
		st10.setachi(Math.random() * 100);
		
		st1.print();st2.print();st3.print();
		st4.print();st5.print();st6.print();
		st7.print();st8.print();st9.print();
		st10.print();
		
		for(int i = 0 ; i < st.length ; i++)
			for(int j = 0 ; j < st.length ; j++)
			{
				if(st[i].achievement < st[j].achievement)
				{
					Student tmp;
					tmp =  st[i];
					st[i] = st[j];
					st[j] = tmp;
				}
			}
		
		System.out.println("best grader is " + st[9].name + " grade: " + 
		st[9].achievement + "sex is " + st[9].sex);
		System.out.println("lowest grader is " + st[9].name + " grade: " + 
		st[9].achievement + "sex is " + st[9].sex);
		
	}

}
