package test;

import java.util.Date;
import java.util.*;

public class class_test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(Math.pow(2, 3));
		
		double a = 100 * Math.random();
		double x = 4.51;
		
		System.out.println(a);
		System.out.println(Math.sqrt(a));
		System.out.println(Math.max(a, 10));
		
		//Math中的方法和字段属性都是静态的所以可以直接使用它们
		//四舍五入
		System.out.println(Math.round(x));
		//向下取整
		System.out.println(Math.floor(x));
		
		//测试时间和日期的类date
		//输出当前时间
		System.out.println(new Date());
		
		//测试日历的类GregorianCalendar
		GregorianCalendar gc = new GregorianCalendar();
		int y = gc.get(Calendar.MONTH);
		System.out.println(y);
		
		gc.set(Calendar.YEAR, 2019);
		System.out.println(gc.get(Calendar.YEAR));
		gc.set(2017,8,17);
		System.out.println(gc.get(Calendar.YEAR));
		System.out.println(gc.get(Calendar.MONTH));
		System.out.println(gc.get(Calendar.DATE));
		
		//获得系统当前时间
		String now = gc.get(Calendar.YEAR) + "年" + gc.get(Calendar.MONTH)
+ "月" + gc.get(Calendar.DATE) + "日" + gc.get(Calendar.HOUR) + "小时" + gc.get(Calendar.MINUTE) + "分"
+ gc.get(Calendar.SECOND) + "秒" ;
		System.out.println(now);
		
		//给你一个人的生日，分析每年的生日是星期几？
		final char[] kor_week = {'日','一','二','三','四','五','六'};
		for(int i = 2010 ; i <= 2020 ; i++)
		{
			gc.set(i,Calendar.JULY,20); //设置日期
			char week = kor_week[gc.get(Calendar.DAY_OF_WEEK) - 1];
			System.out.println(i + "年的生日是星期" + week);
		}
		
	}

}
 