package part2.jmenu;

import javax.swing.*;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import  javax.swing.JOptionPane;

public class WindowMenu extends JFrame {
	JMenuBar menubar;
	JMenu menu, subMenu;
	JMenuItem itemLiterature, itemCooking;

	public WindowMenu() {
	}

	public WindowMenu(String s, int x, int y, int w, int h) {
		
		setIconImage(Toolkit.getDefaultToolkit().getImage("icons\\a.gif"));
		init(s);
		setLocation(x, y);
		setSize(w, h);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	void init(String s) {
		setTitle(s); // 设置窗口的标题
		menubar = new JMenuBar();
		menu = new JMenu("菜单");
		subMenu = new JMenu("体育话题");
		itemLiterature = new JMenuItem("文学话题", new ImageIcon("icons\\a.gif"));
		itemCooking = new JMenuItem("烹饪话题", new ImageIcon("icons\\b.gif"));
		itemLiterature.setAccelerator(KeyStroke.getKeyStroke('A'));
		itemLiterature.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == itemLiterature)
					JOptionPane.showMessageDialog(null,"您点击了文学话题菜单项","消息对话框",JOptionPane.WARNING_MESSAGE);
			}
		});
		itemCooking.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				InputEvent.CTRL_MASK));
		itemCooking.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == itemCooking)
					JOptionPane.showMessageDialog(null,"您点击了组合键Ctrl + S","消息对话框",JOptionPane.WARNING_MESSAGE);
			}
		});
		
		menu.add(itemLiterature);
		menu.addSeparator(); // 在菜单之间增加分隔线
		menu.add(itemCooking);
		menu.add(subMenu);
		subMenu.add(new JMenuItem("足球", new ImageIcon("icons\\c.gif")));
		subMenu.add(new JMenuItem("篮球", new ImageIcon("icons\\d.gif")));
		menubar.add(menu);
		setJMenuBar(menubar);
	}
}
