package part5.layout.borderlayout;

import javax.swing.*;
import java.awt.*;
import static java.awt.BorderLayout.*; //引入BorderLayout类的静态常量
public class WindowBorderLayout extends JFrame {
    public WindowBorderLayout() {
       init();
 //      setLayout(new BorderLayout());
       setBounds(100,100,600,300);
       setVisible(true);
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    void init() {
       JButton bSouth=new JButton("我在南边"),
               bNorth=new JButton("我在北边"),
               bEast =new JButton("我在东边"),
               bWest =new JButton("我在西边"),
               bCenter=new JButton("我在中心");
       add(bNorth,BorderLayout.NORTH);
       add(bSouth,SOUTH);
       add(bEast,EAST);
       add(bWest,WEST); 
       add(bCenter,CENTER);
   }
}

    

