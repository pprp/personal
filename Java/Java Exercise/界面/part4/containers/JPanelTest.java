package part4.containers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class JPanelTest extends JFrame{
	public JPanelTest(){
		init();
	    setTitle("测试JPanel");
	    setBounds(60,100,488,308);
	    setVisible(true);
	    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    setLocationRelativeTo(null);
	}
	public void init(){
		add(new JButton("one"),BorderLayout.NORTH);
//		add(new JButton("two"),BorderLayout.EAST);
		add(new JButton("three"),BorderLayout.WEST);
		add(new JButton("four"),BorderLayout.SOUTH);
		
		JPanel centerPanel = new JPanel();
		add(centerPanel,BorderLayout.CENTER);
		centerPanel.add(new JLabel("标签-x"));
		centerPanel.add(new JLabel("标签-y"));
		centerPanel.add(new JLabel("标签-z"));
		centerPanel.add(new JButton("按钮-x"));
		centerPanel.add(new JButton("按钮-y"));
		centerPanel.add(new JButton("按钮-z"));

	}
	
	
	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		new JPanelTest();
	}

}
