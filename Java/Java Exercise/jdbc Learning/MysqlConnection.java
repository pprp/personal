package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.UIManager;

public class MysqlConnection {
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch(Exception e){
			e.printStackTrace();
		}

		// // 5、查询结果
		// StringBuffer sbuff = new StringBuffer();
		// try {
		// while (rs.next()) {
		// for (int i = 1; i <= 3; i++) {
		// sbuff.append(rs.getString(i) + '\t');
		// }
		// System.out.println(sbuff);
		// sbuff.delete(0, sbuff.length());
		// }
		// } catch (SQLException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// // 6、关闭连接
		// try {
		// con.close();
		// System.out.println("关闭成功");
		// } catch (SQLException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// System.out.println("关闭失败");
		// }
	}

	Connection con = null;
	Statement sql = null;

	public MysqlConnection() {
		// 1、加载驱动
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("已经成功驱动");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("没有驱动成功！");
		}

		// 2、建立连接
		String url = "jdbc:mysql://localhost/test?useSSL=true";
		String user = "root";
		String pwd = "root";
		try {
			con = DriverManager.getConnection(url, user, pwd);
			System.out.println("连接成功！");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("连接失败！");
		}

		// 3、生成statement对象
		try {
			sql = con.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	boolean log(String name, String pwd) {
		String sqlStatement = "select userPwd from user where userName = '"
				+ name + "'";

		try {
			ResultSet rs = sql.executeQuery(sqlStatement);
			if (!rs.next())
				return false;
			String pass = rs.getString("userPwd");
			if (pass.equals(pwd))
				return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

}
