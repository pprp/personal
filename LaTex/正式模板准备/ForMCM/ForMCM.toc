\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Background}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Previous Research}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Out work}{2}{subsection.1.3}
\contentsline {section}{\numberline {2}Analysis of the Problem}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Problem one}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Problem two}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Problem three}{2}{subsection.2.3}
\contentsline {section}{\numberline {3}Assumptions}{2}{section.3}
\contentsline {section}{\numberline {4}Symbols Definition}{3}{section.4}
\contentsline {section}{\numberline {5}Validating the Model}{3}{section.5}
\contentsline {section}{\numberline {6}Conclusions}{3}{section.6}
\contentsline {section}{\numberline {7}A Summary}{3}{section.7}
\contentsline {section}{\numberline {8}Evaluate of the Mode}{3}{section.8}
\contentsline {section}{\numberline {9}Strengths and weaknesses}{3}{section.9}
\contentsline {subsection}{\numberline {9.1}Strengths}{3}{subsection.9.1}
\contentsline {section}{Appendices}{3}{section*.2}
\contentsline {section}{Appendix \numberline {A}First appendix}{3}{Appendix.a.A}
\contentsline {section}{Appendix \numberline {B}Second appendix}{3}{Appendix.a.B}
