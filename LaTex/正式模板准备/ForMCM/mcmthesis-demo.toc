\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Background}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Previous Research}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Out work}{3}{subsection.1.3}
\contentsline {section}{\numberline {2}Analysis of the Problem}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Problem one}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Problem two}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Problem three}{4}{subsection.2.3}
\contentsline {section}{\numberline {3}Assumptions}{4}{section.3}
\contentsline {section}{\numberline {4}Symbols Definition}{4}{section.4}
\contentsline {section}{\numberline {5}Validating the Model}{4}{section.5}
\contentsline {subsection}{\numberline {5.1}北极冰帽覆盖面积随时间变化的回归方程}{4}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}数据}{4}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}原始数据散点图}{6}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}参数$b_0$,$b_1$}{6}{subsubsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.4}参数的置信区间}{6}{subsubsection.5.1.4}
\contentsline {subsubsection}{\numberline {5.1.5}残差r及其置信区间}{6}{subsubsection.5.1.5}
\contentsline {subsubsection}{\numberline {5.1.6}检验参数}{6}{subsubsection.5.1.6}
\contentsline {subsubsection}{\numberline {5.1.7}残差图}{6}{subsubsection.5.1.7}
\contentsline {subsubsection}{\numberline {5.1.8}散点图及回归线}{6}{subsubsection.5.1.8}
\contentsline {subsection}{\numberline {5.2}$CO_2$浓度与北极冰帽覆盖面积之间的回归方程}{7}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}数据来源}{9}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}原始数据散点图}{9}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}参数值及置信区间}{9}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}残差r及其置信区间}{9}{subsubsection.5.2.4}
\contentsline {subsubsection}{\numberline {5.2.5}检验参数}{9}{subsubsection.5.2.5}
\contentsline {subsubsection}{\numberline {5.2.6}残差图}{9}{subsubsection.5.2.6}
\contentsline {subsubsection}{\numberline {5.2.7}散点图及回归线}{9}{subsubsection.5.2.7}
\contentsline {subsection}{\numberline {5.3}研究北极冰帽融化对佛罗里达州会产生什么影响，应用层次分析法针对如何减小浓度给出可行性措施。}{10}{subsection.5.3}
\contentsline {section}{\numberline {6}Conclusions}{10}{section.6}
\contentsline {section}{\numberline {7}A Summary}{10}{section.7}
\contentsline {section}{\numberline {8}Evaluate of the Mode}{10}{section.8}
\contentsline {section}{\numberline {9}Strengths and weaknesses}{10}{section.9}
\contentsline {subsection}{\numberline {9.1}Strengths}{10}{subsection.9.1}
\contentsline {section}{Appendices}{12}{section*.2}
\contentsline {section}{Appendix \numberline {A}First appendix}{12}{Appendix.a.A}
\contentsline {section}{Appendix \numberline {B}Second appendix}{13}{Appendix.a.B}
